/**
 * Revision History
 * ----------------
 * 10-JUN-2013 - Initial Draft
 * 12-MAY-2014 - Added database to store test result
 */

package com.hip.cqa;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.KeyEvent;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.Toast;
import android.util.Log;

import com.hip.cqa.database.TestCase;
import com.hip.cqa.display.testpattern.TestPattern;
import com.hip.cqa.vibrator.VibratorTest;
import com.hip.cqa.simcard.SimCardTest;
import com.hip.cqa.proximity.ProximityTest;
import com.hip.cqa.ambientlight.AmbientLightTest;
import com.hip.cqa.headset.HeadsetInfoTest;
import com.hip.cqa.audio.AudioPlayMediaFile;
import com.hip.cqa.audio.AudioMicLoopBack;

import android.graphics.PorterDuff;

public class Test_All_Mode extends Test_Main
{
	protected String TAG = "Hi-P_Test_All_Mode";
	
	private Button button_start_test_all_mode;
	private boolean isTestCompleted = false;

	//static Handler handler = new Handler();
	
	//private int testMode;
	//private int totalTestCase = 8;
	//private int testcase_count = 1;
	//private boolean test_completed = false;
	//private static final String[] testcases = { "DISPLAY TEST PATTERN", "VIBRATOR", "SIM CARD", "PROXIMITY SENSOR", "AMBIENT LIGHT SENSOR", "HEADSET", "AUDIO MEDIA FILE PLAYBACK", "AUDIO MIC LOOPBACK"};
		
	public static File testResultFile;
	private String filename = "resultLog.xml";
	private String filepath = "log";
	
	/* Time */
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
	
	//public static Handler handler = new Handler();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test_all_mode);
		
		button_start_test_all_mode = (Button) findViewById(R.id.button_start_all_test_cases);
		button_start_test_all_mode.getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY); /* Set GO button to Green Color */
		button_start_test_all_mode.setOnClickListener (onClickListener);		
		
	} //End of protected void onCreate(Bundle savedInstanceState)

	/* Define class for onClickListener */
	private OnClickListener onClickListener = new OnClickListener()
	{		
		public void onClick(View v)
		{
			if (v.getId() == R.id.button_start_all_test_cases)
			{
				//Test_All_Mode.this.test_completed = startLoopTestCases(); 
				//Test_All_Mode.this.test_completed = true; //for testing only
				
				//Database - Clear and insert all the testcase database when run test all mode		
				insertTestCaseIntoDatabase();
				
				createTestResultFile();
				
				recordTestTime();
				
				startLoopTestCases();
				
				/*if (Test_All_Mode.this.test_completed)
				{
					Toast.makeText(getApplicationContext(), "Functional Test Completed. Press VOL-UP to EXIT", Toast.LENGTH_LONG).show();
				}*/
			}
					
		}
	};
	
	private boolean startLoopTestCases()
	{		
		/* Display Test Pattern */
		Intent displayIntent = new Intent(getBaseContext(), TestPattern.class);
		displayIntent.putExtra("testModeType", 1); /* Test_All_Mode = 1 */
		startActivity(displayIntent);
		finish();
		
		return true;
	}
	
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {	    	
    	if((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)) //&& (this.test_completed))
    	{
        	finish();

     		/* Enable system and ringer tone before exiting functional test */
    		/*AudioManager mAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
			int streamType = AudioManager.STREAM_SYSTEM;
    		mAudioManager.setStreamSolo(streamType, false);
    		mAudioManager.setRingerMode(AudioManager.MODE_IN_CALL);
    		mAudioManager.setStreamMute(streamType, false);*/
    		
    		/* Return to Android Home Screen */
    		/*Intent homescreenIntent = new Intent(Intent.ACTION_MAIN);
    		homescreenIntent.addCategory(Intent.CATEGORY_HOME);
    		homescreenIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    		startActivity(homescreenIntent);*/	
    		
    		/* Return to Main Menu */
			Intent nIntent = new Intent(this, Test_Main.class);
			nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(nIntent);
    	}
    	else if((keyCode == KeyEvent.KEYCODE_BACK) || (keyCode == KeyEvent.KEYCODE_VOLUME_UP))
    	{
    		Toast.makeText(this, getString(R.string.mode_exit), 0).show();
    	}
    	return true;
    }
    
	protected void onDestroy()
	{
		super.onDestroy();
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}	
	
	private void createTestResultFile()
	{	
		ContextWrapper contextWrapper = new ContextWrapper(getApplicationContext());
		File directory = contextWrapper.getDir(filepath, Context.MODE_PRIVATE);
		
		/* Create the full path to file in /data/data/com.hip.cqa/app_log/testLog */
		testResultFile = new File(directory, filename); //This command does not create "testLog"
		
		if (testResultFile.exists())
		{
			/* Delete existing testLog */
			testResultFile.delete();
		} 
	}
	
	/* Record time to /data/data/com.hip.cqa/app_log/resultLog */
	private void recordTestTime() 
	{
		/* Set current data and time */
		String TestTime = date.format(new Date());

		/* Save start test time to resultLog */
		StringBuilder builder = new StringBuilder("");
		builder.append(TestTime);
		Log.d("In Test_All_Mode", "TestTime = " + TestTime);

		try 
		{
			/* /data/data/com.hip.cqa/app_log/resultLog */
			BufferedWriter buf = new BufferedWriter(new FileWriter(testResultFile, true));
			buf.append(builder.toString());
			buf.newLine();
			buf.close();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void insertTestCaseIntoDatabase() {
		//Clear testcase database
		db.deleteAllTestCase();
		
		//TestCase - Insert all the test case		
		db.addTestCase(tcDisplay);
		db.addTestCase(tcVibrator);
		db.addTestCase(tcSimCard);
		db.addTestCase(tcAudioMic);
		db.addTestCase(tcHeadset);
		db.addTestCase(tcAudioMedia);
		db.addTestCase(tcFlashLED);
		db.addTestCase(tcTouch);
		db.addTestCase(tcCamera);
		db.addTestCase(tcNfc);
		db.addTestCase(tcProx);
		db.addTestCase(tcAmbLight);
		db.addTestCase(tcGyro);
		db.addTestCase(tcAcc);
		db.addTestCase(tcEcompass);
		db.addTestCase(tcBattery);
		
		/*For reading all the contact
		 Log.d("Reading: ", "Reading all contacts.."); 
	     List<TestCase> testcase = db.getAllTestCase();           
	        
        for (TestCase tc : testcase) {
            String log = "Id: "+tc.get_id()+" ,Name: " + tc.get_test_case_name() + " ,Time: " + tc.get_test_time();
                // Writing Contacts to log
        Log.d(TAG, " "+log);
        }
		*/
	}	

}
