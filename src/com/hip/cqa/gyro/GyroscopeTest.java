/**
 * Revision History
 * ----------------
 * 03-SEP-2013 - Initial Release. Added for P2B. @author JS.Fong 
 * 25-MAY-2014 - Added function to read gyro spec threshold from xml (/data/specs_vX.xml)
 */

package com.hip.cqa.gyro;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hip.cqa.R;
import com.hip.cqa.Test_All_Mode;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.Test_Main;
import com.hip.cqa.acc.AccelerometerTest;
//import com.hip.cqa.eink.EinkWaveTestActivity;
import com.hip.cqa.nfc.NfcTest;
import com.hip.cqa.parse.specs.ParseSpecs;

public class GyroscopeTest extends Test_Main implements SensorEventListener {

	// UI
	TextView xAxis, yAxis, zAxis, tvStatus;
	private static Axis gyroAxis = new Axis();
	Button bLogValue;
	private static int State = 0; // 0 - normal 1- stared
	static Handler hDataLogging = new Handler();
	private static boolean isLogging = false;
	RelativeLayout rlGryoLayout;
	int testAxis = 1; //axis x = 1, y = 2, z = 3	
	String currentTestState = "ready"; // busy, ready, complete

	// Sensor Manager
	private SensorManager sManager;
	private Sensor gyroscope;
	private int testMode;
	float gryoData[];
	private float averXData = 0;
	private float averYData = 0;
	private float averZData = 0;

	private static String testCaseStr = "Gyroscope Test - ";
	private static String testCaseResult = "Not Tested";
	final Context context = this;
	private String TAG;

	// File saving
	private static String filename = "GyroSensorLog.csv";
	public static File sensorDataFile;
	private String filepath = "log";

	// Data logging
	private static int logDelay = 500; // 500ms
	private static int logTime = 0; // 10*0.5 = 5s
	private static int numofData = 10; //10

	/* Time */
	@SuppressLint("SimpleDateFormat")
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
	long lastupdate = 0;

	/* Assign class name of Next test case to variable */
	private Class tcNext = AccelerometerTest.class;

	/* Read passing gyro threshold limit from Sensor XML */
	ParseSpecs readSpecsLimit = new ParseSpecs();
	float buffer = readSpecsLimit.gyrobuffer;
	float averXDataInput = readSpecsLimit.gyroavgX;
	float averYDataInput = readSpecsLimit.gyroavgY;
	float averZDataInput = readSpecsLimit.gyroavgZ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		this.TAG = "Hi-P_Gyroscope";

		super.onCreate(savedInstanceState);
		setContentView(R.layout.gyroscope_test);

		/* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");

		// Keep screen On
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		// Initialize UI
		xAxis = (TextView) findViewById(R.id.gyro_axis_x);
		yAxis = (TextView) findViewById(R.id.gyro_axis_y);
		zAxis = (TextView) findViewById(R.id.gyro_axis_z);
		tvStatus = (TextView) findViewById(R.id.tvGyroStatus);
		bLogValue = (Button) findViewById(R.id.bGyroLogValue);

		bLogValue.setOnClickListener(onClickListener);		
		

		rlGryoLayout = (RelativeLayout) findViewById(R.id.rlGyroLayout);

		// Get a hook to sensor service
		sManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		gyroscope = sManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

		// Create sensor data file
		createSensorDataFile();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		sManager.registerListener(this, gyroscope, sManager.SENSOR_DELAY_UI);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		sManager.unregisterListener(this);
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
		// Do nothing

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// When sensor changed
		long curTime = System.currentTimeMillis();

		if (curTime - lastupdate > 100) {
			if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
				gryoData = event.values;
				gyroAxis.setxAxis(event.values[0]);
				gyroAxis.setyAxis(event.values[1]);
				gyroAxis.setzAxis(event.values[2]);

				// Save sensor data
				// saveSensorData(gryoData);

				setValuetoTextView(gyroAxis);
			}
			lastupdate = curTime;
		}

	}

	private void setValuetoTextView(Axis a) {
		// TODO Auto-generated method stub
		xAxis.setText(String.valueOf(a.getxAxis()));
		yAxis.setText(String.valueOf(a.getyAxis()));
		zAxis.setText(String.valueOf(a.getzAxis()));

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.d(this.TAG, "keyCode = " + keyCode);
		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			finish();

			if (this.testMode == 1) {
				/* Set test result status */
				testCaseResult = "PASS"; 

				Intent nIntent = new Intent(this, tcNext);
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);

			} else /* Individual test case */
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}

		} else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
				&& (this.testMode == 1)) {
			testCaseResult = "FAIL";
			showQuitTestAlert();

		}else if (keyCode == KeyEvent.KEYCODE_CLEAR){ 
			/* Detect keyevent keycode clear to proceed in test */					
			Log.d(TAG, "Keycode detected with testState: "+currentTestState);
			
			averXData = 0;
			averYData = 0;
			averZData = 0;
			logTime = 0;
			isLogging = true;

			/*  If retry after complete */
			if(currentTestState.equals("complete")){
				Log.d(TAG, "Restart testcase");
				testAxis = 1; //reset test axis to x
				currentTestState = "ready"; // reset test state to ready
				rlGryoLayout.setBackgroundColor(Color.BLACK);
			}

			if (currentTestState.equals("ready")){//If not busying start the logging
				if(testAxis == 1){//start x axis
					Log.d(TAG, "Start x axis");
					tvStatus.setText("X AXIS logging. Please Wait");	
					hDataLogging.postDelayed(rLogData, 0);	
					bLogValue.setEnabled(false);
				}else if (testAxis == 2){
					Log.d(TAG, "Start y axis");
					tvStatus.setText("Y AXIS logging. Please Wait");		
					hDataLogging.postDelayed(rLogData, 0);	
					bLogValue.setEnabled(false);
				}else if (testAxis == 3){
					Log.d(TAG, "Start z axis");
					tvStatus.setText("Y AXIS logging. Please Wait");
					hDataLogging.postDelayed(rLogData, 0);	
					bLogValue.setEnabled(false);
				}else{
					//error
				}					
			}else{
				Toast.makeText(this, "BUSY, Test in progress. Please retry", 0)
				.show();
			}



		}else {
			/* Reject other keyevent */
			Toast.makeText(this, getString(R.string.mode_continue_next), 0)
			.show();
		}

		if (!(testCaseResult.equals("Not Tested"))) {
			if (testCaseResult.equals("PASS"))
				db.setTestCasePassFail(Test_Main.tcGyro, true);
			if (testCaseResult.equals("FAIL"))
				db.setTestCasePassFail(Test_Main.tcGyro, false);

			/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr
					+ testCaseResult);

			try {
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter(
						Test_Main.sensorTestResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return true;
	}	

	private void showQuitTestAlert() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");

		/* Set dialog message */
		alertDialogBuilder
		.setMessage("Click YES to Quit, NO to Proceed Next")
		.setCancelable(false)

		.setNegativeButton("No",new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog,int id) 
			{
				/* Failed test but proceed to next test case */
				Intent nIntent = new Intent(GyroscopeTest.this, tcNext);
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);

				/* End current activity */
				finish();
			}
		})

		.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				/*
								Intent homescreenIntent = new Intent(
										Intent.ACTION_MAIN);
								homescreenIntent
										.addCategory(Intent.CATEGORY_HOME);
								homescreenIntent
										.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				 */
				Intent nIntent = new Intent(GyroscopeTest.this, Test_Main.class);
				startActivity(nIntent);

				/* End current activity */
				finish();
			}
		});

		/* Create alert dialog */
		AlertDialog alertDialog = alertDialogBuilder.create();

		/* Show it */
		alertDialog.show();
	}

	private void createSensorDataFile() {
		ContextWrapper contextWrapper = new ContextWrapper(
				getApplicationContext());
		File directory = contextWrapper.getDir(filepath, Context.MODE_PRIVATE);

		/*
		 * Create the full path to file in
		 * /data/data/com.hip.cqa/app_log/testLog
		 */
		sensorDataFile = new File(directory, filename);
		// sensorDataFile= new File("/sdcard/EcompassData/"+filename);

		if (sensorDataFile.exists()) {
			/* Delete existing testLog */
			sensorDataFile.delete();
		}

		// Save the title
		BufferedWriter buf;
		try {
			buf = new BufferedWriter(new FileWriter(sensorDataFile, true));
			StringBuilder builder = new StringBuilder("");
			builder.append("time (dd-MM-yyyy hh:mm:ss)").append(",")
			.append("Gyroscope x m/s^2").append(",")
			.append("Gyroscope y m/s^2").append(",")
			.append("Gyroscope z m/s^2");
			buf.append(builder.toString());
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void saveSensorData(float[] data) {

		// Set current date&time
		String TestTime = date.format(new Date());

		StringBuilder builder = new StringBuilder("");
		builder.append(TestTime).append(",");

		try {
			BufferedWriter buf = new BufferedWriter(new FileWriter(
					sensorDataFile, true));
			builder.append(data[0]).append(",").append(data[1]).append(",")
			.append(data[2]);
			buf.append(builder.toString());
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/* Define class for onClickListener */
	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			averXData = 0;
			averYData = 0;
			averZData = 0;
			logTime = 0;
			isLogging = true;

			/*  If retry after complete */
			if(currentTestState.equals("complete")){
				Log.d(TAG, "Restart testcase");
				testAxis = 1; //reset test axis to x
				currentTestState = "ready"; // reset test state to ready
				rlGryoLayout.setBackgroundColor(Color.WHITE);
			}

			if (currentTestState.equals("ready")){//If not busying start the logging
				if(testAxis == 1){//start x axis
					Log.d(TAG, "Start x axis");
					tvStatus.setText("X AXIS logging. Please Wait");	
					hDataLogging.postDelayed(rLogData, 0);	
					bLogValue.setEnabled(false);
				}else if (testAxis == 2){
					Log.d(TAG, "Start y axis");
					tvStatus.setText("Y AXIS logging. Please Wait");		
					hDataLogging.postDelayed(rLogData, 0);	
					bLogValue.setEnabled(false);
				}else if (testAxis == 3){
					Log.d(TAG, "Start z axis");
					tvStatus.setText("Y AXIS logging. Please Wait");
					hDataLogging.postDelayed(rLogData, 0);	
					bLogValue.setEnabled(false);
				}else{
					//error
				}					
			}else{
				//error
			}
		}

	};

	public Runnable rLogData = new Runnable() {

		@Override
		public void run() {
			Log.d(TAG, "Logging...");
			// Log data 0.5sec per data
			// Save sensor data
			saveSensorData(gryoData);
			averXData += gryoData[0];
			averYData += gryoData[1];
			averZData += gryoData[2];

			String TestTime = date.format(new Date());
			Log.d(TAG, "Data log at: " + TestTime);

			logTime++;
			if (logTime < numofData)
				hDataLogging.postDelayed(rLogData, logDelay);
			else
				hDataLogging.postDelayed(rDoneLog, logDelay);
		}

	};

	public Runnable rDoneLog = new Runnable() {

		@Override
		public void run() {			
			isLogging = false;
			bLogValue.setEnabled(true);
			tvStatus.setText("Calculating.. \n Average XYZ: " + averXData/10 + " \n" + averYData/10 + " \n" + averZData/10);
			StringBuilder builder = new StringBuilder("");
			builder.append("Average value").append(":");

			try {
				BufferedWriter buf = new BufferedWriter(new FileWriter(
						sensorDataFile, true));
				builder.append("X: ").append(averXData/10).append(",")
				.append("Y: ").append(averYData/10).append(",")
				.append("Z: ").append(averZData/10).append("\n");
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Make judgement whether gyroscope pass fail			
			switch (testAxis){
			case 1: //x axis
				if(((averXData< averXDataInput+buffer)&&(averXData> averXDataInput-buffer))
						&&((averYData< buffer)&&(averYData> -buffer)) //0 +- buffer
						&&((averZData< +buffer)&&(averZData> -buffer)) //0 +- buffer
						){
					/* PASS */
					Log.d(TAG, "x PASS");
					rlGryoLayout.setBackgroundColor(Color.CYAN);
					tvStatus.setText("X AXIS PASS. Please proceed Y axis test");				
					currentTestState = "ready"; 
					testAxis = 2; 	
				}else{
					/* FAIL */
					Log.d(TAG, "x FAIL");
					rlGryoLayout.setBackgroundColor(Color.RED);
					tvStatus.setText("X AXIS FAIL");				
					testCaseResult = "FAIL";
					currentTestState = "complete"; 
					testAxis = 1; 	
				}								
				break;

			case 2:
				if(((averYData< averYDataInput+buffer)&&(averYData> averYDataInput-buffer))
						&&((averXData< buffer)&&(averXData> -buffer)) //0 +- buffer
						&&((averZData< +buffer)&&(averZData> -buffer)) //0 +- buffer
						){
					/* PASS */
					Log.d(TAG, "y PASS");
					rlGryoLayout.setBackgroundColor(Color.CYAN);
					tvStatus.setText("Y AXIS PASS. Please proceed Z axis test");				
					currentTestState = "ready"; 
					testAxis = 3; 	
				}else{
					/* FAIL */
					Log.d(TAG, "y FAIL");
					rlGryoLayout.setBackgroundColor(Color.RED);
					tvStatus.setText("Y AXIS FAIL");				
					testCaseResult = "FAIL";
					currentTestState = "complete"; 
					testAxis = 1; 	
				}				
				break;

			case 3: //z axis
				if(((averZData< averZDataInput+buffer)&&(averZData> averZDataInput-buffer))
						&&((averYData< buffer)&&(averYData> -buffer)) //0 +- buffer
						&&((averXData< +buffer)&&(averXData> -buffer)) //0 +- buffer
						){
					/* PASS */
					Log.d(TAG, "z PASS");
					rlGryoLayout.setBackgroundColor(Color.GREEN);
					tvStatus.setText("Test PASS");				
					currentTestState = "complete"; 
					testAxis = 1; 	
				}else{
					/* FAIL */
					Log.d(TAG, "z FAIL");
					rlGryoLayout.setBackgroundColor(Color.RED);
					tvStatus.setText("Z AXIS FAIL");				
					testCaseResult = "FAIL";
					currentTestState = "complete"; 
					testAxis = 1; 	
				}				
				break;

			default:
				//error
				break;				
			}
			
			bLogValue.setEnabled(true);
		}
	};

}
