/**
 * Revision History
 * ----------------
 * 03-SEP-2013 - Initial Release. Added for P2B 
 */

package com.hip.cqa.gyro;

public class Axis {
	
	private float xAxis, yAxis, zAxis;

	public Axis(float xAxis, float yAxis, float zAxis) {
		super();
		this.xAxis = xAxis;
		this.yAxis = yAxis;
		this.zAxis = zAxis;
	}
	
	public Axis(){
		super();
	}

	public float getxAxis() {
		return xAxis;
	}

	public void setxAxis(float xAxis) {
		this.xAxis = xAxis;
	}

	public float getyAxis() {
		return yAxis;
	}

	public void setyAxis(float yAxis) {
		this.yAxis = yAxis;
	}

	public float getzAxis() {
		return zAxis;
	}

	public void setzAxis(float zAxis) {
		this.zAxis = zAxis;
	}
	

}
