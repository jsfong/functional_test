package com.hip.cqa.gyro;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hip.cqa.R;
import com.hip.cqa.Test_All_Mode;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.Test_Main;
import com.hip.cqa.acc.AccelerometerTest;
import com.hip.cqa.acc.AccelerometerTestSimple;

public class GyroscopeTestSimple extends Test_Main implements
		SensorEventListener {

	// UI
	TextView xAxis, yAxis, zAxis, tvStatus;
	private static Axis gyroAxis = new Axis();
	Button bLogValue;
	private static int State = 0; // 0 - normal 1- stared
	static Handler hDataLogging = new Handler();
	private static boolean isLogging = false;
	RelativeLayout rlGryoLayout;

	// Sensor Manager
	private SensorManager sManager;
	private Sensor gyroscope;
	private int testMode;
	float gryoData[];
	private float averXData = 0;
	private float averYData = 0;
	private float averZData = 0;

	private float averXDataInput = 0;// TODO: read from xml
	private float averYDataInput = 0;// TODO: read from xml
	private float averZDataInput = 0;// TODO: read from xml
	private float buffer = 0;// TODO: read from xml

	private static String testCaseStr = "Gyroscope Test - ";
	private static String testCaseResult = "Not Tested";
	final Context context = this;
	private String TAG;

	long lastupdate = 0;

	/* Assign class name of Next test case to variable */
	private Class tcNext = AccelerometerTestSimple.class;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		this.TAG = "Hi-P_Gyroscope";

		super.onCreate(savedInstanceState);
		setContentView(R.layout.gyroscope_test);

		/* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");

		// Keep screen On
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		// Initialize UI
		xAxis = (TextView) findViewById(R.id.gyro_axis_x);
		yAxis = (TextView) findViewById(R.id.gyro_axis_y);
		zAxis = (TextView) findViewById(R.id.gyro_axis_z);
		tvStatus = (TextView) findViewById(R.id.tvGyroStatus);
		bLogValue = (Button) findViewById(R.id.bGyroLogValue);
		tvStatus.setVisibility(View.GONE);
		bLogValue.setVisibility(View.GONE);

		rlGryoLayout = (RelativeLayout) findViewById(R.id.rlGyroLayout);

		// Get a hook to sensor service
		sManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		gyroscope = sManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

		

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		sManager.registerListener(this, gyroscope, sManager.SENSOR_DELAY_UI);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		sManager.unregisterListener(this);
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
		// Do nothing

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// When sensor changed
		long curTime = System.currentTimeMillis();

		if (curTime - lastupdate > 100) {
			if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
				gryoData = event.values;
				gyroAxis.setxAxis(event.values[0]);
				gyroAxis.setyAxis(event.values[1]);
				gyroAxis.setzAxis(event.values[2]);

				// Save sensor data
				// saveSensorData(gryoData);

				setValuetoTextView(gyroAxis);
			}
			lastupdate = curTime;
		}

	}

	private void setValuetoTextView(Axis a) {
		// TODO Auto-generated method stub
		xAxis.setText(String.valueOf(a.getxAxis()));
		yAxis.setText(String.valueOf(a.getyAxis()));
		zAxis.setText(String.valueOf(a.getzAxis()));

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.d(this.TAG, "keyCode = " + keyCode);
		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			finish();

			if (this.testMode == 1) {
				/* Set test result status */
				testCaseResult = "PASS";

				Intent nIntent = new Intent(this, tcNext);
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);

			} else /* Individual test case */
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}

		} else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
				&& (this.testMode == 1)) {
			testCaseResult = "FAIL";
			showQuitTestAlert();

		} else {
			/* Reject other keyevent */
			Toast.makeText(this, getString(R.string.mode_continue_next), 0)
					.show();
		}

		if (!(testCaseResult.equals("Not Tested"))) {
			if (testCaseResult.equals("PASS"))
				db.setTestCasePassFail(Test_Main.tcGyro, true);
			if (testCaseResult.equals("FAIL"))
				db.setTestCasePassFail(Test_Main.tcGyro, false);

			/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr
					+ testCaseResult);

			try {
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter(
						Test_All_Mode.testResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return true;
	}

	private void showQuitTestAlert() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");

		/* Set dialog message */
		alertDialogBuilder
				.setMessage("Click YES to Quit, NO to Proceed Next")
				.setCancelable(false)
				
				.setNegativeButton("No",new DialogInterface.OnClickListener() 
			    {
				    public void onClick(DialogInterface dialog,int id) 
				    {
					    /* Failed test but proceed to next test case */
    				    Intent nIntent = new Intent(GyroscopeTestSimple.this, tcNext);
    				    nIntent.putExtra("testModeType", 1);
    				    nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				    startActivity(nIntent);
	    		
					    /* End current activity */
					    finish();
				    }
			    })
			
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								/*
								 * Intent homescreenIntent = new Intent(
								 * Intent.ACTION_MAIN); homescreenIntent
								 * .addCategory(Intent.CATEGORY_HOME);
								 * homescreenIntent
								 * .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								 */
								Intent nIntent = new Intent(GyroscopeTestSimple.this,
										Test_Main.class);
								startActivity(nIntent);

								/* End current activity */
								finish();
							}
						});

		/* Create alert dialog */
		AlertDialog alertDialog = alertDialogBuilder.create();

		/* Show it */
		alertDialog.show();
	}

}
