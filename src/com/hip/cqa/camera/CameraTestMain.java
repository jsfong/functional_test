/**
 * Revision History
 * ----------------
 * 13-AUG-2013 - Initial Draft
 * 04-SEP-2013 - Call Gyroscope for P2B
 */

package com.hip.cqa.camera;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.hip.cqa.R;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.Test_Main;
import com.hip.cqa.Test_All_Mode;
import com.hip.cqa.acc.AccelerometerTest;
import com.hip.cqa.gyro.GyroscopeTest;
import com.hip.cqa.nfc.NfcTest;

/* Main entry point for camera test */ 
public class CameraTestMain extends Test_Main
{
	
	// Variable
	Button rearCamera, frontCamera;
	private int testMode;
	private static String testCaseStr = "Camera Test - ";
	private static String testCaseResult = "Not Tested";
	final Context context = this;

	/* Assign class name of Next test case to variable */
	private Class tcNext = NfcTest.class;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.camera_main);
		
		rearCamera = (Button)findViewById(R.id.button_camera_rear);
		frontCamera = (Button)findViewById(R.id.button_camera_front);
		
		/* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");
		
		rearCamera.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent rearCameraIntent = new Intent(getBaseContext(), CameraTest.class);
				rearCameraIntent.putExtra("testModeType", CameraTestMain.this.testMode); /* Test_Individual_Mode = 2 */
				rearCameraIntent.putExtra("camera_no", 0);
				startActivity(rearCameraIntent);
			}
			
		});
		
		frontCamera.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent frontCameraIntent = new Intent(getBaseContext(), CameraTest.class);
				frontCameraIntent.putExtra("testModeType", CameraTestMain.this.testMode); /* Test_Individual_Mode = 2 */
				frontCameraIntent.putExtra("camera_no", 1);
				startActivity(frontCameraIntent);
			}
			
		});
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event)
    {
    	if(keyCode == KeyEvent.KEYCODE_VOLUME_UP)
    	{
    		finish();
    		
			if (this.testMode == 1)
			{
				/* Set test result status */
				testCaseResult = "PASS"; 
				
				Intent nIntent = new Intent(this, tcNext); 
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
				
			}
			else // Individual test case
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}

    	}
    	else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) && (this.testMode == 1))
    	{
    		testCaseResult = "FAIL";
    		showQuitTestAlert();

    	}
    	else
    	{
    		/* Reject other keyevent */
    		Toast.makeText(this, getString(R.string.mode_continue_next), 0).show();
    	}
    	

    	if(!(testCaseResult.equals("Not Tested")))
    	{
    		if(testCaseResult.equals("PASS"))
    			db.setTestCasePassFail(Test_Main.tcCamera,true);
    		if(testCaseResult.equals("FAIL"))
    			db.setTestCasePassFail(Test_Main.tcCamera,false);
    		
    		/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr + testCaseResult);

			try 
			{
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter
						(Test_All_Mode.testResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}   
    	}
    	
    	return true;
    }  
	
    private void showQuitTestAlert()
    {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
 
		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");
		
		/* Set dialog message */
		alertDialogBuilder
			.setMessage("Click YES to Quit, NO to Proceed Next")
			.setCancelable(false)
			
			.setNegativeButton("No",new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog,int id) 
				{
					/* Failed test but proceed to next test case */
    				Intent nIntent = new Intent(CameraTestMain.this, tcNext);
    				nIntent.putExtra("testModeType", 1);
    				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				startActivity(nIntent);
	    		
					/* End current activity */
					finish();
				}
			})
			
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog,int id) 
				{
					/*
					Intent homescreenIntent = new Intent(
							Intent.ACTION_MAIN);
					homescreenIntent
							.addCategory(Intent.CATEGORY_HOME);
					homescreenIntent
							.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							*/
					Intent nIntent = new Intent(CameraTestMain.this, Test_Main.class);
					startActivity(nIntent);
		    		
		    		/* End current activity */
		    		finish();
				}
			 });

			/* Create alert dialog */
			AlertDialog alertDialog = alertDialogBuilder.create();
		 
			/* Show it */
			alertDialog.show();
    }	

	protected void onStop()
	{
		super.onStop();
		finish();
	}
	
}
