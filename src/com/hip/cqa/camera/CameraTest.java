/**
 * Revision History
 * ----------------
 * 13-AUG-2013 - Initial Draft
 * 28-AUG-2013 - Remove 180 degree rotation for preview image
 * 07-JUN-2014 - Add camera.setDisplayOrientation(180) for front camera preview for Factory_P2 sw
 */


package com.hip.cqa.camera;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import com.hip.cqa.R;

public class CameraTest extends Activity implements SurfaceHolder.Callback {

	private static final String TAG = "Hi-P Camera Test";
	// Variable
	static private SurfaceView surfaceview;
	static private SurfaceHolder surfaceholder;
	static private Camera camera;
	static private int num_of_camera;
	static private int camera_no = 0;
	private Window mywindow;
	private WindowManager.LayoutParams lp;

	static boolean previewing = false;
	private Handler h1 = new Handler();
	private Handler hPrintToast = new Handler();
	private int testMode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.camera_viewfinder);

		// Initialize
		/* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");
		this.camera_no = testModeSelected.getInt("camera_no");
		
		Log.d(this.TAG, "camera_no" + this.camera_no);
		
		
		// Initialize camera
		getWindow().setFormat(PixelFormat.UNKNOWN);
		surfaceview = (SurfaceView) findViewById(R.id.svViewFinder);
		surfaceholder = surfaceview.getHolder();
		surfaceholder.addCallback(this);
		surfaceholder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		num_of_camera = Camera.getNumberOfCameras();

		// Check camera availability
		if (!getPackageManager()
				.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
			Toast.makeText(this, "No camera on this device", Toast.LENGTH_SHORT)
					.show();
		}

		Toast.makeText(this, "Camera Test start", Toast.LENGTH_SHORT).show();
		h1.postDelayed(EnableCamera, 1000);
	}

	Runnable EnableCamera = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			if (startRearCamera() != 0)
				hPrintToast.post(rPrintToastError);
		}

	};

	private int startRearCamera() {
		// TODO Auto-generated method stub
		if (!previewing) {

			// Start camera previewing
			camera = Camera.open(camera_no);
			Log.i(TAG, "Camera " + camera_no + " Enable");
			if (camera != null) {

				try {
					surfaceview.setVisibility(View.VISIBLE);
					
					/* Factory_P2 SW - Rotate image 180 for front camera only
					 * camera_no = 0 -> Rear camera
					 * camera_no = 1 -> Front camera
					 */
					
					if (camera_no == 1) /* Front camera */
					{
						camera.setDisplayOrientation(180);
					}
					camera.setPreviewDisplay(surfaceholder);
					camera.startPreview();
					previewing = true;

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				Log.e(TAG, "Camera " + camera_no + " not found");
				return -1;
			}
		}
		return 0;
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBackPressed() 
	{
		// TODO Auto-generated method stub
		Toast.makeText(this, R.string.mode_continue_next, 0).show();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
    {
    	if(keyCode == KeyEvent.KEYCODE_VOLUME_UP)
    	{
			if (previewing) 
			{
				camera.stopPreview();
				camera.release();
				camera = null;
				previewing = false;
			}
			
			finish();
			
			Intent nIntent = new Intent(this, CameraTestMain.class);
			nIntent.putExtra("testModeType", this.testMode); //Return testModeType to CameraTestMain.class
			nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(nIntent);

    	}
    	else if((keyCode == KeyEvent.KEYCODE_BACK) || (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN))
    	{
    		Toast.makeText(this, getString(R.string.mode_continue_next), 0).show();
    	}
    	return true;
    }  

	Runnable rPrintToastError = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			Toast.makeText(CameraTest.this,
					"Error opening camera: " + camera_no, Toast.LENGTH_LONG)
					.show();
		}

	};

}
