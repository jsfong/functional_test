/**
 * Revision History
 * ----------------
 * 30-APR-2013 - Initial Release. Added for P1B. @author JS.Fong 
 * 25-MAY-2014 - Added function to read accelerometer spec threshold from xml (/data/specs_vX.xml)
 */

package com.hip.cqa.acc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hip.cqa.R;
import com.hip.cqa.Test_All_Mode;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.Test_Main;
import com.hip.cqa.ecompass.EcompassTest;
import com.hip.cqa.parse.specs.ParseSpecs;

public class AccelerometerTest extends Test_Main implements SensorEventListener{

	private int testMode;
	private static String testCaseStr = "Accelerometer Test - ";
	private static String testCaseResult = "Not Tested";
	final Context context = this;

	
	//UI
	TextView tvAccX;
	TextView tvAccY;
	TextView tvAccZ;
	TextView tvAxis;
	TextView tvAccStatus;
	RelativeLayout rl;
	Button bLogAccValue;
	
	//Control
	boolean isX = false;
	boolean isY = false;
	boolean isZ = false;

	//Sensor
	private SensorManager mAccSensorManager;
	private  Sensor accelerometer;	
	private final  double gravity = 9.81;
	private long lastupdate = 0;
	float accData [];
	float accXData, accYData, accZData;

	//File saving
	private static String filename = "AccSensorLog.csv";
	public static File sensorDataFile;
	private String filepath = "log";

	/* Time */
	@SuppressLint("SimpleDateFormat")
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

	/* Assign class name of Next test case to variable */
	private Class tcNext = EcompassTest.class;
	
	
	/* Read passing accelerometer threshold limit from Sensor XML */
	ParseSpecs readSpecsLimit = new ParseSpecs();
	float accThreshold = readSpecsLimit.accelerometeraccthreshold;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acc);
		
		/* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");

		tvAccX = (TextView)findViewById(R.id.tvAccX);
		tvAccY = (TextView)findViewById(R.id.tvAccY);
		tvAccZ = (TextView)findViewById(R.id.tvAccZ);
		tvAxis = (TextView)findViewById(R.id.tvAxis);
		tvAccStatus = (TextView)findViewById(R.id.tvAccStatus);
		
		rl = (RelativeLayout)findViewById(R.id.rlAccBackground);

		this.mAccSensorManager = ((SensorManager) getSystemService(Context.SENSOR_SERVICE));
		accelerometer = this.mAccSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		
		bLogAccValue = (Button)findViewById(R.id.bLogAcc);
		bLogAccValue.setOnClickListener(onClickListener);

		//Create sensor data file
		createSensorDataFile();
		
		//Start X axis test
		isX = true;
		isY = false;
		isZ = false;
		
		//Start Instruction
		tvAccStatus.setText(getString(R.string.x_axis));			
	}

	@Override
	protected void onResume() {		
		super.onResume();
		//Register Sensor listener		
		mAccSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
	}

	@Override
	protected void onPause() {		
		super.onPause();
		mAccSensorManager.unregisterListener(this);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// When sensor changed
		if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
			 accData  = event.values;
			///Log.d(TAG, "values: "+ accData[0]+" "+ accData[1]+" "+ accData[2]);
			
			long curTime = System.currentTimeMillis();
			if(curTime - lastupdate  > 100){
				
				//Save sensor data
				//saveAccSensorData(accData); //Do not save data 
				
				//Checking
				checkAxis(accData);
				
				//Display
				tvAccX.setText("X: "+accData[0]);
				tvAccY.setText("Y: "+accData[1]);
				tvAccZ.setText("Z: "+accData[2]);
				
				lastupdate = curTime;
			}		
		}

	}

	private void checkAxis(float[] accData) {
		// Check phone axis	
		if(Math.sqrt(
				accData[0]*accData[0]+
				accData[1]*accData[1]+
				accData[2]*accData[2]
				) < 15){
			
			
			if(accData[0] > 9){
				tvAxis.setText("X");
				//rl.setBackgroundColor(Color.GREEN);
			}else if(accData[1] > 9){
				tvAxis.setText("Y");
				//rl.setBackgroundColor(Color.GREEN);
			}else if(accData[2] > 9){
				tvAxis.setText("Z");
				//rl.setBackgroundColor(Color.GREEN);
			}else if(accData[0] < -9){
				tvAxis.setText("-X");
				//rl.setBackgroundColor(Color.GREEN);
			}else if(accData[1] < -9){
				tvAxis.setText("-Y");
				//rl.setBackgroundColor(Color.GREEN);
			}else if(accData[2] < -9){
				tvAxis.setText("-Z");
				//rl.setBackgroundColor(Color.GREEN);
			}else{
				tvAxis.setText("Turn to any Axis");
				//rl.setBackgroundColor(0x00000000);
			}
			
		}else{
			tvAxis.setText("Please hold the unit still");
			//rl.setBackgroundColor(Color.RED);
		}
			
			
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// Not using

	}

	private void createSensorDataFile() {
		ContextWrapper contextWrapper = new ContextWrapper(getApplicationContext());
		File directory = contextWrapper.getDir(filepath, Context.MODE_PRIVATE);

		/* Create the full path to file in /data/data/com.hip.cqa/app_log/testLog */
		sensorDataFile = new File(directory, filename); 
		//sensorDataFile= new File("/sdcard/EcompassData/"+filename);	

		if (sensorDataFile.exists())
		{
			/* Delete existing testLog */
			sensorDataFile.delete();
		} 

		//Save the title
		BufferedWriter buf;
		try {
			buf = new BufferedWriter(new FileWriter(sensorDataFile, true));
			StringBuilder builder = new StringBuilder("");
			builder.append("time (dd-MM-yyyy hh:mm:ss)")
			.append(",")
			.append("accerelometer x m/s^2")
			.append(",")
			.append("accerelometer y m/s^2")
			.append(",")
			.append("accerelometer z m/s^2");			
			buf.append(builder.toString());
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		

	}

	private void saveAccSensorData(float[] acc) {

		//Set current date&time		
		String TestTime = date.format(new Date());

		StringBuilder builder = new StringBuilder("");
		builder.append(TestTime).append(",");

		try {
			BufferedWriter buf = new BufferedWriter(new FileWriter(sensorDataFile, true));
			builder.append(acc[0])
			.append(",")
			.append(acc[1])
			.append(",")
			.append(acc[2]);						
			buf.append(builder.toString());
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void onDestroy() {
		super.onDestroy();
		mAccSensorManager.unregisterListener(this);
	}

	private void showQuitTestAlert() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");

		/* Set dialog message */
		alertDialogBuilder
				.setMessage("Click YES to Quit, NO to Proceed Next")
				.setCancelable(false)
				
				.setNegativeButton("No",new DialogInterface.OnClickListener() 
			    {
				    public void onClick(DialogInterface dialog,int id) 
				    {
					    /* Failed test but proceed to next test case */
    				    Intent nIntent = new Intent(AccelerometerTest.this, tcNext);
    				    nIntent.putExtra("testModeType", 1);
    				    nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				    startActivity(nIntent);
	    		
					    /* End current activity */
					    finish();
				    }
			    })
			
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								/* Return to Android Home Screen */
								/*
								Intent homescreenIntent = new Intent(
										Intent.ACTION_MAIN);
								homescreenIntent
										.addCategory(Intent.CATEGORY_HOME);
								homescreenIntent
										.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
										*/
								Intent nIntent = new Intent(AccelerometerTest.this, Test_Main.class);
								startActivity(nIntent);
								
					    		/* End current activity */							
					    		finish();
							}
						});

		/* Create alert dialog */
		AlertDialog alertDialog = alertDialogBuilder.create();

		/* Show it */
		alertDialog.show();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) 
		{
			finish();
	
			/* Must unregister sensor first before calling for AmbientLightTest */
			mAccSensorManager.unregisterListener(this);
			
			if (this.testMode == 1) {
				/* Set test result status */
				//testCaseResult = "PASS"; //PASS&FAIL set by judging accelerometer value, not vol button
	
				Intent nIntent = new Intent(this, tcNext);
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);				
				
			} else /* Individual test case */
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}
		} else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
				&& (this.testMode == 1)) {
			//testCaseResult = "FAIL"; //PASS&FAIL set by judging accelerometer value, not vol button
			showQuitTestAlert();
	
		} else if (keyCode == KeyEvent.KEYCODE_CLEAR){ 
			/* Detect keyevent keycode clear to proceed in test */	
			logAcc();
		}else {
			/* Reject other keyevent */
			Toast.makeText(this, getString(R.string.mode_continue_next), 0)
					.show();
		}			
			
		if(!(testCaseResult.equals("Not Tested"))&& (this.testMode == 1))
    	{
			if(testCaseResult.equals("PASS"))
    			db.setTestCasePassFail(Test_Main.tcAcc,true);
    		if(testCaseResult.equals("FAIL"))
    			db.setTestCasePassFail(Test_Main.tcAcc,false);
    		
			/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr
					+ testCaseResult);
	
			try {
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter(
						Test_Main.sensorTestResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
    	}
		return true;
	}
	
	/* Define class for onClickListener */
	private OnClickListener onClickListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
			//When log value click
			logAcc();

		}
		
	};
	
	public boolean logAcc(){
		
		if(isX){
			//Log the luv values
			accXData = accData[0];
			Log.d(TAG, "accXdata: "+accXData);
			
			saveAccSensorData(accData);
			
			//Start Instruction
			tvAccStatus.setText(getString(R.string.y_axis));					

			isX = false;
			isY = true;
			isZ = false;
			
		}else if (isY){
			//Log the luv values
			accYData = accData[1];
			Log.d(TAG, "accXdata: "+accXData);
			saveAccSensorData(accData);
			
			//Start Instruction
			tvAccStatus.setText(getString(R.string.z_axis));					

			isX = false;
			isY = false;
			isZ = true;
			
		}else if (isZ){
			//Disable log button
			bLogAccValue.setEnabled(false);
			
			//Log the luv values
			accZData = accData[2];
			Log.d(TAG, "accXdata: "+accXData);
			saveAccSensorData(accData);
			
			//Making decision				
			if(isAccTestPass(accXData, accYData, accZData)){
				tvAccStatus.setText("Test PASS");	
				isX = false;
				isY = false;
				isZ = false;
				rl.setBackgroundColor(Color.GREEN);
				
				//Set test PASS
				testCaseResult = "PASS";
			}else{
				tvAccStatus.setText("Test FAIL, Place in X-axis and retry please");
				rl.setBackgroundColor(Color.RED);
				isX = true;
				isY = false;
				isZ = false;
				bLogAccValue.setEnabled(true);
				
				//Set test FAIL
				testCaseResult = "FAIL";
			}
		}else{
			return false;
			
		}		
		return true;		
	};
	
	private boolean isAccTestPass(float accXData, float accYData,
			float accZData) {
		float buffer = accThreshold;
		
		double xdata = gravity - Math.sqrt(accXData*accXData);
		double ydata = gravity - Math.sqrt(accYData*accYData);
		double zdata = gravity - Math.sqrt(accZData*accZData);
		Log.d(TAG, "Cal: x: "+xdata + " y: "+ ydata +" z:"+ zdata);
		
		if(
				(xdata < buffer) &&
				(ydata < buffer) &&
				(zdata < buffer) 
			)
			return true;
		else
			return false;				
		
	};
	


}
