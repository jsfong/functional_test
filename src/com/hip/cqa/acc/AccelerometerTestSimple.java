package com.hip.cqa.acc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hip.cqa.R;
import com.hip.cqa.Test_All_Mode;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.Test_Main;
import com.hip.cqa.ecompass.EcompassTest;
import com.hip.cqa.ecompass.EcompassTestSimple;

public class AccelerometerTestSimple extends Test_Main implements SensorEventListener{
	
	private int testMode;
	

	private static String testCaseStr = "Accelerometer Test - ";
	private static String testCaseResult = "Not Tested";
	final Context context = this;

	
	//UI
	TextView tvAccX;
	TextView tvAccY;
	TextView tvAccZ;
	TextView tvAxis;
	TextView tvAccStatus;
	RelativeLayout rl;
	Button bLogAccValue;
	
	//Control
	boolean isX = false;
	boolean isY = false;
	boolean isZ = false;

	//Sensor
	private SensorManager mAccSensorManager;
	private  Sensor accelerometer;	
	private final  double gravity = 9.81;
	private long lastupdate = 0;
	float accData [];
	
	/* Assign class name of Next test case to variable */
	private Class tcNext = EcompassTestSimple.class;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acc);
		
		/* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");

		tvAccX = (TextView)findViewById(R.id.tvAccX);
		tvAccY = (TextView)findViewById(R.id.tvAccY);
		tvAccZ = (TextView)findViewById(R.id.tvAccZ);
		tvAxis = (TextView)findViewById(R.id.tvAxis);
		tvAccStatus = (TextView)findViewById(R.id.tvAccStatus);
		tvAxis.setVisibility(View.GONE);
		tvAccStatus.setVisibility(View.GONE);
		
		
		rl = (RelativeLayout)findViewById(R.id.rlAccBackground);

		this.mAccSensorManager = ((SensorManager) getSystemService(Context.SENSOR_SERVICE));
		accelerometer = this.mAccSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		
		bLogAccValue = (Button)findViewById(R.id.bLogAcc);
		bLogAccValue.setVisibility(View.GONE);			
		
	}	

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected void onResume() {		
		super.onResume();
		//Register Sensor listener		
		mAccSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
	}

	@Override
	protected void onPause() {		
		super.onPause();
		mAccSensorManager.unregisterListener(this);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// When sensor changed
		if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
			 accData  = event.values;
			///Log.d(TAG, "values: "+ accData[0]+" "+ accData[1]+" "+ accData[2]);
			
			long curTime = System.currentTimeMillis();
			if(curTime - lastupdate  > 100){				
				
				//Display
				tvAccX.setText("X: "+accData[0]);
				tvAccY.setText("Y: "+accData[1]);
				tvAccZ.setText("Z: "+accData[2]);			
				
			}		
		}

	}
	
	protected void onDestroy() {
		super.onDestroy();
		mAccSensorManager.unregisterListener(this);
	}

	private void showQuitTestAlert() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");

		/* Set dialog message */
		alertDialogBuilder
				.setMessage("Click YES to Quit, NO to Proceed Next")
				.setCancelable(false)

				.setNegativeButton("No",new DialogInterface.OnClickListener() 
			    {
				    public void onClick(DialogInterface dialog,int id) 
				    {
					    /* Failed test but proceed to next test case */
    				    Intent nIntent = new Intent(AccelerometerTestSimple.this, tcNext);
    				    nIntent.putExtra("testModeType", 1);
    				    nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				    startActivity(nIntent);
	    		
					    /* End current activity */
					    finish();
				    }
			    })
			    
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								/* Return to Android Home Screen */
								/*
								Intent homescreenIntent = new Intent(
										Intent.ACTION_MAIN);
								homescreenIntent
										.addCategory(Intent.CATEGORY_HOME);
								homescreenIntent
										.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
										*/
								Intent nIntent = new Intent(AccelerometerTestSimple.this, Test_Main.class);
								startActivity(nIntent);
								
					    		/* End current activity */							
					    		finish();
							}
						});

		/* Create alert dialog */
		AlertDialog alertDialog = alertDialogBuilder.create();

		/* Show it */
		alertDialog.show();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) 
		{
			finish();
	
			/* Must unregister sensor first before calling for AmbientLightTest */
			mAccSensorManager.unregisterListener(this);
			
			if (this.testMode == 1) {
				/* Set test result status */
				testCaseResult = "PASS"; 
	
				Intent nIntent = new Intent(this, tcNext);
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);				
				
			} else /* Individual test case */
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}
		} else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
				&& (this.testMode == 1)) {
			testCaseResult = "FAIL"; 
			showQuitTestAlert();
	
		} else {
			/* Reject other keyevent */
			Toast.makeText(this, getString(R.string.mode_continue_next), 0)
					.show();
		}			
			
		if(!(testCaseResult.equals("Not Tested"))&& (this.testMode == 1))
    	{
			if(testCaseResult.equals("PASS"))
    			db.setTestCasePassFail(Test_Main.tcAcc,true);
    		if(testCaseResult.equals("FAIL"))
    			db.setTestCasePassFail(Test_Main.tcAcc,false);
    		
			/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr
					+ testCaseResult);
	
			try {
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter(
						Test_All_Mode.testResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
    	}
		return true;
	}

}
