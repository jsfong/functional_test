/**
 * Revision History
 * ----------------
 * 13-AUG-2013 - Initial Draft
 */

package com.hip.cqa.touch;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hip.cqa.R;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.Test_Main;
import com.hip.cqa.acc.AccelerometerTest;
import com.hip.cqa.camera.CameraTestMain;
import com.hip.cqa.Test_All_Mode;

public class MainActivity extends Test_Main implements OnClickListener {

	// Variable
	private static final String TAG = "Hi-P Touch Main";
	private TextView intro, status, lastTestedDate;
	private Button start;
	public Editor editor;
	private int testMode;
	static boolean exitTest = true;
	
	private static String testCaseStr = "Touch Panel Test - ";
	static String testCaseResult = "Not Tested";
	final Context context = this;

	/* Assign class name of Next test case to variable */
	private Class tcNext = CameraTestMain.class;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.activity_main);
		setContentView(R.layout.touch_main);

		/* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");
		
		// UI initialize
		status = (TextView) findViewById(R.id.tvStatus);
		lastTestedDate = (TextView) findViewById(R.id.tvLastTestDate);
		start = (Button) findViewById(R.id.bStart);
		start.setOnClickListener(this);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.d(TAG, "onResume");

		// Read SharedPreferences
		SharedPreferences sharedPref = this.getSharedPreferences(
				"com.hip.cqa.touch", Context.MODE_PRIVATE);
		String TestResult = "com.hip.cqa.touch.testResult";		
		String TestDateTime = "com.hip.cqa.touch.time";
		int pattern = sharedPref.getInt(TestResult, 0);
		String dateTime = sharedPref.getString(TestDateTime, null);
		if (pattern != 0) {
			status.setText("Touch Test Passed :");
			lastTestedDate.setText(dateTime);
		}

	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}*/

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// When click attach a draw view on top of this activity		
		exitTest = false;
		setContentView(R.layout.touch_activity_main);
		LinearLayout ll = (LinearLayout) findViewById(R.id.drawPanel);
		DrawView drawView = new DrawView(getApplicationContext());
		ll.addView(drawView);
		drawView.requestFocus();

	}

    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
    	if(keyCode == KeyEvent.KEYCODE_VOLUME_UP && exitTest== true)
    	{
    		finish();
			if (this.testMode == 1)
			{
				/* Only Set PASS in Drawview */
				//testCaseResult = "PASS"; 
				
				Intent nIntent = new Intent(this, tcNext); /* tcNext = Camera */
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);			
			}
			else /* Individual test case */
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}
    	}
    	else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) && (this.testMode == 1))
    	{
    		testCaseResult = "FAIL";
    		showQuitTestAlert();

    	}
    	else
    	{
    		/* Reject other keyevent */
    		if(exitTest){
    			Toast.makeText(this, getString(R.string.mode_continue_next), Toast.LENGTH_SHORT).show();
    		}else
    			Toast.makeText(this, "Please continue the test", Toast.LENGTH_SHORT).show();
    		
    	}

    	if(!(testCaseResult.equals("Not Tested")))
    	{
    		if(testCaseResult.equals("PASS"))
    			db.setTestCasePassFail(Test_Main.tcTouch,true);
    		if(testCaseResult.equals("FAIL"))
    			db.setTestCasePassFail(Test_Main.tcTouch,false);
    		/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr + testCaseResult);

			try 
			{
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter
						(Test_All_Mode.testResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}   
    	}
    	
    	return true;
    }
    
    private void showQuitTestAlert()
    {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
 
		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");
		
		/* Set dialog message */
		alertDialogBuilder
			.setMessage("Click YES to Quit, NO to Proceed Next")
			.setCancelable(false)
			
			.setNegativeButton("No",new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog,int id) 
				{
					/* Failed test but proceed to next test case */
    				Intent nIntent = new Intent(MainActivity.this, tcNext);
    				nIntent.putExtra("testModeType", 1);
    				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				startActivity(nIntent);
	    		
					/* End current activity */
					finish();
				}
			})
			
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog,int id) 
				{
		    		/* Return to Android Home Screen */
					/*
					Intent homescreenIntent = new Intent(
							Intent.ACTION_MAIN);
					homescreenIntent
							.addCategory(Intent.CATEGORY_HOME);
					homescreenIntent
							.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							*/
					Intent nIntent = new Intent(MainActivity.this, Test_Main.class);
					startActivity(nIntent);
		    		
		    		/* End current activity */
		    		finish();
				}
			 });

			/* Create alert dialog */
			AlertDialog alertDialog = alertDialogBuilder.create();
		 
			/* Show it */
			alertDialog.show();
    }
    
	protected void onStop()
	{
		super.onStop();
		finish();
	}
    
}
