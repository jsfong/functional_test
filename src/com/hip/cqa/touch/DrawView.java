/**
 * Revision History
 * ----------------
 * 13-AUG-2013 - Initial Draft
 */

package com.hip.cqa.touch;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Toast;

import com.hip.cqa.R;

@SuppressLint("Instantiatable")
public class DrawView extends View implements OnTouchListener {

	private static final String TAG = "Draw View";
	
	// Variable
	// Drawing
	List<Point> points = new ArrayList<Point>();
	List<Point> points2 = new ArrayList<Point>();
	Paint paint = new Paint();
	Paint gridPaint = new Paint();
	Paint textPaint = new Paint();
	Paint axisPaint = new Paint();
	Path path = new Path();

	// Line checking
	private static boolean endOfLine = true;
	private static Point currentLocation = new Point();
	private static boolean isTouch = false;
	private static boolean inRange = true;
	private static boolean isStartCorrect = false;
	private static boolean isEndCorrect = false;
	int tolerance = 100;

	// Pattern
	private static int pattern = 1;
	private static boolean isPattern1Pass = false;
	private static boolean isPattern2Pass = false;
	private static boolean isPattern3Pass = false;
	private static boolean isPattern4Pass = false;

	// Other
	public static float screenWidth;
	public static float screenHeight;
	// public static Point point1 = new Point();
	// public static Point point2 = new Point();

	// Time
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

	// Multi touch
	private static boolean isStartCorrect2 = false;
	private static boolean isEndCorrect2 = false;

	// Bitmap
	Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.guide);
	Bitmap bm2 = BitmapFactory
			.decodeResource(getResources(), R.drawable.guide2);
	Bitmap bm3 = BitmapFactory
			.decodeResource(getResources(), R.drawable.guide3);
	Bitmap bm4 = BitmapFactory
			.decodeResource(getResources(), R.drawable.guide4);
	private static boolean helpScreen = true;

	// Fail message
	@SuppressLint("Instantiatable")
	@SuppressWarnings("unused")
	private static boolean displayFailMessage = false;
	private static int failMessage = -1;

	public DrawView(Context context) {
		super(context);
		Log.d(TAG, "Constructor");
		setFocusable(true);
		setFocusableInTouchMode(true);
		paint = createPaint(Color.GREEN, 2);
		axisPaint = createPaint(Color.BLUE, 2);
		textPaint = createTextPaint(Color.RED, 100);
		this.setOnTouchListener(this);

	}

	private Paint createTextPaint(int color, int i) {
		// TODO Auto-generated method stub
		Paint temp = new Paint();
		temp.setColor(color);
		temp.setStyle(Style.FILL);
		temp.setTextSize(i);
		return temp;
	}

	private Paint createPaint(int color, float width) {
		// TODO Auto-generated method stub
		Paint temp = new Paint();
		temp.setStyle(Paint.Style.STROKE);
		temp.setAntiAlias(true);
		temp.setColor(color);
		temp.setStrokeWidth(width);
		// temp.setStrokeJoin(Paint.Join.ROUND);
		temp.setStrokeCap(Cap.ROUND);
		return temp;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		// Log.d(TAG, "On Touch");

		// Coordinate for draw axis
		isTouch = true;
		currentLocation.x = (int) event.getX();
		currentLocation.y = (int) event.getY();
		
		//Sample: Log.d(TAG, "w: " + w + " h: " + h);
		Log.d(TAG, "currentLocation.x = " +  currentLocation.x + "currentLocation.y = " + currentLocation.y);

		// Remove the helpScreen
		if (helpScreen) {
			helpScreen = false;
			invalidate();
		}

		// Ontouch for multiple touch
		int action = MotionEventCompat.getActionMasked(event);
		// Get the index of the pointer associated with the action.
		int index = MotionEventCompat.getActionIndex(event);
		int x = -1;
		int y = -1;

		// Log.d(TAG, "The action is " + actionToString(action));

		switch (action) {
		/**
		 * ACTION_DOWN done
		 */
		case MotionEvent.ACTION_DOWN:
			
			// Reset the checking
			isStartCorrect = false;
			isEndCorrect = false;
			isStartCorrect2 = false;
			isEndCorrect2 = false;
			failMessage = -1;

			Point point = new Point();
			point.x = (int) MotionEventCompat.getX(event, index);
			point.y = (int) MotionEventCompat.getY(event, index);
			points.add(point);
			// Log.d(TAG, "Id down: " + activePointer);
			invalidate();

			// Check if start on correct edge
			if (endOfLine) {
				isStartCorrect = inStartRange(point, tolerance);
				// Log.d(TAG, "Start Correct");
			}

			break;

		/**
		 * ACTION_MOVE
		 */
		case MotionEvent.ACTION_MOVE:
			// When single touch
			if (event.getPointerCount() == 1) {
				Point point1 = new Point();
				point1.x = (int) MotionEventCompat.getX(event, index);
				point1.y = (int) MotionEventCompat.getY(event, index);
				// Check if stay in range
				// Only update when stay in range
				// Once out of range consider false
				if (inRange) {
					inRange = inRange(point1, point1, tolerance, false);
					// Log.d(TAG, "inRange" + String.valueOf(inRange));
				}
			}

			// when multi touch
			if (event.getPointerCount() == 2) {
				Point point2 = new Point();
				point2.x = (int) MotionEventCompat.getX(event, 0);
				point2.y = (int) MotionEventCompat.getY(event, 0);
				Point point3 = new Point();
				point3.x = (int) MotionEventCompat.getX(event, 1);
				point3.y = (int) MotionEventCompat.getY(event, 1);

				if (inRange)
					inRange = inRange(point2, point3, tolerance, false);

			}

			// Get all point
			for (int i = 0; i < event.getPointerCount(); i++) {
				Point point4 = new Point();
				point4.x = (int) MotionEventCompat.getX(event, i);
				point4.y = (int) MotionEventCompat.getY(event, i);

				// Get id and store repectively array points list
				int id = event.getPointerId(i);
				if (id == 0) {
					points.add(point4);
					// Log.d(TAG, "Id move1: " + id);
				} else {
					points2.add(point4);
					// Log.d(TAG, "Id move2: " + id);
				}
			}

			invalidate();

			break;

		/**
		 * ACTION_UP TODO pass fail judgement
		 */
		case MotionEvent.ACTION_UP:

			// Pass fail judgement
			Point point5 = new Point();
			point5.x = (int) event.getX();
			point5.y = (int) event.getY();

			// Check if end correct
			isEndCorrect = inEndRange(point5, tolerance);
			if (isEndCorrect)
				Log.d(TAG, "End CORRECT");

			if (isStartCorrect && isEndCorrect && inRange) {
				Log.d(TAG, "PASS!!");
				if (pattern == 1) {
					isPattern1Pass = true;
					helpScreen = true;
				} else if (pattern == 2) {
					isPattern2Pass = true;
					helpScreen = true;
				}
				if (pattern < 3)
					pattern++;

			}

			if (isStartCorrect && isEndCorrect && inRange && isStartCorrect2
					&& isEndCorrect2) {
				if (pattern == 3) {
					Log.d(TAG, "P3 PASS!!");
					isPattern3Pass = true;
					helpScreen = true;
				}

				else if (pattern == 4) {
					isPattern4Pass = true;
					MainActivity.exitTest = true;
					writetoSharedPref(pattern);
					/* Set PASS */
					MainActivity.testCaseResult = "PASS";

				}
				if (pattern < 4)
					pattern++;

			}
			
			if(!isStartCorrect||!isEndCorrect||!isStartCorrect2||!isEndCorrect2){
				failMessage = 1;
			}

			// Clear the setting and prepare
			points = new ArrayList();
			points2 = new ArrayList();
			isTouch = false;
			endOfLine = true;
			inRange = true;
//			isStartCorrect = false;
//			isEndCorrect = false;
//			isStartCorrect2 = false;
//			isEndCorrect2 = false;

			invalidate();
			break;

		/**
		 * ACTION_POINTER_UP done
		 */
		case MotionEvent.ACTION_POINTER_UP:
			Log.d(TAG, "ACTION_POINTER_UP");
			Point point6 = new Point();
			point6.x = (int) event.getX();
			point6.y = (int) event.getY();

			// Check if end correct
			isEndCorrect2 = inEndRange(point6, tolerance);
			if (isEndCorrect2)
				Log.d(TAG, "End 2 CORRECT");
			break;

		/**
		 * ACTION_POINTER_DOWN done
		 */
		case MotionEvent.ACTION_POINTER_DOWN:
			int i = event.getPointerId(getIndex(event));
			// Log.d(TAG, "Id up: " + i);

			Point point7 = new Point();
			point7.x = (int) MotionEventCompat.getX(event, getIndex(event));
			point7.y = (int) MotionEventCompat.getY(event, getIndex(event));
			points2.add(point7);

			// Check secondary pointer land correctly
			isStartCorrect2 = inStartRange(point7, tolerance);
			if (isStartCorrect2)
				// Log.d(TAG, "Second start correct");

				invalidate();
			break;
		}

		return true;

	}

	// Given an action int, returns a string description
	public static String actionToString(int action) {
		switch (action) {

		case MotionEvent.ACTION_DOWN:
			return "Down";
		case MotionEvent.ACTION_MOVE:
			return "Move";
		case MotionEvent.ACTION_POINTER_DOWN:
			return "Pointer Down";
		case MotionEvent.ACTION_UP:
			return "Up";
		case MotionEvent.ACTION_POINTER_UP:
			return "Pointer Up";
		case MotionEvent.ACTION_OUTSIDE:
			return "Outside";
		case MotionEvent.ACTION_CANCEL:
			return "Cancel";
		}
		return "";
	}

	private int getIndex(MotionEvent event) {
		int idx = (event.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
		return idx;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		// TODO Auto-generated method stub
		Log.d(TAG, "onSizeChanged");
		this.screenWidth = w;
		this.screenHeight = h;
		Log.d(TAG, "w: " + w + " h: " + h);

		// gridList = createGrid(wDividerNo, hDividerNo);
		// Log.i(TAG, "Number of grid created: " + gridList.size());
		super.onSizeChanged(w, h, oldw, oldh);
	}

	private void writetoSharedPref(int pattern2) {

		// Writing SharedPref
		Context context = getContext();
		String TestResult = "com.hip.cqa.touch.testResult";
		String TestDateTime = "com.hip.cqa.touch.time";
		String datetime = date.format(new Date());

		SharedPreferences sharedPref = context.getSharedPreferences(
				"com.hip.cqa.touch", Context.MODE_PRIVATE);
		Editor editor = sharedPref.edit();
		editor.putInt(TestResult, pattern2);
		editor.putString(TestDateTime, datetime);
		editor.commit();

	}

	private boolean inEndRange(Point point, int tolerance2) {
		float lowerBoundX = 0, lowerBoundY = 0, upperBoundX = 0, upperBoundY = 0;
		float lowerBoundX2 = 0, lowerBoundY2 = 0, upperBoundX2 = 0, upperBoundY2 = 0;

		if (pattern == 1) {
			lowerBoundX = screenWidth - tolerance2;
			lowerBoundY = screenHeight - tolerance2;
			upperBoundX = screenWidth;
			upperBoundY = screenHeight;

			if (isInPad(lowerBoundX, lowerBoundY, upperBoundX, upperBoundY,
					point))
				return true;
			else
				return false;
		}

		else if (pattern == 2) {
			lowerBoundX = 0;
			lowerBoundY = screenHeight - tolerance2;
			upperBoundX = tolerance2;
			upperBoundY = screenHeight;

			if (isInPad(lowerBoundX, lowerBoundY, upperBoundX, upperBoundY,
					point))
				return true;
			else
				return false;
		}

		else if (pattern == 3) {

			boolean onRight = false;
			boolean onLeft = false;

			// Right pad
			lowerBoundX = 2 * screenWidth / 3 - tolerance2;
			lowerBoundY = screenHeight - tolerance2 * 3 / 2;
			upperBoundX = 2 * screenWidth / 3 + tolerance2;
			upperBoundY = screenHeight;

			// Left pad
			lowerBoundX2 = screenWidth / 3 - tolerance2;
			lowerBoundY2 = screenHeight - tolerance2 * 3 / 2;
			upperBoundX2 = screenWidth / 3 + tolerance2;
			upperBoundY2 = screenHeight;

			onRight = isInPad(lowerBoundX, lowerBoundY, upperBoundX,
					upperBoundY, point);
			onLeft = isInPad(lowerBoundX2, lowerBoundY2, upperBoundX2,
					upperBoundY2, point);

			return onRight || onLeft;

		}

		else if (pattern == 4) {

			boolean onUp = false;
			boolean onDown = false;

			// Top pad
			lowerBoundX = screenWidth - tolerance2 * 3 / 2;
			lowerBoundY = screenHeight / 3 - tolerance2;
			upperBoundX = screenWidth;
			upperBoundY = screenHeight / 3 + tolerance2;

			// Bottom pad
			lowerBoundX2 = screenWidth - tolerance2 * 3 / 2;
			lowerBoundY2 = 2 * screenHeight / 3 - tolerance2;
			upperBoundX2 = screenWidth;
			upperBoundY2 = 2 * screenHeight / 3 + tolerance2;

			onUp = isInPad(lowerBoundX, lowerBoundY, upperBoundX, upperBoundY,
					point);
			onDown = isInPad(lowerBoundX2, lowerBoundY2, upperBoundX2,
					upperBoundY2, point);

			return onUp || onDown;

		}
		return false;
	}

	private void touch_move(MotionEvent event) {
		// TODO Auto-generated method stub
		Log.i(TAG, "ACTION MOVE");
		// Log.d(TAG, "EndofLine: " + String.valueOf(endOfLine));
		// endOfLine = false;

		Point point = new Point();
		point.x = (int) event.getX();
		point.y = (int) event.getY();

		points.add(point);

		invalidate();
	}

	private void touch_start(MotionEvent event) {
		// TODO Auto-generated method stub
		Log.i(TAG, "ACTION DOWN");
		// Log.d(TAG, "EndofLine: " + String.valueOf(endOfLine));

		// Get id
		int mActivePointerId = event.getPointerId(0);
		Log.d(TAG, "1st pointer id: " + mActivePointerId);

		// Push the coordinate to point for path drawing
		Point point = new Point();
		point.x = (int) event.getX();
		point.y = (int) event.getY();
		points.add(point);

		// Check if start on correct edge
		// if (endOfLine)
		// isStartCorrect = inStartRange(point, tolerance);

		// Check if stay in range
		// Only update when stay in range
		// Once out of range consider false
		// if (inRange) {
		// inRange = inRange(point, tolerance);
		// Log.d(TAG, "inRange" + String.valueOf(inRange));
		//
		// }

		endOfLine = false;

		invalidate();

	}

	/**
	 * Check if it start from start range
	 * 
	 * @param point
	 * @param tolerance2
	 * @return
	 */
	private boolean inStartRange(Point point, int tolerance2) {
		// TODO Auto-generated method stub
		// Log.d(TAG, "Checking start range");
		float lowerBoundX = 0, lowerBoundY = 0, upperBoundX = 0, upperBoundY = 0;
		float lowerBoundX2 = 0, lowerBoundY2 = 0, upperBoundX2 = 0, upperBoundY2 = 0;

		if (pattern == 1) {
			lowerBoundX = 0;
			lowerBoundY = 0;
			upperBoundX = tolerance2;
			upperBoundY = tolerance2;

			if (isInPad(lowerBoundX, lowerBoundY, upperBoundX, upperBoundY,
					point))
				return true;
			else
				return false;
		}

		else if (pattern == 2) {
			lowerBoundX = screenWidth - tolerance2;
			lowerBoundY = 0;
			upperBoundX = screenWidth;
			upperBoundY = tolerance2;

			if (isInPad(lowerBoundX, lowerBoundY, upperBoundX, upperBoundY,
					point))
				return true;
			else
				return false;
		}

		else if (pattern == 3) {
			// Check active pointer land in which pad
			boolean onRight = false;
			boolean onLeft = false;

			// Right pad
			lowerBoundX = 2 * screenWidth / 3 - tolerance2;
			lowerBoundY = 0;
			upperBoundX = 2 * screenWidth / 3 + tolerance2;
			upperBoundY = tolerance2 * 3 / 2;

			// Left pad
			lowerBoundX2 = screenWidth / 3 - tolerance2;
			lowerBoundY2 = 0;
			upperBoundX2 = screenWidth / 3 + tolerance2;
			upperBoundY2 = tolerance2 * 3 / 2;

			onRight = isInPad(lowerBoundX, lowerBoundY, upperBoundX,
					upperBoundY, point);
			onLeft = isInPad(lowerBoundX2, lowerBoundY2, upperBoundX2,
					upperBoundY2, point);

			return onRight || onLeft;

		}

		else if (pattern == 4) {
			// Check active pointer land in which pad
			boolean onUp = false;
			boolean onDown = false;

			// Top pad
			lowerBoundX = 0;
			lowerBoundY = screenHeight / 3 - tolerance2;
			upperBoundX = tolerance2 * 3 / 2;
			upperBoundY = screenHeight / 3 + tolerance2;

			// Bottom pad
			lowerBoundX2 = 0;
			lowerBoundY2 = 2 * screenHeight / 3 - tolerance2;
			upperBoundX2 = tolerance2 * 3 / 2;
			upperBoundY2 = 2 * screenHeight / 3 + tolerance2;

			onUp = isInPad(lowerBoundX, lowerBoundY, upperBoundX, upperBoundY,
					point);
			onDown = isInPad(lowerBoundX2, lowerBoundY2, upperBoundX2,
					upperBoundY2, point);

			return onUp || onDown;

		}
		return false;

	}

	private boolean isInPad(float lowerBoundX, float lowerBoundY,
			float upperBoundX, float upperBoundY, Point point) {
		// TODO Auto-generated method stub

		return (lowerBoundX <= point.x && point.x < upperBoundX)
				&& (lowerBoundY <= point.y && point.y < upperBoundY);

	}

	/**
	 * Check is it in range
	 * 
	 * @param point
	 * @param point2
	 * @param mutliTouch
	 * @return
	 */
	private boolean inRange(Point point1, Point point2, int w,
			boolean mutliTouch) {
		// TODO Auto-generated method stub

		float m = 0;
		float c1 = 0, c2 = 0;

		// ** For pattern1
		if (pattern == 1) {
			// Formula for pattern1 line
			float x1, x2, x3, x4, y1, y2, y3, y4;
			
			x1 = 0;
			y1 = -w;
			x2 = screenWidth - w;
			y2 = -screenHeight;
			x3 = screenWidth;
			y3 = -(screenHeight - w);
			x4 = w;
			y4 = 0;

			/* Titnaium: Coordinates mirrored from Platinum */
			/*x1 = 0;
			y1 = -w;
			x2 = screenWidth - w;
			y2 = -screenHeight;
			x3 = screenWidth;
			y3 = -(screenHeight - w);
			x4 = w;
			y4 = 0;*/
			
			// Get gradient m
			m = (y2 - y1) / (x2 - x1);
			Log.d(TAG, "Gradient: " + m);

			// Get c
			// lower line
			c1 = -w;
			// Upper line
			c2 = y4 - m * x4;
			Log.d(TAG, "C2: " + c2);

			return checkRange(point1.x, point1.y, m, c1, c2);

		}

		// ** For Pattern2
		else if (pattern == 2) {
			// Formula for pattern1 line
			float x1, x2, x3, x4, y1, y2, y3, y4;
			x1 = screenWidth - w;
			y1 = 0;
			x2 = 0;
			y2 = screenHeight - w;
			x3 = w;
			y3 = screenHeight;
			x4 = screenWidth;
			y4 = -w;

			// Get gradient m
			m = -(y2 - y1) / (x2 - x1);
			Log.d(TAG, "Gradient: " + m);

			// Get c
			// lower line
			c1 = y1 - m * x1;
			Log.d(TAG, "C1: " + c1);
			// Upper line
			c2 = y4 - m * x4;
			Log.d(TAG, "C2: " + c2);

			return checkRange(point1.x, point1.y, m, c1, c2);
		}

		// ** For Pattern 3
		// check x
		else if (pattern == 3) {
			float lowerBoundX1, upperBoundX1, lowerBoundX2, upperBoundX2;
			lowerBoundX1 = screenWidth / 3 - w;
			upperBoundX1 = screenWidth / 3 + w;
			lowerBoundX2 = 2 * screenWidth / 3 - w;
			upperBoundX2 = 2 * screenWidth / 3 + w;

			if (((lowerBoundX1 <= point1.x && point1.x < upperBoundX1) || (lowerBoundX2 <= point1.x && point1.x < upperBoundX2))
					&& ((lowerBoundX1 <= point2.x && point2.x < upperBoundX1) || (lowerBoundX2 <= point2.x && point2.x < upperBoundX2))) {
				return true;
			}

		}

		else if (pattern == 4) {
			float lowerBoundY1, upperBoundY1, lowerBoundY2, upperBoundY2;
			lowerBoundY1 = screenHeight / 3 - w;
			upperBoundY1 = screenHeight / 3 + w;
			lowerBoundY2 = 2 * screenHeight / 3 - w;
			upperBoundY2 = 2 * screenHeight / 3 + w;

			if (((lowerBoundY1 <= point1.y && point1.y < upperBoundY1) || (lowerBoundY2 <= point1.y && point1.y < upperBoundY2))
					&& ((lowerBoundY1 <= point2.y && point2.y < upperBoundY1) || (lowerBoundY2 <= point2.y && point2.y < upperBoundY2))) {
				return true;
			}

		}
		return false;

	}

	private boolean checkRange(float x, float y, float m, float c1, float c2) {
		// TODO Auto-generated method stub
		// Check if x in range when y varrying
		float lowerBoundX = (-y - c1) / m;
		Log.d(TAG, "lowerBoundX: " + lowerBoundX);

		float upperBoundX = (-y - c2) / m;
		Log.d(TAG, "upperBoundX: " + upperBoundX);

		if (lowerBoundX <= x && x < upperBoundX) {
			Log.d(TAG, "In range");
			return true;
		} else {
			Log.d(TAG, "NOT in range");
			return false;
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// draw background
		drawFailMessage(canvas);

		// draw green background when test pass
		if ((isPattern1Pass && pattern == 1)
				|| (isPattern2Pass && pattern == 2)
				|| (isPattern3Pass && pattern == 3)
				|| (isPattern4Pass && pattern == 4))
			canvas.drawColor(Color.GREEN);

		// draw bitmap
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setFilterBitmap(true);
		paint.setDither(true);
		
		

		switch (pattern) {
		case 1:
			if (helpScreen){
				Bitmap bitmap = getResizedBitmap(bm,screenHeight,screenWidth);
				canvas.drawBitmap(bitmap, 0, 0, paint);
			}
				
			break;
		case 2:
			if (helpScreen){
				Bitmap bitmap = getResizedBitmap(bm2,screenHeight,screenWidth);
				canvas.drawBitmap(bitmap, 0, 0, paint);
			}
				
			break;
		case 3:
			if (helpScreen){
				Bitmap bitmap = getResizedBitmap(bm3,screenHeight,screenWidth);
				canvas.drawBitmap(bitmap, 0, 0, paint);
			}
				
			break;
		case 4:
			if (helpScreen){
				Bitmap bitmap = getResizedBitmap(bm4,screenHeight,screenWidth);
				canvas.drawBitmap(bitmap, 0, 0, paint);
			}
				
			break;

		}

		// draw the pattern
		drawPattern(canvas);

		// draw starting and ending point
		drawStartEndPoint(canvas, tolerance);

		// draw touch line
		drawTouchLine(canvas);

		// draw touch axis
		if (isTouch)
			drawAxis(canvas);

		// Draw PASS test when test is Passed
		if (isPattern4Pass)
			canvas.drawText("PASS", 20, 900, textPaint);

	}

	/**
	 * draw fail message for several cases
	 * 1. out of range
	 * 2. not start or end on destinated pad
	 * @param canvas
	 */
	private void drawFailMessage(Canvas canvas) {
		
		Paint paint2 = new Paint();
		paint2.setColor(Color.WHITE);
        paint2.setTextAlign(Paint.Align.CENTER);
        paint2.setTextSize(50);
        String s = "Error, please retry";

		// draw red background when pointer out of boundary
		if (!inRange && isTouch){
			canvas.drawColor(Color.RED);			
			
			String s1 = "Are you going out of boundary?";					
	        canvas.drawText(s1, 360, 900 ,paint2);
			
		}
		
		// draw red boundary when no 
		if(failMessage == 1){
			canvas.drawColor(Color.RED);
			String s1 = "Not start or end on";
			String s2 = "correct green pad?";	
			canvas.drawText(s, 360, 840 ,paint2);
			canvas.drawText(s1, 360, 900 ,paint2);
			canvas.drawText(s2, 360, 960 ,paint2);
		}

		
	}

	

	/**
	 * Draw starting and ending pad
	 * 
	 * @param canvas
	 */
	private void drawStartEndPoint(Canvas canvas, int tolerance2) {
		// TODO Auto-generated method stub
		float lowerBoundX1 = 0, lowerBoundY1 = 0, upperBoundX1 = 0, upperBoundY1 = 0;
		float lowerBoundX2 = 0, lowerBoundY2 = 0, upperBoundX2 = 0, upperBoundY2 = 0;
		float lowerBoundX3 = 0, lowerBoundY3 = 0, upperBoundX3 = 0, upperBoundY3 = 0;
		float lowerBoundX4 = 0, lowerBoundY4 = 0, upperBoundX4 = 0, upperBoundY4 = 0;
		Paint whiteSquare = new Paint();
		whiteSquare.setColor(Color.GREEN);
		whiteSquare.setStyle(Paint.Style.FILL);
		whiteSquare.setAlpha(170);

		if (pattern == 1) {
			lowerBoundX1 = 0;
			lowerBoundY1 = 0;
			upperBoundX1 = tolerance2;
			upperBoundY1 = tolerance2;
			
			lowerBoundX2 = screenWidth - tolerance2;
			lowerBoundY2 = screenHeight - tolerance2;
			upperBoundX2 = screenWidth;
			upperBoundY2 = screenHeight;
		}

		else if (pattern == 2) {
			lowerBoundX1 = screenWidth - tolerance2;
			lowerBoundY1 = 0;
			upperBoundX1 = screenWidth;
			upperBoundY1 = tolerance2;

			lowerBoundX2 = 0;
			lowerBoundY2 = screenHeight - tolerance2;
			upperBoundX2 = tolerance2;
			upperBoundY2 = screenHeight;
		}

		else if (pattern == 3) {

			lowerBoundX1 = lowerBoundX2 = screenWidth / 3 - tolerance2;
			lowerBoundX3 = lowerBoundX4 = 2 * screenWidth / 3 - tolerance2;
			upperBoundX1 = upperBoundX2 = screenWidth / 3 + tolerance2;
			upperBoundX3 = upperBoundX4 = 2 * screenWidth / 3 + tolerance2;

			lowerBoundY1 = lowerBoundY3 = 0;
			lowerBoundY2 = lowerBoundY4 = screenHeight - tolerance2 * 3 / 2;
			upperBoundY1 = upperBoundY3 = tolerance2 * 3 / 2;
			upperBoundY2 = upperBoundY4 = screenHeight;
		}

		else if (pattern == 4) {

			// Increase the size of the start end point

			lowerBoundY1 = lowerBoundY3 = screenHeight / 3 - tolerance2;
			lowerBoundY2 = lowerBoundY4 = 2 * screenHeight / 3 - tolerance2;
			upperBoundY1 = upperBoundY3 = screenHeight / 3 + tolerance2;
			upperBoundY2 = upperBoundY4 = 2 * screenHeight / 3 + tolerance2;

			lowerBoundX1 = lowerBoundX2 = 0;
			lowerBoundX3 = lowerBoundX4 = screenWidth - tolerance2 * 3 / 2;
			upperBoundX1 = upperBoundX2 = tolerance2 * 3 / 2;
			upperBoundX3 = upperBoundX4 = screenWidth;
		}

		canvas.drawRect(lowerBoundX1, lowerBoundY1, upperBoundX1, upperBoundY1,
				whiteSquare);
		canvas.drawRect(lowerBoundX2, lowerBoundY2, upperBoundX2, upperBoundY2,
				whiteSquare);

		if (pattern == 3 || pattern == 4) {
			// Log.d(TAG, "Draw start end point 4");
			canvas.drawRect(lowerBoundX3, lowerBoundY3, upperBoundX3,
					upperBoundY3, whiteSquare);
			canvas.drawRect(lowerBoundX4, lowerBoundY4, upperBoundX4,
					upperBoundY4, whiteSquare);
		}

	}

	/**
	 * Draw current pointer axis
	 * 
	 * @param canvas
	 */
	private void drawAxis(Canvas canvas) {
		// TODO Auto-generated method stub
		// Log.d(TAG, "Draw axis");
		// Vertical
		int startX = currentLocation.x;
		int stopX = currentLocation.x;
		int startY = 0;
		int stopY = (int) screenHeight;
		canvas.drawLine(startX, startY, stopX, stopY, axisPaint);

		// Horizontal
		int startX2 = 0;
		int stopX2 = (int) screenWidth;
		int startY2 = currentLocation.y;
		int stopY2 = currentLocation.y;
		canvas.drawLine(startX2, startY2, stopX2, stopY2, axisPaint);
	}

	/**
	 * Draw the 4 pattern
	 * 
	 * @param canvas
	 */
	private void drawPattern(Canvas canvas) {
		// TODO Auto-generated method stub
		// Paint for boundary draw
		Paint gray = new Paint();
		gray.setColor(Color.GRAY);
		gray.setStyle(Paint.Style.FILL);
		gray.setAlpha(127);

		// Variable for line equal
		float x, y, c = 0;
		float m;
		float w = tolerance; // width of line

		if (pattern == 1)
			drawPattern1(canvas, gray, w);
		else if (pattern == 2)
			drawPattern2(canvas, gray, w);
		else if (pattern == 3)
			drawPattern3(canvas, gray, w);
		else if (pattern == 4)
			drawPattern4(canvas, gray, w);

	}

	/**
	 * Draw two horizontal lines
	 * 
	 * @param canvas
	 * @param paint
	 * @param w
	 */
	private void drawPattern4(Canvas canvas, Paint paint, float w) {
		// TODO Auto-generated method stub
		// Log.d(TAG, "Draw pattern 4");

		Path p = new Path();
		Path p2 = new Path();
		float x1, x2, x3, x4, x5, x6, x7, x8, y1, y2, y3, y4, y5, y6, y7, y8;
		y1 = y2 = screenHeight / 3 - w;
		y3 = y4 = screenHeight / 3 + w;
		y5 = y6 = 2 * screenHeight / 3 - w;
		y7 = y8 = 2 * screenHeight / 3 + w;

		x1 = x4 = x5 = x8 = 0;
		x2 = x3 = x6 = x7 = screenWidth;

		p.moveTo(x1, y1);
		p.lineTo(x2, y2);
		p.lineTo(x3, y3);
		p.lineTo(x4, y4);
		p.lineTo(x1, y1);
		p.close();

		p2.moveTo(x5, y5);
		p2.lineTo(x6, y6);
		p2.lineTo(x7, y7);
		p2.lineTo(x8, y8);
		p2.lineTo(x5, y5);
		p2.close();

		canvas.drawPath(p, paint);
		canvas.drawPath(p2, paint);
	}

	/**
	 * Draw two vertical line
	 * 
	 * @param canvas
	 * @param paint
	 * @param w
	 */
	private void drawPattern3(Canvas canvas, Paint paint, float w) {
		// TODO Auto-generated method stub
		// Log.d(TAG, "Draw pattern 3");

		Path p = new Path();
		Path p2 = new Path();
		float x1, x2, x3, x4, x5, x6, x7, x8, y1, y2, y3, y4, y5, y6, y7, y8;
		x1 = x2 = screenWidth / 3 - w;
		x3 = x4 = screenWidth / 3 + w;
		x5 = x6 = 2 * screenWidth / 3 - w;
		x7 = x8 = 2 * screenWidth / 3 + w;

		y1 = y4 = y5 = y8 = 0;
		y2 = y3 = y6 = y7 = screenHeight;

		p.moveTo(x1, y1);
		p.lineTo(x2, y2);
		p.lineTo(x3, y3);
		p.lineTo(x4, y4);
		p.lineTo(x1, y1);
		p.close();

		p2.moveTo(x5, y5);
		p2.lineTo(x6, y6);
		p2.lineTo(x7, y7);
		p2.lineTo(x8, y8);
		p2.lineTo(x5, y5);
		p2.close();

		canvas.drawPath(p, paint);
		canvas.drawPath(p2, paint);
	}

	/**
	 * Draw Pattern 2 line from top right to bottom left
	 * 
	 * @param canvas
	 * @param gray
	 * @param w
	 */
	private void drawPattern2(Canvas canvas, Paint paint, float w) {
		// TODO Auto-generated method stub
		// Log.d(TAG, "Draw pattern 2");

		Path p = new Path();
		float x1, x2, x3, x4, y1, y2, y3, y4;
		x1 = screenWidth - w;
		y1 = 0;
		x2 = 0;
		y2 = screenHeight - w;
		x3 = w;
		y3 = screenHeight;
		x4 = screenWidth;
		y4 = w;

		p.moveTo(screenWidth, 0);
		p.lineTo(x1, y1);
		p.lineTo(x2, y2);
		p.lineTo(0, screenHeight);
		p.lineTo(x3, y3);
		p.lineTo(x4, y4);
		p.lineTo(screenWidth, 0);
		p.close();

		canvas.drawPath(p, paint);

	}

	/**
	 * Draw Pattern 1 line from top left to bottom right
	 * 
	 * @param canvas
	 * @param gray
	 * @param w
	 */
	private void drawPattern1(Canvas canvas, Paint paint, float w) {
		// TODO Auto-generated method stub
		// Log.d(TAG, "Draw pattern 1");

		Path p = new Path();
		float x1, x2, x3, x4, y1, y2, y3, y4;
		x1 = 0;
		y1 = w;
		x2 = screenWidth - w;
		y2 = screenHeight;
		x3 = screenWidth;
		y3 = screenHeight - w;
		x4 = w;
		y4 = 0;

		p.moveTo(0, 0);
		p.lineTo(x1, y1);
		p.lineTo(x2, y2);
		p.lineTo(screenWidth, screenHeight);
		p.lineTo(x3, y3);
		p.lineTo(x4, y4);
		p.lineTo(0, 0);
		p.close();

		canvas.drawPath(p, paint);

	}

	/**
	 * Draw the touch line during onDraw
	 * 
	 * @param canvas
	 * @return True when success
	 */
	private boolean drawTouchLine(Canvas canvas) {
		Path path = new Path();
		Path path2 = new Path();
		boolean first = true;
		boolean first2 = true;

		for (Point point : points) {
			if (first) {
				first = false;
				// Log.d(TAG, "onDraw point: " + point.x + " " + point.y);
				path.moveTo(point.x, point.y);
			} else {
				path.lineTo(point.x, point.y);
			}
		}
		canvas.drawPath(path, paint);

		for (Point point : points2) {
			if (first2) {
				first2 = false;
				// Log.d(TAG, "onDraw point: " + point.x + " " + point.y);
				path2.moveTo(point.x, point.y);
			} else {
				path2.lineTo(point.x, point.y);
			}
		}
		canvas.drawPath(path2, paint);

		return true;

	}

	@Override
	protected void onDetachedFromWindow() {
		// Reset the test after the view detached
		isPattern1Pass = false;
		isPattern2Pass = false;
		isPattern3Pass = false;
		isPattern4Pass = false;
		pattern = 1;
		helpScreen = true;
		super.onDetachedFromWindow();
	}
	
	public Bitmap getResizedBitmap(Bitmap bm, float newHeight, float newWidth){
		int width = bm.getWidth();
		int height = bm.getHeight();
		float scaleWidth =newWidth/width;
		float scaleHeight =newHeight/height;
		 // CREATE A MATRIX FOR THE MANIPULATION
	    Matrix matrix = new Matrix();
	    // RESIZE THE BIT MAP
	    matrix.postScale(scaleWidth, scaleHeight);
	    
	 // "RECREATE" THE NEW BITMAP
	    Bitmap resizedBitmap = Bitmap.createBitmap(bm,0,0,width,height,matrix,false);
		
		return resizedBitmap;
	}

	
	
	

}
