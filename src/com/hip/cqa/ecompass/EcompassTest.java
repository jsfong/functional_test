/**
 * Revision History
 * ----------------
 * 30-APR-2013 - Initial Release. Added for P1B. @author JS.Fong 
 * 25-MAY-2014 - Added function to read ecompass passing threshold from xml (/data/specs_vX.xml)
 */

package com.hip.cqa.ecompass;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hip.cqa.R;
import com.hip.cqa.Test_Main;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.Test_All_Mode;
import com.hip.cqa.Test_Result;
import com.hip.cqa.acc.AccelerometerTest;
import com.hip.cqa.parse.specs.ParseSpecs;

public class EcompassTest extends Test_Main implements SensorEventListener{
	
	private static final String TAG = "Ecompass Test";
	
	//UI 
	private ImageView image;
	private float currentDegree = 0f;
	private TextView tvHeading, tvResult;	
	private RelativeLayout layout;
	
	//Sensor
	private SensorManager mSensorManager;
	Sensor accelerometer;
	Sensor magnetometer;
	float[] mGravity;
	float[] mGeomagnetic;
	
	//Ctrl
	final float rad2deg = (float)(180.0f/Math.PI);
	int mBuffer;
	int mCount;
	float orientation[] = new float [3]; //orientation contains: azimuth, pitch and roll
	float incl;
	boolean isPass = false;
	
	boolean setDeg = false;
	float startingDeg = 0f;
	
	//File saving
	private static String filename = "ecompassSensorLog.csv";
	public static File sensorDataFile;
	private String filepath = "log";
	
	/* Time */
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
	
	/* Assign class name of Next test case to variable
	* This is the last test case for Titnaium, therefore no more assignment for tcNext
	private Class tcNext = .class;
	*/
	
	/* Tracking of Position */
	boolean isRotate = true;
	TextView tvEcompassPosition;
	Button bLogEcompassPosition;
	
	private static String testCaseStr = "Ecompass Test - ";
	private static String testCaseResult = "Not Tested";
	final Context context = this;
	private int testMode;

	/* Storing readback heading */
	float headingPositionOne = 0;
	float headingPositionTwo = 0;
	float headingPositionThree = 0;
	float headingPositionFour = 0;
	int headingTurnAngle = 90;
	float lowerLimit = 0; 
	float upperLimit = 0;
	float buffer = 15; //Buffer for accepted range 345-0, 0-15
	
	/* Track turning count */
	int turnCount = 0;
	
	/* Tracking screen orientation */
	int screenOrientation = 0;
	
	/* Read ecompass tolerance limit from Sensor XML */
	ParseSpecs readSpecsLimit = new ParseSpecs();
    float passingDeg = readSpecsLimit.ecompasspassingDegree;
			
	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ecompass);
		
		/* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");
		
		//Init UI and Sensor here
		layout = (RelativeLayout)findViewById(R.id.rlLayout);
		//layout.setBackgroundColor(Color.WHITE);
		
		image = (ImageView)findViewById(R.id.ivImageCompass);
		tvHeading = (TextView)findViewById(R.id.tvHeading);
		tvResult = (TextView)findViewById(R.id.tvResult);
		mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
		accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		
		tvEcompassPosition = (TextView)findViewById(R.id.tvEcompassPosition);
		
		bLogEcompassPosition = (Button)findViewById(R.id.bLogEcompassPosition);
		bLogEcompassPosition.setOnClickListener(onClickListener);
		
		//Create sensor data file
		createSensorDataFile();
		
		/* Start with Start Position test */
		isRotate = false;
		
		/* Instruct to place phone in START position */
		tvEcompassPosition.setText(getString(R.string.start_position));
		Log.d(TAG, "passingDeg: " + passingDeg);
		
		/* For debugging: print passingDeg read from XML */
		//tvEcompassPosition.setText(String.valueOf(passingDeg));
		
		/* Set text color for tvResult */
		tvResult.setTextColor(Color.BLACK);
		
	}

	@Override
	protected void onResume() {		
		super.onResume();
		
		//Register Sensor listener
		mSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
		mSensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
				
		/* Track display screen orientation */
		/* If without getting screen orientation, the compass heading becomes inaccurate */
		Display display = ((WindowManager)getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		screenOrientation = display.getRotation();
	}

	@Override
	protected void onPause() {		
		super.onPause();
		mSensorManager.unregisterListener(this);
	}
	
	@Override
	public void onSensorChanged(SensorEvent event) {
		
		/* Handle sensor changed */
		switch (event.sensor.getType())
		{
			case Sensor.TYPE_ACCELEROMETER:
				mGravity = event.values.clone();
				break;
				
			case Sensor.TYPE_MAGNETIC_FIELD:
				mGeomagnetic = event.values.clone();
				break;
		}
		
		if(mGravity != null && mGeomagnetic != null)
		{
			float R[] = new float[16];
			float I[] = new float[9];
			
			boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
			
			if(success)
			{				
				/* All three angles above are in radians and positive in counter-clockwise direction */
				SensorManager.getOrientation(R, orientation); 
			}
			
			/* getOrientation(R,orientation)
			   orientation[0] = azimuth
			   orientation[1] = pitch
			   orientation[2] = roll
			*/
			
			//Output 
			if(mBuffer++ > 20)
			{
				/* Convert grad to currentDegree */
				currentDegree = (float)Math.toDegrees(orientation[0]);
				
				/* Adjust azimuth to range [0..360] */
				currentDegree = (currentDegree + 360) % 360;
				
				mBuffer = 0;					
				mCount ++;
				
				/**
				 * Decision Making
				 */
				if (mCount >2) /* Only proceed to read starting currentDegree after 3rd loop */
				{ 
					/* Rotate compass to trueNoth and fix image at position */
					if ((mCount == 3) && (!isRotate))
					{
						float trueNorth = 360 - currentDegree;
						
						/* Rotate image facing true North */				
						RotateAnimation ra = new RotateAnimation(
								currentDegree,
								trueNorth,
								Animation.RELATIVE_TO_SELF,
								0.5f,
								Animation.RELATIVE_TO_SELF,
								0.5f);
						
						/* set the animation after the end of the reservation status */
						ra.setFillAfter(true);					
						image.startAnimation(ra);
						
						/* Fix image at True North */
						isRotate = true;
					}			
				}
				
				/* Display the currentDegree */
				tvHeading.setText("Heading: "+ currentDegree); /* Adjust currentDegree between 0 - 360 */

			}
			
		}
		
	}

	private void createSensorDataFile() {
		ContextWrapper contextWrapper = new ContextWrapper(getApplicationContext());
		File directory = contextWrapper.getDir(filepath, Context.MODE_PRIVATE);
		
		/* Create the full path to file in /data/data/com.hip.cqa/app_log/testLog */
		sensorDataFile = new File(directory, filename); 
		//sensorDataFile= new File("/sdcard/EcompassData/"+filename);	
		
		if (sensorDataFile.exists())
		{
			/* Delete existing testLog */
			sensorDataFile.delete();
		} 
		
		//Save the title
		BufferedWriter buf;
		
		try {
			buf = new BufferedWriter(new FileWriter(sensorDataFile, true));
			StringBuilder builder = new StringBuilder("");
			builder.append("time (dd-MM-yyyy hh:mm:ss)")
			.append(",")
			.append("heading")
			.append(",")
			.append("magnetometer x uT")
			.append(",")
			.append("magnetometer y uT")
			.append(",")
			.append("magnetometer z uT");
			buf.append(builder.toString());
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void saveSensorData(float heading, float[] mag){
		//Set current date&time		
		String TestTime = date.format(new Date());
		
		StringBuilder builder = new StringBuilder("");
		builder.append(TestTime).append(",");
		
		try {
			BufferedWriter buf = new BufferedWriter(new FileWriter(sensorDataFile, true));
			builder.append(heading)
				.append(",")
				.append(mag[0])
				.append(",")
				.append(mag[1])
				.append(",")
				.append(mag[2]);
			
			buf.append(builder.toString());
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// Not using
		
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event)
    {
    	if(keyCode == KeyEvent.KEYCODE_VOLUME_UP)
    	{
    		finish();
    		
			if (this.testMode == 1)
			{
				//Set test result based on ecompass result
				if (isPass)
					testCaseResult = "PASS";
				else
					testCaseResult = "FAIL";
				
				Intent nIntent = new Intent(this, Test_Main.class);
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);				
				
			}
			else /* Individual test case */
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}
    	}
    	else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) && (this.testMode == 1))
    	{
    		testCaseResult = "FAIL";
    		showQuitTestAlert();

    	}
    	else
    	{
    		/* Reject other keyevent */
    		Toast.makeText(this, getString(R.string.mode_continue_next), 0).show();
    	}
    	
    	if(!(testCaseResult.equals("Not Tested")))
    	{
    		if(testCaseResult.equals("PASS"))
    			db.setTestCasePassFail(Test_Main.tcEcompass,true);
    		if(testCaseResult.equals("FAIL"))
    			db.setTestCasePassFail(Test_Main.tcEcompass,false);
    		
    		/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr + testCaseResult);

			try 
			{
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter
						(Test_Main.sensorTestResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}   
    	}
    	
    	return true;
    }
	
	private void showQuitTestAlert()
    {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
 
		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");
		
		/* Set dialog message */
		alertDialogBuilder
			.setMessage("Click YES to Quit, NO to Proceed Next")
			.setCancelable(false)
			
		    .setNegativeButton("No",new DialogInterface.OnClickListener() 
			{
			    public void onClick(DialogInterface dialog,int id) 
				{
				    /* Failed test but proceed to next test case */
    				Intent nIntent = new Intent(EcompassTest.this, Test_Main.class);
    				nIntent.putExtra("testModeType", 1);
    				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				startActivity(nIntent);
	    		
					/* End current activity */
					finish();
			    }
			})
			    
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog,int id) 
				{
					/*
					Intent homescreenIntent = new Intent(
							Intent.ACTION_MAIN);
					homescreenIntent
							.addCategory(Intent.CATEGORY_HOME);
					homescreenIntent
							.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							*/
					Intent nIntent = new Intent(EcompassTest.this, Test_Main.class);
					startActivity(nIntent);
		    		
		    		/* End current activity */
		    		finish();
				}
			 });

			/* Create alert dialog */
			AlertDialog alertDialog = alertDialogBuilder.create();
		 
			/* Show it */
			alertDialog.show();
    }	
	
	/* Define class for onClickListener - For "Log Heading" button */
	private OnClickListener onClickListener = new OnClickListener() {
	    @Override
	    public void onClick(View v) {
	    	
//	    	/* Instruct to turn phone to next position */
//	    	tvEcompassPosition.setText(getString(R.string.next_position));

//			/* Track the number of "Log Heading" performed */
//				switch (turnCount)
//				{
//					case 0:
//						startingDeg = currentDegree;
//						
//						/* Calculate the expected turning degree */
//						headingPositionOne = (startingDeg + 90) % 360;
//						headingPositionTwo = (startingDeg + 180) % 360;
//						headingPositionThree = (startingDeg + 270) % 360;
//						headingPositionFour = (startingDeg + 360) % 360;
//							
//						turnCount++;
//						Log.d(TAG,"Start: "+ startingDeg);
//						break;
//							
//					case 1: /* Check if phone is turned within the degree limits */
//						lowerLimit = headingPositionOne - passingDeg;
//						upperLimit = headingPositionOne + passingDeg;
//						
//						if((currentDegree > lowerLimit) && (currentDegree < upperLimit))
//						{
//							tvResult.setText("PASS");
//							layout.setBackgroundColor(Color.GREEN);
//							turnCount++;
//							isPass = true;
//						}
//						else
//						{
//							tvResult.setText("FAIL");
//							layout.setBackgroundColor(Color.RED);
//							isPass = false;
//						}
//						break;
//							
//					case 2: /* Check if phone is turned within the degree limits */
//						lowerLimit = headingPositionTwo - passingDeg;
//						upperLimit = headingPositionTwo + passingDeg;
//						
//						if((currentDegree > lowerLimit) && (currentDegree < upperLimit))
//						{
//							tvResult.setText("PASS");
//							layout.setBackgroundColor(Color.GREEN);
//							turnCount++;
//							isPass = true;
//						}
//						else
//						{
//							tvResult.setText("FAIL");
//							layout.setBackgroundColor(Color.RED);
//							isPass = false;
//						}
//						break;
//								
//					case 3: /* Check if phone is turned within the degree limits */
//						lowerLimit = headingPositionThree - passingDeg;
//						upperLimit = headingPositionThree + passingDeg;
//						
//						if((currentDegree > lowerLimit) && (currentDegree < upperLimit))
//						{
//							tvResult.setText("PASS");
//							layout.setBackgroundColor(Color.GREEN);
//							turnCount++;
//							isPass = true;
//						}
//						else
//						{
//							tvResult.setText("FAIL");
//							layout.setBackgroundColor(Color.RED);
//							isPass = false;
//						}
//						break;
//							
//					case 4: /* Check if phone is turned within the degree limits */
//						lowerLimit = headingPositionFour - passingDeg;
//						upperLimit = headingPositionFour + passingDeg;
//						
//						if((currentDegree > lowerLimit) && (currentDegree < upperLimit))
//						{
//							tvResult.setText("PASS. TEST COMPLETED");
//							layout.setBackgroundColor(Color.GREEN);
//							turnCount = 0; /* Restart counter */
//							isPass = true;
//						}
//						else
//						{
//							tvResult.setText("FAIL");
//							layout.setBackgroundColor(Color.RED);
//							isPass = false;
//						}
//						break;
//				} /* End of switch */			
			
	    	saveSensorData(currentDegree, mGeomagnetic);
	    	
	    	/* Judge Pass Fail */
	    	if(((currentDegree > 0) && (currentDegree < buffer))
	    			||((currentDegree > 360-buffer)&&(currentDegree < 360))){
	    		/*PASS */
	    		tvResult.setText("PASS");
				layout.setBackgroundColor(Color.GREEN);				
				isPass = true;	    		
	    	}else{
	    		/*FAIL*/
	    		tvResult.setText("FAIL");
				layout.setBackgroundColor(Color.RED);
				isPass = false;
	    	}

	    }
	}; /* End of "Log Heading" button clickListener */

}
