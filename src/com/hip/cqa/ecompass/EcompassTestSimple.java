package com.hip.cqa.ecompass;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;

import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hip.cqa.R;
import com.hip.cqa.Test_All_Mode;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.Test_Main;
import com.hip.cqa.Test_Result;
import com.hip.cqa.battery.batteryChargingTest;

public class EcompassTestSimple extends Test_Main implements SensorEventListener{

	private static final String TAG = "Ecompass Test";
	//UI 
	private ImageView image;
	private float currentDegree = 0f;
	private TextView tvHeading, tvResult;	
	private RelativeLayout layout;
	
	//Sensor
	private SensorManager mSensorManager;
	Sensor accelerometer;
	Sensor magnetometer;
	float[] mGravity;
	float[] mGeomagnetic;
	
	//Ctrl
	final float rad2deg = (float)(180.0f/Math.PI);
	int mBuffer;
	int mCount;
	float orientation[] = new float [3];// orientation contains: azimut, pitch and roll
	float incl;
	boolean isPass = false;
	
	boolean setDeg = false;
	float startingDeg = 0f;
	int startingQ =0;
	float totalMoveDeg;	
	boolean turnClockwise = true; 
	float passingDeg = 90; //TODO: Read from xml
	
	//File saving
	private static String filename = "ecompassSensorLog.csv";
	public static File sensorDataFile;
	private String filepath = "log";
	
	/* Time */
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
	
	private static String testCaseStr = "Ecompass Test - ";
	private static String testCaseResult = "Not Tested";
	final Context context = this;
	private int testMode;
	
	private Class tcNext = batteryChargingTest.class;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ecompass);
		
		/* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");
		
		//Init UI and Sensor here
		layout = (RelativeLayout)findViewById(R.id.rlLayout);
		//layout.setBackgroundColor(Color.WHITE);
		
		image = (ImageView)findViewById(R.id.ivImageCompass);
		tvHeading = (TextView)findViewById(R.id.tvHeading);
		tvResult = (TextView)findViewById(R.id.tvResult);
		mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
		accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);		
		
	}

	@Override
	protected void onResume() {		
		super.onResume();
		
		//Register Sensor listener
		mSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
		mSensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
	}

	@Override
	protected void onPause() {		
		super.onPause();
		mSensorManager.unregisterListener(this);
	}
	
	@Override
	public void onSensorChanged(SensorEvent event) {
		// Handle sensor changed
		if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER)
			mGravity = event.values;
		if(event.sensor.getType()==Sensor.TYPE_MAGNETIC_FIELD)
			mGeomagnetic = event.values;
		
		if(mGravity != null && mGeomagnetic != null){
			float R[] = new float[9];
			float I[] = new float[9];
			
			boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
			if(success){				
				SensorManager.getOrientation(R, orientation);
				//incl = SensorManager.getInclination(I);
			}
			
			//TODO: remap the coordinate??
			// some test code which will be used/cleaned up before we ship this.
			// SensorManager.remapCoordinateSystem(mR,
			// SensorManager.AXIS_X, SensorManager.AXIS_Z, mR);
			// SensorManager.remapCoordinateSystem(mR,
			// SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, mR);
			
			//Output 
			if(mBuffer++ > 20){
				float degree = orientation[0]*rad2deg;				
				mBuffer = 0;					
				mCount ++;				
				
				//Correct rotation angles 0 - 360		
				if(degree < 0){
					degree = -degree;
				}else{
					degree = 360 - degree;
				}				
				
				Log.d("delta","totalMoveDeg: "+totalMoveDeg );
				
				/**
				 * UI Diplay Start
				 */
				//Display the degree
				tvHeading.setText("Heading: "+ degree);
				
				//Fix transition rotation error		
				while(degree - currentDegree < -180){
					degree += 360;					
				}
				while(degree - currentDegree > 180){
					degree -= 360;					
				}							
				
				//Create a rotation animation				
				RotateAnimation ra = new RotateAnimation(
						currentDegree,
						degree,
						Animation.RELATIVE_TO_SELF,0.5f,
						Animation.RELATIVE_TO_SELF,0.5f
						);
				
				//How long the animation take place
				ra.setDuration(500);
				
				// set the animation after the end of the reservation status
				ra.setFillAfter(true);
				
				image.startAnimation(ra);
				currentDegree = degree;
			}
			
		}
		
	}

	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// Not using
		
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event)
    {
    	if(keyCode == KeyEvent.KEYCODE_VOLUME_UP)
    	{
    		finish();
    		
			if (this.testMode == 1)
			{
				//Set test result based on ecompass result
				testCaseResult = "PASS";				
				
				Intent nIntent = new Intent(this, tcNext);
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);				
				
			}
			else /* Individual test case */
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}
    	}
    	else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) && (this.testMode == 1))
    	{
    		testCaseResult = "FAIL";
    		showQuitTestAlert();

    	}
    	else
    	{
    		/* Reject other keyevent */
    		Toast.makeText(this, getString(R.string.mode_continue_next), 0).show();
    	}
    	
    	if(!(testCaseResult.equals("Not Tested")))
    	{
    		if(testCaseResult.equals("PASS"))
    			db.setTestCasePassFail(Test_Main.tcEcompass,true);
    		if(testCaseResult.equals("FAIL"))
    			db.setTestCasePassFail(Test_Main.tcEcompass,false);
    		
    		/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr + testCaseResult);

			try 
			{
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter
						(Test_All_Mode.testResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}   
    	}
    	
    	return true;
    }
	
	private void showQuitTestAlert()
    {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
 
		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");
		
		/* Set dialog message */
		alertDialogBuilder
			.setMessage("Click YES to Quit, NO to Proceed Next")
			.setCancelable(false)
			
			.setNegativeButton("No",new DialogInterface.OnClickListener() 
			{
			    public void onClick(DialogInterface dialog,int id) 
				{
				    /* Failed test but proceed to next test case */
    				Intent nIntent = new Intent(EcompassTestSimple.this, Test_Result.class);
    				nIntent.putExtra("testModeType", 1);
    				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				startActivity(nIntent);
	    		
					/* End current activity */
					finish();
			    }
			})
			
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog,int id) 
				{
					/*
					Intent homescreenIntent = new Intent(
							Intent.ACTION_MAIN);
					homescreenIntent
							.addCategory(Intent.CATEGORY_HOME);
					homescreenIntent
							.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							*/
					Intent nIntent = new Intent(EcompassTestSimple.this, Test_Main.class);
					startActivity(nIntent);
		    		
		    		/* End current activity */
		    		finish();
				}
			 });

			/* Create alert dialog */
			AlertDialog alertDialog = alertDialogBuilder.create();
		 
			/* Show it */
			alertDialog.show();
    }	
}
