/**
 * Revision History
 * ----------------
 * 10-JUN-2013 - Initial Draft
 * 08-AUG-2013 - Change background colour to green when detect SIM card
 */

package com.hip.cqa.simcard;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.Toast;

import com.hip.cqa.R;
import com.hip.cqa.Test_All_Mode;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.Test_Main;
import com.hip.cqa.audio.AudioMicLoopBack;
import com.hip.cqa.vibrator.VibratorTest;

public class SimCardTest extends Test_Main
{
	private int testMode;
	private static String testCaseStr = "SIM Card Test - ";
	private static String testCaseResult = "Not Tested";
	final Context context = this;
	
	/* Assign class name of Next test case to variable */
	private Class tcNext = AudioMicLoopBack.class;
	
	/* Listen to Phone state */
	PhoneStateListener listener = new PhoneStateListener()
	{
		public void onServiceStateChanged(ServiceState paramAnonymousServiceState)
		{
			SimCardTest.this.printSimData();
		}
	};
	
	TextView simStateTextView;
	TelephonyManager telephonyManager;

	public void onCreate(Bundle paramBundle)
	{
		this.TAG = "Hi-P_SIMCard";
		
		super.onCreate(paramBundle);
		setContentView(R.layout.simcard);
		
        /* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");
		
		this.simStateTextView = ((TextView)findViewById(R.id.textView_simcardState));
		this.telephonyManager = ((TelephonyManager)getSystemService("phone"));
		printSimData();
		
		this.telephonyManager.listen(this.listener, 1); /* Continuous monitor for any change in SIM state */
	}

    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
    	if(keyCode == KeyEvent.KEYCODE_VOLUME_UP)
    	{
    		finish();
    		
			if (this.testMode == 1)
			{
				/* Set test result status */
				testCaseResult = "PASS"; 
				
				Intent nIntent = new Intent(this, tcNext); /* MIC loopback test */								
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}
			else /* Individual test case */
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}
    	}
    	else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) && (this.testMode == 1))
    	{
    		testCaseResult = "FAIL";
    		showQuitTestAlert();

    	}
    	else
    	{
    		/* Reject other keyevent */
    		Toast.makeText(this, getString(R.string.mode_continue_next), 0).show();
    	}
    	

    	if(!(testCaseResult.equals("Not Tested")))
    	{
    		if(testCaseResult.equals("PASS"))
    			db.setTestCasePassFail(Test_Main.tcSimCard,true);
    		if(testCaseResult.equals("FAIL"))
    			db.setTestCasePassFail(Test_Main.tcSimCard,false);
    		
    		/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr + testCaseResult);

			try 
			{
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter
						(Test_All_Mode.testResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}   
    	}
    	
    	return true;
    }

	protected void printSimData()
	{
		
		String simLabel = "SIM State: ";
		String simState = " ";
		int startStrLen, endStrLen;
		int colorType = Color.BLACK;
		
		this.simStateTextView.setText("");
		switch (this.telephonyManager.getSimState())
		{
			case 0: //SIM_STATE_UNKNOWN
				simState = "UNKNOWN";
				colorType = Color.RED;
				break;
				
			case 1: //SIM_STATE_ABSENT
				simState = "ABSENT";
				colorType = Color.RED;				
				break;
			case 2: //SIM_STATE_PIN_REQUIRED
				simState = "PIN_REQUIRED";
				colorType = Color.RED;
				break;
				
			case 3: //SIM_STATE_PUK_REQUIRED
				simState = "PUK_REQUIRED";
				colorType = Color.RED;
				break;
				
			case 4: //SIM_STATE_NETWORK_LOCKED
				simState = "LOCKED";
				colorType = Color.RED;
				break;
				
			case 5: //SIM_STATE_READY
				simState = "READY";
				colorType = Color.BLACK;
				setBgClr();
				break;
				
			default:
				simLabel = "SIM_STATE: UNDEFINED_";
				simState = String.valueOf(this.telephonyManager.getSimState());
				colorType = Color.RED;
				//this.simStateTextView.setText("SIM_STATE: UNDEFINED_" + this.telephonyManager.getSimState());
				break;			

		}
		
		/* Set the respective color for simStatus & print to screen */
		this.simStateTextView.setText(simLabel + simState, BufferType.SPANNABLE);
		Spannable wordHighlight = (Spannable)this.simStateTextView.getText();
		startStrLen = simLabel.length();
		endStrLen = startStrLen + simState.length();
		wordHighlight.setSpan(new ForegroundColorSpan(colorType), startStrLen, endStrLen, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		this.simStateTextView.setText(wordHighlight);	
	}
	
    private void setBgClr() {
		// Set bg color to green
		View view = this.getWindow().getDecorView();
		view.setBackgroundColor(Color.GREEN);
	}

	private void showQuitTestAlert()
    {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
 
		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");
		
		/* Set dialog message */
		alertDialogBuilder
			.setMessage("Click YES to Quit, NO to Proceed Next")
			.setCancelable(false)
			
			.setNegativeButton("No",new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog,int id) 
				{
					/* Failed test but proceed to next test case */
    				Intent nIntent = new Intent(SimCardTest.this, tcNext);
    				nIntent.putExtra("testModeType", 1);
    				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				startActivity(nIntent);
	    		
					/* End current activity */
					finish();
				}
			})
			
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog,int id) 
				{
					/*
					Intent homescreenIntent = new Intent(
							Intent.ACTION_MAIN);
					homescreenIntent
							.addCategory(Intent.CATEGORY_HOME);
					homescreenIntent
							.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							*/
					Intent nIntent = new Intent(SimCardTest.this, Test_Main.class);
					startActivity(nIntent);
		    		
		    		/* End current activity */
		    		finish();
				}
			 });

			/* Create alert dialog */
			AlertDialog alertDialog = alertDialogBuilder.create();
		 
			/* Show it */
			alertDialog.show();
    }	


	protected void onStop()
	{
		super.onStop();
		finish();
	}
	
}


