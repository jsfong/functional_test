/**
 * Revision History
 * ----------------
 * 25-MAY-2013 - Initial Draft
 * 25-MAY-2013 - Added function to read sensor specs limit from XML in /data/local/sensor_specs.xml
 */

package com.hip.cqa.parse.specs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.hip.cqa.R;
import com.hip.cqa.Test_Main;

import android.util.Log;

public class ParseSpecs extends Test_Main {

	/* Sensor XML filename */
	private static String FILENAME = "/data/local/sensor_specs.xml";
	
	private static final String TAG = "ParseXML";
	
	public static float ambientbuffer = 0f;
	public static float ambientbrightfluxcompare = 0f;
	public static float ambientdarkfluxcompare = 0f;
	public static float accelerometeraccthreshold = 0f;
	public static float ecompasspassingDegree = 0f;
	public static float gyrobuffer = 0f;
	public static float gyroavgX = 0f;
	public static float gyroavgY = 0f;
	public static float gyroavgZ = 0f;
	
	/* Call to read sensor specs from FILENAME */
	public Boolean readSensorSpecs =  readXMLValue();
	
	private Boolean readXMLValue() {
		
		XmlPullParserFactory pullParserFactory;
		try {
			pullParserFactory = XmlPullParserFactory.newInstance();
			XmlPullParser parser = pullParserFactory.newPullParser();

			//Define FileInputStream
			File file = new File (FILENAME);
			FileInputStream fis = new FileInputStream (file);
			
			if (fis != null) 
			{
				InputStreamReader in_sr = new InputStreamReader (fis);
				
				// set the input for the parser using an InputStreamReader
				parser.setInput(in_sr);
				
		        Log.d(TAG, "In xmlpullparser:, call for parseXML()");
		        parseXML(parser);
			
			}
			else /* This served as backup in case /data/local/sensor_specs.xml does not exist */
			{
				ambientbuffer = 50;
				ambientbrightfluxcompare = 250;
				ambientdarkfluxcompare = 50;
				accelerometeraccthreshold = 1;
				ecompasspassingDegree = 15;
				gyrobuffer = 0;
				gyroavgX = 0;
				gyroavgY = 0;
				gyroavgZ = 0;
			}


		} catch (XmlPullParserException e) {

			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}
	
	private void parseXML(XmlPullParser parser) throws XmlPullParserException,IOException
	{
		ArrayList<SpecsCase> testcases = null;
        int eventType = parser.getEventType();
        SpecsCase currentTestcase = null;
        
        while (eventType != XmlPullParser.END_DOCUMENT){
            String name = null;
            switch (eventType){
                case XmlPullParser.START_DOCUMENT:
                	testcases = new ArrayList();
                    break;
                    
                case XmlPullParser.START_TAG:
                    name = parser.getName();
                    if (name.equals("testcase"))
                    {
                    	currentTestcase = new SpecsCase();
                    } 
                    else if (currentTestcase != null)
                    {
                        if (name.equals("testname")) {
                        	currentTestcase.testname = parser.nextText();
                        } 
                        else if (name.equals("limitvalue1")) {
                        	currentTestcase.limitvalue1 = Float.parseFloat(parser.nextText());
                        } 
                        else if (name.equals("limitvalue2")) {
                        	currentTestcase.limitvalue2= Float.parseFloat(parser.nextText());
                        }
                        else if (name.equals("limitvalue3")) {
                        	currentTestcase.limitvalue3= Float.parseFloat(parser.nextText());
                        }
                        else if (name.equals("limitvalue4")) {
                        	currentTestcase.limitvalue4= Float.parseFloat(parser.nextText());
                        }
                    }
                    break;
                    
                case XmlPullParser.END_TAG:
                    name = parser.getName();
                    if (name.equalsIgnoreCase("testcase") && currentTestcase != null) {
                    	testcases.add(currentTestcase);
                    } 
            }
            eventType = parser.next();
            
        } //end of while

       printTestCases(testcases);

	}

	private void printTestCases(ArrayList<SpecsCase> testcases)
	{		
		String content = "";
		Iterator<SpecsCase> it = testcases.iterator();
		while(it.hasNext())
		{
			SpecsCase currTestcase  = it.next();
			content = content + "Testname :" +  currTestcase.testname + "\n";
			content = content + "Limitvalue1 :" +  currTestcase.limitvalue1 + "\n";
			content = content + "Limitvalue2 :" +  currTestcase.limitvalue2 + "\n";
			content = content + "Limitvalue3 :" +  currTestcase.limitvalue3 + "\n";
			content = content + "Limitvalue4 :" +  currTestcase.limitvalue4 + "\n";
			
			if (currTestcase.testname.equals("ambient")) {
				ambientbuffer = currTestcase.limitvalue1;
				ambientbrightfluxcompare = currTestcase.limitvalue2;
				ambientdarkfluxcompare = currTestcase.limitvalue3;
			}
			else if (currTestcase.testname.equals("accelerometer")) {
				accelerometeraccthreshold = currTestcase.limitvalue1;
			}
			else if (currTestcase.testname.equals("ecompass")) {
				ecompasspassingDegree = currTestcase.limitvalue1;
			}
			else if (currTestcase.testname.equals("gyroscope")) {
				gyrobuffer = currTestcase.limitvalue1;
				gyroavgX = currTestcase.limitvalue2;
				gyroavgY = currTestcase.limitvalue3;
				gyroavgZ = currTestcase.limitvalue4;
			}
		}	
			/* For debugging */
			/*Log.d(TAG, "Ambient:buffer: " + ambientbuffer);
			Log.d(TAG, "Ambient:brightfluxcompare: " + ambientbrightfluxcompare);
			Log.d(TAG, "Ambient:darkfluxcompare: " + ambientdarkfluxcompare);
			Log.d(TAG, "Acc:accthreshold: " + accelerometeraccthreshold);
			Log.d(TAG, "Ecompass:passingDegree: " + ecompasspassingDegree);
			Log.d(TAG, "Gyro:buffer: " + gyrobuffer);
			Log.d(TAG, "Gyro:avgX: " + gyroavgX);
			Log.d(TAG, "Gyro:avgY: " + gyroavgY);
			Log.d(TAG, "Gyro:avgZ: " + gyroavgZ);*/
			
	}
}
