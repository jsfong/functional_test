/**
 * Revision History
 * ----------------
 * 25-MAY-2013 - Initial Draft
 * 25-MAY-2013 - Added function to read sensor specs limit from XML in /data/specs_vX.xml
 */

package com.hip.cqa.parse.specs;

public class SpecsCase
{
	//private variable
	public String testname;
	public float limitvalue1;
	public float limitvalue2;
	public float limitvalue3;
	public float limitvalue4;
	
	//Empty constructor
	public SpecsCase()
	{
		
	}

	//Constructor
	public SpecsCase(String testname, float limitvalue1, float limitvalue2, float limitvalue3, float limitvalue4)
	{
		super();
		this.testname = testname;
		this.limitvalue1 = limitvalue1;
		this.limitvalue2 = limitvalue2;		
		this.limitvalue3 = limitvalue3;
		this.limitvalue4 = limitvalue4;
	}
}

