/**
 * Revision History
 * ----------------
 * 18-FEB-2014 - Initial Draft
 * 18-FEB-2014 - Hide Hi-PTest apk icon from launcher after installation. Enter special key code (*#*#9876#*#*) to start Hi-PTest apk
 */

package com.hip.cqa;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;
import com.android.internal.telephony.TelephonyIntents;
import android.content.pm.PackageManager;
import android.util.Log;

public class Test_Receiver extends BroadcastReceiver 
{
	private static final String TAG = "Test_Receiver";
	
	@Override
	public void onReceive(Context localcontext, Intent localintent) 
	{
    	PackageManager hiptest_pm = localcontext.getPackageManager();
    	ComponentName hiptest_cn = new ComponentName(localcontext, Test_Main.class);
    		
		Log.d(TAG, "Test_Receiver: " + localintent);
    
	    /* Get user input secret code */
	    String action = localintent.getAction();
	    String host = localintent.getData() != null ? localintent.getData().getHost() : null;

	    /* Secret code: *#*#9876#*#* */
	    if (TelephonyIntents.SECRET_CODE_ACTION.equals(action) && "9876".equals(host)) 
	    {
	    	/* Show Hi-PTest APK icon*/
			hiptest_pm.setComponentEnabledSetting(hiptest_cn, 
					PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
			  	
	    	Intent hiptest_Intent = new Intent(localcontext, Test_Main.class);
			hiptest_Intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			localcontext.startActivity(hiptest_Intent);
	    }
	    else
	    {
	    	/* Hide Hi-PTest APK icon */
			hiptest_pm.setComponentEnabledSetting(hiptest_cn, 
					PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
	    }
	}
}
