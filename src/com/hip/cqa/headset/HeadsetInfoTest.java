/**
 * Revision History
 * ----------------
 * 10-JUN-2013 - Initial Draft
 * 22-JUL-2013 - Added functionality for Media Button detection test
 * 26-JUL-2013 - Save each test case result to /data/data/com.hip.cqa/app_log/resultLog.xml
 */

package com.hip.cqa.headset;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.BufferType;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.view.KeyEvent;
import android.util.Log;

import com.hip.cqa.R;
import com.hip.cqa.audio.AudioPlayMediaFile;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.Test_All_Mode;
import com.hip.cqa.Test_Main;

public class HeadsetInfoTest extends Test_Main {
	/* Define Local Variables */
	private AudioManager mAudioManager;
	private TextView mHeadsetDetectTextView;
	private TextView mHeadsetKeyTextView;
	private TextView mHeadsetMicTextView;
	private TextView mHeadsetTypeTextView;

	private int testMode;
	private String TAG = "Hi-P_HeadsetInfo";
	private static String testCaseStr = "Headset Test - ";
	private static String testCaseResult = "Not Tested";
	final Context context = this;

	/* Assign class name of Next test case to variable */
	private Class tcNext = AudioPlayMediaFile.class;
	
	public void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setContentView(R.layout.headsetinfo);

		Log.d(this.TAG, "In HeadsetInfoTest");

		/* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");

		this.mHeadsetDetectTextView = ((TextView) findViewById(R.id.textView_headset_detect_info));
		this.mHeadsetDetectTextView.setText("Headset Detect Status: NA");

		this.mHeadsetTypeTextView = ((TextView) findViewById(R.id.textView_headset_type_info));
		this.mHeadsetTypeTextView.setText("Headset Type: NA");

		this.mHeadsetMicTextView = ((TextView) findViewById(R.id.textView_headset_mic_info));
		this.mHeadsetMicTextView.setText("Headset Has Microphone: NA");

		this.mHeadsetKeyTextView = ((TextView) findViewById(R.id.textView_headset_key_info));
		this.mHeadsetKeyTextView.setText("Headset Key Press Status: NA");

		this.mAudioManager = ((AudioManager) getSystemService(Context.AUDIO_SERVICE));

		IntentFilter mediaFilter = new IntentFilter(Intent.ACTION_MEDIA_BUTTON);
		mediaFilter.setPriority(10000);
		registerReceiver(headsetReceiver, mediaFilter);
	}

	protected void onPause() {
		super.onPause();
		unregisterReceiver(this.headsetReceiver);
	}

	protected void onResume() {
		super.onResume();
		IntentFilter localIntentFilter = new IntentFilter(
				Intent.ACTION_HEADSET_PLUG);
		registerReceiver(this.headsetReceiver, localIntentFilter);
	}

	private BroadcastReceiver headsetReceiver = new BroadcastReceiver() {
		public void onReceive(Context paramAnonymousContext,
				Intent paramAnonymousIntent) {
			String str = paramAnonymousIntent.getAction();

			if (str == null) {
				return;
			} else {
				if (str.equals(Intent.ACTION_HEADSET_PLUG)) {
					HeadsetInfoTest.this.handleActionHeadsetPlug(
							paramAnonymousContext, paramAnonymousIntent);
					return;
				}
			}
		}
	};

	private String getHeadsetHasMic(Intent paramIntent) {
		int i = paramIntent.getIntExtra("microphone", 0);
		int j = paramIntent.getIntExtra("state", 0);
		if ((i == 1) && (j == 1)) {
			Log.d("getHeadsetHasMic", "getHeadsetHasMic=YES");
			return "YES";
		} else {
			Log.d("getHeadsetHasMic", "getHeadsetHasMic=NO");
			return "NO";
		}
	}

	private String getHeadsetPlugState(Intent paramIntent) {
		if (paramIntent.getIntExtra("state", 0) == 1) {
			Log.d("getHeadsetPlugState", "getHeadsetPlugState=PLUGGED");
			return "PLUGGED";
		} else {
			Log.d("getHeadsetPlugState", "getHeadsetPlugState=UNPLUGGED");
			return "UNPLUGGED";
		}
	}

	private String getHeadsetType(Intent paramIntent) {
		String str = "NO DEVICE";

		if (paramIntent.getIntExtra("state", 0) == 1) {
			Log.d("getHeadsetType", "getHeadsetType.paramIntent.getIntExtra=1");
			str = paramIntent.getStringExtra("name");
			if (str == null) {
				Log.d("getHeadsetType",
						"getHeadsetType.paramIntent.getIntExtra.name=UNKNOWN");
				str = "UNKNOWN";
			}
		}

		return str;
	}

	private void handleActionHeadsetPlug(Context paramContext,
			Intent paramIntent) {
		String str1 = getHeadsetPlugState(paramIntent);
		this.mHeadsetDetectTextView.setText("Headset Detect Status: " + str1);

		String str2 = getHeadsetType(paramIntent);
		this.mHeadsetTypeTextView.setText("Headset Type: " + str2);

		String str3 = getHeadsetHasMic(paramIntent);
		this.mHeadsetMicTextView.setText("Headset Has Microphone: " + str3);
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			finish();

			if (this.testMode == 1) {
				/* Set test result status */
				testCaseResult = "PASS";

				Intent nIntent = new Intent(this, tcNext);
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			} else /* Individual test case */
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}
		} else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
				&& (this.testMode == 1)) {
			testCaseResult = "FAIL";
			showQuitTestAlert();

		} else if (keyCode == KeyEvent.KEYCODE_HEADSETHOOK) {
			showMediaButtonStatus("PRESSED");
		} else {
			/* Reject other keyevent */
			Toast.makeText(this, getString(R.string.mode_continue_next), 0)
					.show();
		}

		if (!(testCaseResult.equals("Not Tested"))) {
			
			if(testCaseResult.equals("PASS"))
    			db.setTestCasePassFail(Test_Main.tcHeadset,true);
    		if(testCaseResult.equals("FAIL"))
    			db.setTestCasePassFail(Test_Main.tcHeadset,false);
			/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr
					+ testCaseResult);

			try {
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter(
						Test_All_Mode.testResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return true;
	}

	public boolean onKeyUp(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_HEADSETHOOK) {
			showMediaButtonStatus("RELEASED");
		}
		return true;
	}

	protected void showMediaButtonStatus(String status) {

		String mediaButtonLabel = "Headset Key Press Status: ";
		String mediaButtonStatus = " ";
		int startStrLen, endStrLen;
		int colorType = Color.BLACK;

		this.mHeadsetKeyTextView.setText("");

		if (status == "PRESSED") {
			/* Headset media key = PRESSED */
			mediaButtonStatus = "PRESSED";
			setBgClr();
			colorType = Color.BLACK;
		} else if (status == "RELEASED") {
			/* Headset media key = PRESSED */
			mediaButtonStatus = "RELEASED";
			colorType = Color.RED;
		}

		/* Set the respective color for mediaButtonStatus & print to screen */
		this.mHeadsetKeyTextView.setText(mediaButtonLabel + mediaButtonStatus,
				BufferType.SPANNABLE);
		Spannable wordHighlight = (Spannable) this.mHeadsetKeyTextView
				.getText();
		startStrLen = mediaButtonLabel.length();
		endStrLen = startStrLen + mediaButtonStatus.length();
		wordHighlight.setSpan(new ForegroundColorSpan(colorType), startStrLen,
				endStrLen, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		this.mHeadsetKeyTextView.setText(wordHighlight);
	}

	protected void onDestroy() {
		super.onDestroy();
	}

	private void showQuitTestAlert() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");

		/* Set dialog message */
		alertDialogBuilder
				.setMessage("Click YES to Quit, NO to Proceed Next")
				.setCancelable(false)
				
				.setNegativeButton("No",new DialogInterface.OnClickListener() 
			    {
				    public void onClick(DialogInterface dialog,int id) 
				    {
				        /* Failed test but proceed to next test case */
    				    Intent nIntent = new Intent(HeadsetInfoTest.this, tcNext);
    				    nIntent.putExtra("testModeType", 1);
    				    nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				    startActivity(nIntent);
	    		
					    /* End current activity */
					    finish();
				    }
			    })
			
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								/*
								Intent homescreenIntent = new Intent(
										Intent.ACTION_MAIN);
								homescreenIntent
										.addCategory(Intent.CATEGORY_HOME);
								homescreenIntent
										.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
										*/
								Intent nIntent = new Intent(HeadsetInfoTest.this, Test_Main.class);
								startActivity(nIntent);
								
					    		/* End current activity */
					    		finish();
							}
						});

		/* Create alert dialog */
		AlertDialog alertDialog = alertDialogBuilder.create();

		/* Show it */
		alertDialog.show();
	}

	private void setBgClr() {
		// Set bg color to green
		View view = this.getWindow().getDecorView();
		view.setBackgroundColor(Color.GREEN);
	}
	
	protected void onStop()
	{
		super.onStop();
		finish();
	}

} // End of public class HeadsetInfoTest

