package com.hip.cqa;

import java.util.ArrayList;
import java.util.List;

import com.hip.cqa.database.TestCase;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;

public class Test_Result extends Test_Main {

	//UI
	TextView tvPass, tvFail, tvNotTested, tvDetail;
	Button bDone;
	
	//Data
	int totalTestCases;
	int totalPass;
	int totalFail;
	int totalNotTested;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test_result);

		//Get all test case
		List<TestCase> allTc = new ArrayList<TestCase>();
		allTc = db.getAllTestCase(-1);		
		totalTestCases = allTc.size();
		
		//Get pass test case
		List<TestCase> passTc = new ArrayList<TestCase>();
		passTc = db.getAllTestCase(1);			
		totalPass = passTc.size();
		
		//Get fail test case
		List<TestCase> failTc = new ArrayList<TestCase>();
		failTc = db.getAllTestCase(2);			
		totalFail = failTc.size();
		
		//Get Not tested test case
		List<TestCase> NTTc = new ArrayList<TestCase>();
		NTTc = db.getAllTestCase(0);			
		totalNotTested = NTTc.size();
		
		//UI init
		tvPass = (TextView)findViewById(R.id.tvResultPass);
		tvFail = (TextView)findViewById(R.id.tvResultFail);
		tvNotTested = (TextView)findViewById(R.id.tvResultNotTested);
		tvDetail = (TextView)findViewById(R.id.tvResultDetail);
		
		tvPass.setText("Pass: "+totalPass+"/"+ totalTestCases);
		tvPass.setTextColor(Color.GREEN);
		
		tvFail.setText("Fail: "+totalFail+"/"+ totalTestCases );
		tvFail.setTextColor(Color.RED);
		
		tvNotTested.setText("Not tested: : "+totalNotTested+"/"+ totalTestCases );
		tvDetail.setText("" );
		
		bDone = (Button)findViewById(R.id.bResultDone);
		bDone.setOnClickListener(onClickListener);
		
		// Last Test Case for P2B
		Toast.makeText(this, "All Test Cases completed. Press DONE to return to Main Menu", 0).show();
		
		List<TestCase> testCaseList = db.getAllTestCase(-1);		
		for(TestCase testcase: testCaseList){
			String testName = testcase.get_test_case_name();
			String testStatus = new String ();
			String testTime = testcase.get_test_time();
			
			if(testcase.get_test_case_status()  == 1)//pass
				testStatus = "PASS";
			else if (testcase.get_test_case_status()  == 2) //fail
				testStatus = "FAIL";
			else
				testStatus = "Not Tested";
			
			String msg =  testName + " "+testStatus+ " at "+ testTime + "\n";
			tvDetail.append(msg);
			 
		}
		
	}

	private OnClickListener onClickListener = new OnClickListener()
	{
		public void onClick(View v)
		{
			switch(v.getId())
			{
				case R.id.bResultDone:
					Intent test_all_modeIntent = new Intent(getBaseContext(), Test_Main.class);					
					startActivity(test_all_modeIntent);
					break;
			}
					
		}
	};
	

	
}
