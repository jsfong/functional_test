/**
 * Revision History
 * ----------------
 * 10-JUN-2013 - Initial Draft
 * 22-JUL-2013 - Added functionality for Media Button detection test
 * 23-JUL-2013 - Add in two buttons to call for third party apk for gyroscope and ecompass
 * 26-JUL-2013 - Save each test case result to /data/data/com.hip.cqa/app_log/resultLog.xml
 * 29-JUL-2013 - Add in GPS test
 * 30-JUL-2013 - Update source code to sync with STC EInk API changes
 * 07-AUG-2013 - New interface, new button report and exit
 * 08-AUG-2013 - ProximityTest.java: Change background colour to GREEN for PASS
 * 08-AUG-2013 - Change background colour to green when detect SIM card
 * 28-AUG-2013 - Remove 180 degree rotation for preview image. CameraTest.java
 * 29-AUG-2013 - Added Revision History for all .java in versionName 11.4
 * 03-SEP-2013 - Fix wrong audio routing. Change setMode(audioManager.MODE_IN_CALL) to setMode(audioManager.MODE_NORMAL) 
 * 04-SEP-2013 - Added  Gyroscope test in main test loop. 
 * 04-SEP-2013 - Remove Gyroscope and Accelerometer from other test
 * 06-SEP-2013 - Fix bug for unregister sensor before exiting Proximity and Ambient Light test
 * 25-SEP-2013 - Update source code and noproguard.classes.jar to support new STC Eink changes
 * 30-OCT-2013 - Added 3 new test pattern (RED, GREEN, BLUE) to capture display line issue (TestPattern.java)
 * 18-FEB-2014 - Hide Hi-PTest apk icon from launcher after installation. Enter special key code (*#*#9876#*#*) to start Hi-PTest apk
 * 27-MAR-2014 - Titnaium: Modify Hi-PTest apk for P1A build. Disable "Secondary MIC loopback", "Flash LED", "EPD", "Front Camera Test", "Touch Test - Secondary display panel", "NFC"
 * 30-MAR-2014 - Titnaium: Skip Flash LED test for P1A
 * 10-APR-2014 - Git is used for revision tracking ver1.0
 * 12-MAY-2014 - Added new feature to allow operator to proceed with next test for failed test case
 * 12-MAY-2014 - Added FlashLED to Database
 * 12-MAY-2014 - Update Ecompass to log values for N, S, E, W direction
 * 12-MAY-2014 - Move MIC loopback test cases after Headset test (HS -> MIC loopback -> Media playback)
 * 12-MAY-2014 - Assign next test case to class variable, "tcNext"
 * 25-MAY-2014 - Read Sensor Specs limts from XML in /data/local/sensor_specs.xml during runtime
 * 06-JUN-2014 - Added Battery Charging test case
 * 07-JUN-2014 - Fix Front Camera VF rotated 180 degree. Fix crash when launch "Sensors Tests"
 * 
 */

package com.hip.cqa;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.hip.cqa.ambientlight.AmbientLightTest;
import com.hip.cqa.database.DatabaseHandler;
import com.hip.cqa.database.TestCase;
import com.hip.cqa.gyro.GyroscopeTest;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class Test_Main extends Activity {
	/* Define local variables */

	protected String TAG = "Hi-P_Test_Selection_Menu";
	private Button button_start_test_individual_mode;
	private Button button_start_test_all_mode;
	private Button button_other_tests;
	private Button button_report;
	private Button button_exit;
	private Button button_sensor_test;

	// private boolean isTestCompleted = false;

	// private AudioManager mAudioManager;
	// private int streamType;

	/* SensorValueInput file */
	/*public static File sensorValueInputFile;
	private String filename = "sensorValueInput.xml";
	private String filepath = "input";*/
	
	/* Sensor Test Log file*/
	public static File sensorTestResultFile;
	private String filenameSensorTest = "sensorResultLog.xml";
	private String filepathSensorTest = "log";
	
	/* Assign class name of first sensor test case to variable */
	private Class tcSensorStart = GyroscopeTest.class;
	
	// Testcase database
	public static DatabaseHandler db;
	public static TestCase tcDisplay = new TestCase(0, "Display", 
			"No record", 0);
	public static TestCase tcVibrator = new TestCase(1, "Vibrator",
			"No record", 0);
	public static TestCase tcSimCard = new TestCase(2, "SimCard", 
			"No record", 0);
	public static TestCase tcAudioMic = new TestCase(3, "AudioMic",
			"No record", 0);
	public static TestCase tcHeadset = new TestCase(4, "Headset", 
			"No record", 0);
	public static TestCase tcAudioMedia = new TestCase(5, "AudioMedia",
			"No record", 0);	
	public static TestCase tcFlashLED = new TestCase(6, "FlashLED",
			"No record", 0);
	public static TestCase tcTouch = new TestCase(7, "Touch", 
			"No record", 0);
	public static TestCase tcCamera = new TestCase(8, "Camera", 
			"No record", 0);
	public static TestCase tcNfc = new TestCase(9, "NFC", 
			"No record", 0);	
	public static TestCase tcProx = new TestCase(10, "Proximity", 
			"No record", 0);
	public static TestCase tcAmbLight = new TestCase(11, "AmbientLight",
			"No record", 0);
	public static TestCase tcGyro = new TestCase(12, "Gyroscope", 
			"No record", 0);
	public static TestCase tcAcc = new TestCase(13, "Accelerometer",
			"No record", 0);
	public static TestCase tcEcompass = new TestCase(14, "Ecompass",
			"No record", 0);
	public static TestCase tcBattery = new TestCase(15, "Battery",
			"No record", 0);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main2);

		/* Create testcase database */
		db = new DatabaseHandler(this);

		/* Disable system and ringer tone throughout test */
		AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		/*
		 * int streamType = AudioManager.STREAM_SYSTEM;
		 * mAudioManager.setStreamSolo(streamType, true);
		 * mAudioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
		 * mAudioManager.setStreamMute(streamType, true);
		 */
		mAudioManager.adjustVolume(AudioManager.ADJUST_RAISE, 0);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		addListenerTestMenuOnButton(); /* Handle Selection */
		
		// Hide the report and exit - Future development use
		button_report.setVisibility(View.INVISIBLE);
		button_exit.setVisibility(View.INVISIBLE);

		// For reading all the contact
		Log.d("Reading: ", "Reading all contacts..");
		List<TestCase> testcase = db.getAllTestCase(-1);

		for (TestCase tc : testcase) {
			String log = "Id: " + tc.get_id() + " ,Name: "
					+ tc.get_test_case_name() + " ,Time: " + tc.get_test_time();
			// Writing Contacts to log
			Log.d(TAG, " " + log);
		}

	} // End of protected void onCreate(Bundle savedInstanceState)

	public void addListenerTestMenuOnButton() {
		button_start_test_all_mode = (Button) findViewById(R.id.button_test_all_mode);
		button_start_test_all_mode.setOnClickListener(onClickListener);

		button_start_test_individual_mode = (Button) findViewById(R.id.button_test_individual_menu);
		button_start_test_individual_mode.setOnClickListener(onClickListener);

		// button_gyroscope_test = (Button)
		// findViewById(R.id.button_test_gyroscope);
		// button_gyroscope_test.setOnClickListener (onClickListener);
		//
		// button_ecompass_test = (Button)
		// findViewById(R.id.button_test_ecompass);
		// button_ecompass_test.setOnClickListener (onClickListener);
		//
		// button_gps_test = (Button) findViewById(R.id.button_test_gps);
		// button_gps_test.setOnClickListener (onClickListener);
		//
		button_other_tests = (Button) findViewById(R.id.bOtherTest);
		button_other_tests.setOnClickListener(onClickListener);

		button_report = (Button) findViewById(R.id.bReport);
		button_report.setOnClickListener(onClickListener);

		button_exit = (Button) findViewById(R.id.bExit);
		button_exit.setOnClickListener(onClickListener);
		
		button_sensor_test = (Button) findViewById(R.id.bSensorTest);
		button_sensor_test.setOnClickListener(onClickListener);
	}

	/* Define class for onClickListener */
	private OnClickListener onClickListener = new OnClickListener() {
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.button_test_all_mode:
				Intent test_all_modeIntent = new Intent(getBaseContext(),
						Test_All_Mode.class);
				// test_all_modeIntent.putExtra("testModeType", 1); /*
				// Test_All_Mode = 1 */
				startActivity(test_all_modeIntent);
				break;

			case R.id.button_test_individual_menu:
				Intent test_individual_modeIntent = new Intent(
						getBaseContext(), Test_Individual_Mode.class);
				// test_individual_modeIntent.putExtra("testModeType", 2); /*
				// Test_Individual_Mode = 2 */
				startActivity(test_individual_modeIntent);
				break;

				// case R.id.button_test_gyroscope:
				// Intent test_gyroscope_Intent = new Intent();
				// PackageManager gyroscope_manager = getPackageManager();
				// test_gyroscope_Intent =
				// gyroscope_manager.getLaunchIntentForPackage("com.innoventions.sensorkinetics");
				// test_gyroscope_Intent.addCategory(test_gyroscope_Intent.CATEGORY_LAUNCHER);
				// startActivity(test_gyroscope_Intent);
				// break;
				//
				// case R.id.button_test_ecompass:
				// Intent test_ecompass_Intent = new Intent();
				// PackageManager ecompass_manager = getPackageManager();
				// test_ecompass_Intent =
				// ecompass_manager.getLaunchIntentForPackage("com.apksoftware.compass");
				// test_ecompass_Intent.addCategory(test_ecompass_Intent.CATEGORY_LAUNCHER);
				// startActivity(test_ecompass_Intent);
				// break;
				//
				// case R.id.button_test_gps:
				// Intent test_gps_Intent = new Intent();
				// PackageManager gps_manager = getPackageManager();
				// test_gps_Intent =
				// gps_manager.getLaunchIntentForPackage("com.eclipsim.gpsstatus2");
				// test_gps_Intent.addCategory(test_gps_Intent.CATEGORY_LAUNCHER);
				// startActivity(test_gps_Intent);
				// break;

			case R.id.bOtherTest:
				Intent intent_other_test = new Intent(getBaseContext(),
						Other_Test.class);
				startActivity(intent_other_test);
				break;

			case R.id.bReport:
				Intent intent_report = new Intent(getBaseContext(),
						Test_Individual_Mode.class);
				startActivity(intent_report);
				break;
			
			case R.id.bSensorTest:
				//TODO: Start Back Panel test here. Point to the activity here.
				
				//Create SensorTestFile
				createSensorTestResultFile();
				
				/* Start sensor test */
				//Intent displayIntent = new Intent(getBaseContext(), AmbientLightTest.class);
				Intent sensorIntent = new Intent(getBaseContext(), tcSensorStart); /* Perform Ambient test first */
				sensorIntent.putExtra("testModeType", 1); /* Test_All_Mode = 1 */
				startActivity(sensorIntent);
				break;
				
			case R.id.bExit:
				finish();
				break;
			}

		}
	};

	public boolean onCreateOptionsMenu(Menu paramMenu) {
		super.onCreateOptionsMenu(paramMenu);
		paramMenu.add(0, 1, 0, "EXIT").setShortcut('0', 's');
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
		Log.d(this.TAG, "paramMenuItem ID:" + paramMenuItem.getItemId());
		switch (paramMenuItem.getItemId()) {
		case 1:
			finish();

			/* Return to Android Home Screen */
			Intent homescreenIntent = new Intent(Intent.ACTION_MAIN);
			homescreenIntent.addCategory(Intent.CATEGORY_HOME);
			homescreenIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(homescreenIntent);
			return true;

		default:
			return super.onOptionsItemSelected(paramMenuItem);
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
			Test_Main.this.finish();

			/* Hide Hi-P apk icon after exited app */
			PackageManager hiptest_pm = this.getPackageManager();
			ComponentName hiptest_cn = new ComponentName(this, Test_Main.class);

			hiptest_pm.setComponentEnabledSetting(hiptest_cn,
					PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
					PackageManager.DONT_KILL_APP);

			/* Enable system and ringer tone before exiting functional test */
			/*
			 * AudioManager mAudioManager =
			 * (AudioManager)getSystemService(Context.AUDIO_SERVICE); int
			 * streamType = AudioManager.STREAM_SYSTEM;
			 * mAudioManager.setStreamSolo(streamType, false);
			 * mAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
			 * mAudioManager.setStreamMute(streamType, false);
			 * mAudioManager.setStreamVolume(streamType, 3, 1);
			 */

			/* Return to Android Home Screen */
			Log.d(this.TAG, "onKeydown, after finish");
			Intent homescreenIntent = new Intent(Intent.ACTION_MAIN);
			homescreenIntent.addCategory(Intent.CATEGORY_HOME);
			homescreenIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			homescreenIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(homescreenIntent);
		} else if ((keyCode == KeyEvent.KEYCODE_BACK)
				|| (keyCode == KeyEvent.KEYCODE_VOLUME_UP)) {
			Toast.makeText(this, getString(R.string.mode_exit), 0).show();
		}
		/*
		 * else if (keyCode == KeyEvent.KEYCODE_HOME) { Log.d(this.TAG,
		 * "HOME KEY PRESSED"); }
		 */
		return true;
	}

	protected void onDestroy() {
		super.onDestroy();
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}
	
	private void createSensorTestResultFile()
	{	
		ContextWrapper contextWrapper = new ContextWrapper(getApplicationContext());
		File directory = contextWrapper.getDir(filepathSensorTest, Context.MODE_PRIVATE);
		
		/* Create the full path to file in /data/data/com.hip.cqa/app_log/testLog */
		sensorTestResultFile = new File(directory, filenameSensorTest); //This command does not create "testLog"
		
		if (sensorTestResultFile.exists())
		{
			/* Delete existing testLog */
			sensorTestResultFile.delete();
		} 
	}

	/*
	 * public boolean onTouchEvent(MotionEvent event) { if
	 * (mGestureDetector.onTouchEvent(event)) { return true; }
	 * 
	 * //<your code to process the event here> }
	 */

	/* Disable Home Key */
	/*
	 * @Override public void onAttachedToWindow() {
	 * this.getWindow().setType(WindowManager
	 * .LayoutParams.TYPE_KEYGUARD_DIALOG); super.onAttachedToWindow(); }
	 */

} // end of Test_Selection_Menu
