package com.hip.cqa.nfc;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.graphics.Color;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hip.cqa.R;
import com.hip.cqa.Test_All_Mode;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.Test_Main;
import com.hip.cqa.acc.AccelerometerTest;
import com.hip.cqa.camera.CameraTestMain;
import com.hip.cqa.gyro.GyroscopeTest;
import com.hip.cqa.proximity.ProximityTest;
import com.hip.cqa.touch.MainActivity;

public class NfcTest extends Test_Main{

	/* Define Local Variables */
	public static final String TAG = "Hi-P_NFC";

	//NFC
	public static final String MIME_TEXT_PLAIN = "text/plain";
	private NfcAdapter mNfcAdapter;
	public static String sNdefString; //Use Serial_number as write/read string
	public static NdefMessage NfcMsg; 

	//UI
	private static TextView tvNfcStatus, tvNfcTestingInfo;
	private static Button bStartNfcTest;
	private static ImageView ivNfcTestImage;

	private static String testCaseStr = "NFC Test - ";
	private static String testCaseResult = "Not Tested";
	final Context context = this;
	private int testMode;
	private int turnOnNfcNotication = 4;
	
	private static boolean isWriteSuccessful = false;
	
	private static boolean isRetry = false;

	/* Assign class name of Next test case to variable */
	private Class tcNext = ProximityTest.class;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nfc_test);

		//UI init
		tvNfcStatus = (TextView)findViewById(R.id.tvNfcStatus);
		tvNfcTestingInfo = (TextView)findViewById(R.id.tvNfcTestingInfo);
		bStartNfcTest = (Button)findViewById(R.id.bStartNfcTest);
		ivNfcTestImage = (ImageView)findViewById(R.id.ivNfcIcon);	
		
		ivNfcTestImage.setImageResource(R.drawable.tagnfc);
		ivNfcTestImage.setVisibility(View.GONE);
		
		//Hide test info
		tvNfcTestingInfo.setVisibility(View.GONE);

		//Read Test Mode Type selected by User
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");
		
		//NFC
		mNfcAdapter = NfcAdapter.getDefaultAdapter(this);		
		addStartNfcTestListener();
		
		//Get device ID
		//sNdefString = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);		
		sNdefString = Build.SERIAL;
	}
	
	@Override
	protected void onResume() {		
		super.onResume();
		
		tvNfcTestingInfo.setText("Starting...\n");
		
		if(mNfcAdapter==null){
			Toast.makeText(this, "This device doesn't support NFC.\n", Toast.LENGTH_SHORT).show();
			tvNfcTestingInfo.append("FAIL -- Device doesn't support NFC\n");
			//TODO: Fail pls log
		}
	
		if(!mNfcAdapter.isEnabled()){
			turnOnNfcNotication --;
			if(turnOnNfcNotication == 0){
				Toast.makeText(this, "Please Skip NFC test, Press VOL-UP", Toast.LENGTH_LONG).show();
				bStartNfcTest.setEnabled(false);
				tvNfcStatus.setText("NFC disabled. Please Skip by Press VOL-UP");
			}else{
				tvNfcTestingInfo.append("FAIL -- NFC is disable, Please turn on NFC\n");
				Toast.makeText(this, "Please Turn On NFC and press back to continue", Toast.LENGTH_LONG).show();
				startActivity(new Intent("android.settings.NFC_SETTINGS"));
			}		
		}else{
			tvNfcTestingInfo.append("PASS -- NFC is enable\n");			
			tvNfcTestingInfo.append("Press Start... \n");
			if(!isRetry){
				tvNfcStatus.setTextColor(Color.GREEN);
			tvNfcStatus.setText("Ready");
			}else{
				//Enble Retry
				bStartNfcTest.setEnabled(true);
				bStartNfcTest.setText("Retry NFC Test");
			}
			
		}	
		
	}

	private void addStartNfcTestListener(){
		bStartNfcTest.setOnClickListener(OnStartNfcTestListener);
	}
	
	/* Define class for onClickListener */
	  private OnClickListener OnStartNfcTestListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
			// When Write Tag button is clicked	
			testCaseResult = "FAIL";
			isRetry = false;
			
			tvNfcStatus.setText("Place tag near NFC. Don't remove the tag");
			tvNfcStatus.setTextColor(Color.GREEN);
			tvNfcTestingInfo.append("Place the tag near to the NFC\n");		
			bStartNfcTest.setEnabled(false);			
			ivNfcTestImage.setVisibility(View.VISIBLE);
			setupForegroundDispatch(NfcTest.this, mNfcAdapter);
			
		}		  
	  };
	  
	private void showQuitTestAlert()
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");

		/* Set dialog message */
		alertDialogBuilder
		.setMessage("Click YES to Quit, NO to Proceed Next")
		.setCancelable(false)
		
		.setNegativeButton("No",new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog,int id) 
			{
				/* Failed test but proceed to next test case */
    			Intent nIntent = new Intent(NfcTest.this, tcNext);
    			nIntent.putExtra("testModeType", 1);
    			nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    			startActivity(nIntent);
	    		
				/* End current activity */
				finish();
			}
		})
			
		.setPositiveButton("Yes",new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog,int id) 
			{
				/*
				Intent homescreenIntent = new Intent(
						Intent.ACTION_MAIN);
				homescreenIntent
						.addCategory(Intent.CATEGORY_HOME);
				homescreenIntent
						.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						*/
				Intent nIntent = new Intent(NfcTest.this, Test_Main.class);
				startActivity(nIntent);

				/* End current activity */
				finish();
			}
		});

		/* Create alert dialog */
		AlertDialog alertDialog = alertDialogBuilder.create();

		/* Show it */
		alertDialog.show();
	}	
	
	@Override
	protected void onPause(){
		/*
		 * Call this before onPause, otherwise an IllegalArgumentException is thrown as well
		 */
		stopForegroundDispatch(NfcTest.this, mNfcAdapter);
		super.onPause();
	}

	@Override
	protected void onNewIntent (Intent intent){
		/*
		 * This method gets called, when a new Intent gets associated with the current activity instance.
		 * Instead of creating a new activity, onNewIntent will be called.
		 * 
		 * This method get called, when the user attaches a Tag to the devices
		 */
		//Hide the place tag near nfc image
		ivNfcTestImage.setVisibility(View.GONE);
		handleIntent(intent);
	}

	/**
	 * Handle when receive the intent
	 * Start AsynTask here
	 * @param intent
	 */
	private void handleIntent(Intent intent) {

		Log.i(TAG, "handleIntent");
		tvNfcTestingInfo.append("PASS -- NFC tag detected: ");

		Tag t = (Tag)intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		String[] tlist = t.getTechList();
		android.util.Log.v("NFC", "Discovered tag ["+tlist+"]["+t+"] with intent: " + intent);
		android.util.Log.v("NFC", "{"+t+"}");      

		if(mNfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())){
			Log.d(TAG, "ACTION_NDEF_DISCOVERED");     
			tvNfcTestingInfo.append("ACTION_NDEF_DISCOVERED\n");

			String type = intent.getType();
			if (MIME_TEXT_PLAIN.equals(type)) {				
				Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
				//tvStatus2.append("NFC testing start\n");
				//new NdefWriteReadTask().execute(tag);				
				if(isWriteSuccessful){
					tvNfcTestingInfo.append(MIME_TEXT_PLAIN +" tech detected,initiate reading\n");
					new NdefReaderTask().execute(tag);
				}else if (!isWriteSuccessful){
					tvNfcTestingInfo.append("Place the tag near to the NFC\n");
					new NdefWriterTask().execute(tag);
				}else{
					tvNfcTestingInfo.append("FAIL -- "+MIME_TEXT_PLAIN +" tech is not detected\n");
				}	
				
			}else{
				Log.d(TAG, "Wrong mime type");
			}
		}else if(mNfcAdapter.ACTION_TECH_DISCOVERED.equals(intent.getAction())){
			Log.d(TAG, "ACTION_TECH_DISCOVERED");
			tvNfcTestingInfo.append("ACTION_TECH_DISCOVERED\n");

			Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
			String[] techList = tag.getTechList();
			String searchedTech = Ndef.class.getName();

			for (String tech : techList){
				if(searchedTech.equals(tech)){
					//tvStatus2.append("NFC testing start\n");
					//new NdefWriteReadTask().execute(tag);
					
					if(isWriteSuccessful){
						tvNfcTestingInfo.append(searchedTech +" tech detected,initiate reading\n");
						new NdefReaderTask().execute(tag);
					}else if (!isWriteSuccessful){
						tvNfcTestingInfo.append("Place the tag near to the NFC\n");
						new NdefWriterTask().execute(tag);
					}else{
						tvNfcTestingInfo.append("FAIL -- "+searchedTech +" tech is not detected\n");
					}					
					break;
				}
			}

		}else if(mNfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())){
			Log.d(TAG, "ACTION_TAG_DISCOVERED");        	
			tvNfcTestingInfo.append("ACTION_TAG_DISCOVERED\n");

			Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
			String[] techList = tag.getTechList();
			String searchedTech = Ndef.class.getName();			

			//Proceed if contain NDEF in tech list
			for (String tech : techList){
				if(searchedTech.equals(tech)){
					//tvStatus2.append("NFC testing start\n");
					//new NdefWriteReadTask().execute(tag);
					
					Log.d(TAG, "Tech discover: "+tech);
					if(isWriteSuccessful){
						tvNfcTestingInfo.append(searchedTech +" tech detected,initiate reading\n");
						new NdefReaderTask().execute(tag);
					}else if (!isWriteSuccessful){
						tvNfcTestingInfo.append("Place the tag near to the NFC\n");
						new NdefWriterTask().execute(tag);
					}else{
						tvNfcTestingInfo.append("FAIL -- "+searchedTech +" tech is not detected\n");
					}						
					break;
				}
				
				Log.d(TAG, "No support Tech discover");
				tvNfcStatus.setText("FAIL, NFC Tag tech not supported. Please use correct NFC tag adn retry.");
				tvNfcStatus.setTextColor(Color.RED);				
				isRetry = true; //enable retry
			}
		}	
	}

	/**
	 * 
	 * @param mainActivity
	 * @param mNfcAdapter2
	 */
	private void setupForegroundDispatch(final Activity mainActivity,
			NfcAdapter mNfcAdapter2) {

		Log.i(TAG, "setupForegroundDispatch");

		//Create an Intent & pending intent in order to sent out when tag detected
		final Intent intent = new Intent(mainActivity.getApplicationContext(), mainActivity.getClass());
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

		final PendingIntent pendingIntent = PendingIntent.getActivity(mainActivity.getApplicationContext(),
				0, intent, 0);

		//Create IntenetFilter
		IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
		try {
			ndef.addDataType("*/*");
		} catch (MalformedMimeTypeException e) {
			throw new RuntimeException("fail", e);
		}
		IntentFilter td = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
		IntentFilter[] mFilters = new IntentFilter[]{
				ndef, td
		};

		String [][] mTechLists = new String[][] { new String[] { 
				NfcV.class.getName(),
				NfcF.class.getName(),
				NfcA.class.getName(),
				NfcB.class.getName()
		} };


		mNfcAdapter2.enableForegroundDispatch(mainActivity, pendingIntent, mFilters, mTechLists);

	}

	/**
	 * 
	 * @param mainActivity
	 * @param mNfcAdapter2
	 */
	private void stopForegroundDispatch(final Activity mainActivity,
			NfcAdapter mNfcAdapter2) {
		Log.i(TAG, "stopForegroundDispatch");

		mNfcAdapter2.disableForegroundDispatch(mainActivity);

	}	


	/**
	 * 
	 * AsynTask start here
	 *
	 */
	public class NdefReaderTask extends AsyncTask<Tag, Void, String>{	

		private static final String TAG = "Ndef Reading";		

		@Override
		protected String doInBackground(Tag... params) {			

			Tag tag = params[0];

			Ndef ndef = Ndef.get(tag);
			if(ndef == null){
				//NDEF not supported
				return null;
			}

			NdefMessage ndefMessage = ndef.getCachedNdefMessage();

			NdefRecord[] records = ndefMessage.getRecords();
			for(NdefRecord ndefRecord : records){
				if(ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(ndefRecord.getType(), ndefRecord.RTD_TEXT)){
					try {
						return readText(ndefRecord);
					} catch (UnsupportedEncodingException e) {
						Log.e(TAG, "Unsupported Encoding",e);						
					}
				}
			}
			return null;
		}

		private String readText(NdefRecord record) throws UnsupportedEncodingException {
			/*
			 * See NFC forum specification for "Text Record Type Definition" at 3.2.1 
			 * 
			 * http://www.nfc-forum.org/specs/
			 * 
			 * bit_7 defines encoding
			 * bit_6 reserved for future use, must be 0
			 * bit_5..0 length of IANA language code
			 */

			byte [] payload = record.getPayload();

			//Get the Text Encoding
			String textEncoding = ((payload[0] & 128) == 0)? "UTF-8" : "UTF-16";

			//Get the Language  Code
			int languageCodeLength = payload[0] & 0063;

			// String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
			// e.g. "en"

			// Get the Text
			return new String(payload, languageCodeLength+1, payload.length - languageCodeLength -1, textEncoding);

		}

		@Override
		protected void onPostExecute(String result){
			//Stop foregroudDispatch
			stopForegroundDispatch(NfcTest.this, mNfcAdapter);
			if(result != null){
				
				if (sNdefString.equals(result)){
					tvNfcStatus.setTextColor(Color.GREEN);
					tvNfcStatus.setText("PASS Msg: "+ result);
					tvNfcTestingInfo.append("PASS -- Read Successful \n");
					
					/* Set test result status */
					testCaseResult = "PASS"; 
				}else{
					tvNfcStatus.setTextColor(Color.RED);
					tvNfcStatus.setText("FAIL Msg: "+ result);
					tvNfcTestingInfo.append("FAIL -- Read Unsuccessful \n");
					
					/* Set test result status */
					testCaseResult = "FAIL"; 
				}
				
			}
			isWriteSuccessful = false;
			bStartNfcTest.setEnabled(true);
		}		
	}

	public class NdefWriterTask extends AsyncTask<Tag, Void, String>{

		private static final String TAG = "Ndef writer";
		@Override
		protected String doInBackground(Tag... params) {			
			
			Tag tag = params[0];		
			Ndef ndef = Ndef.get(tag);

			//Generate Ndefmessage
			NdefRecord record = null;
			try {
				record = createRecord(sNdefString);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			NfcMsg = new NdefMessage(record);
			Log.i(TAG, "Ndef msg generated");

			int size = NfcMsg.toByteArray().length;
			Log.d(TAG,"NdefWriterTask start");



			if(ndef != null){
				//NDEF supported	
				Log.d(TAG,"Ndef supported, writing msg");				

				try {
					ndef.connect();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					Log.e(TAG, "ndef connect error", e);
				}

				if(!ndef.isWritable()){
					Log.e(TAG, "ndef unwritable");					
					return null;
				}
				if(ndef.getMaxSize()<size){
					Log.e(TAG, "Message is larger than NFC storage");					
					return null;
				}

				try {
					ndef.writeNdefMessage(NfcMsg);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					Log.e(TAG, "writeNdefMessage  error", e);
				} catch (FormatException e) {
					// TODO Auto-generated catch block
					Log.e(TAG, "writeNdefMessage format error", e);
				}
			}else{				
				Log.d(TAG,"Ndef need format, format and writing msg");
				NdefFormatable format = NdefFormatable.get(tag);
				if(format !=null){
					
					try {
						format.connect();
						format.format(NfcMsg);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (FormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}	

			return null;
		}

		@Override
		protected void onPostExecute(String result) {			
				Log.i(TAG, "Write content successful ");
				ivNfcTestImage.setVisibility(View.VISIBLE);
				tvNfcStatus.setText("Remove and place the tag near NFC again\n");
				tvNfcTestingInfo.append("Pls place the tag again near the NFC to read back the value\n");				
				isWriteSuccessful = true;
				setupForegroundDispatch(NfcTest.this, mNfcAdapter);
						
			super.onPostExecute(result);
		}

		private NdefRecord createRecord(String text) throws UnsupportedEncodingException{

			//Create the message in according to the standard
			String lang = "en";
			byte[] textBytes = text.getBytes();
			byte[] langBytes = lang.getBytes("US-ASCII");
			int langLength = langBytes.length;
			int textLength = textBytes.length;

			byte[] payload = new byte[1 + langLength + textLength];
			payload[0] = (byte) langLength;

			//copy langbytes and textbytes into payload
			System.arraycopy(langBytes, 0, payload, 1, langLength);
			System.arraycopy(textBytes, 0, payload, 1 + langLength, textLength);

			NdefRecord recordNFC = new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], payload);
			return recordNFC;

		}


	}

	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		Log.d(this.TAG, "keyCode = " + keyCode);
		if(keyCode == KeyEvent.KEYCODE_VOLUME_UP)
		{
			finish();
	
			if (this.testMode == 1)
			{		
				//Start Next Activity
				Intent nIntent = new Intent(this, tcNext);
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}
			else /* Individual test case */
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}			
	
		}
		else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) && (this.testMode == 1))
		{			
			testCaseResult = "FAIL";
			showQuitTestAlert();
		}
		else
		{
			/* Reject other keyevent */
			Toast.makeText(this, getString(R.string.mode_continue_next), 0).show();
		}
	
		//if(!(testCaseResult.equals("Not Tested"))&& (this.testMode == 1))
		if((this.testMode == 1))
    	{
			if(testCaseResult.equals("PASS"))
    			db.setTestCasePassFail(Test_Main.tcNfc,true);
    		if(testCaseResult.equals("FAIL"))
    			db.setTestCasePassFail(Test_Main.tcNfc,false);
    		
			/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr + testCaseResult);
	
			try 
			{				
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter
						(Test_All_Mode.testResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}   
    	}
	
		return true;
	}	
}
