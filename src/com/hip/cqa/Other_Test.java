/**
 * Revision History
 * ----------------
 * 08-AUG-2013 - Initial Draft. Added for version 11
 * 04-SEP-2013 - Remove Gyroscope test
 * 06-JUN-2014 - Add Back Panel Test (YotaPhone Demo API)
 */

package com.hip.cqa;

import java.io.File;

//import com.hip.cqa.gyro.GyroscopeTest;
//import com.hip.cqa.proximity.ProximityTest;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Other_Test extends Activity implements OnClickListener {

	// Variable
	//private Button button_gyroscope_test;
	private Button button_ecompass_test;
	private Button button_gps_test;
	private Button back;
	private Button VerifyTestResult;
	private Button button_backpanel_test;

	// Log file para
	public static File testResultFile;
	private String filename = "resultLog2.xml";
	private String filepath = "log";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.other_test);

//		button_gyroscope_test = (Button) findViewById(R.id.button_test_gyroscope);
//		button_gyroscope_test.setOnClickListener(this);

		button_ecompass_test = (Button) findViewById(R.id.button_test_ecompass);
		button_ecompass_test.setOnClickListener(this);

		button_gps_test = (Button) findViewById(R.id.button_test_gps);
		button_gps_test.setOnClickListener(this);

		back = (Button) findViewById(R.id.bBack);
		back.setOnClickListener(this);

		VerifyTestResult = (Button) findViewById(R.id.bVerifyTestResult);
		VerifyTestResult.setOnClickListener(this);
		
		button_backpanel_test = (Button) findViewById(R.id.bBackPanelTest);
		button_backpanel_test.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
//		case R.id.button_test_gyroscope:
//			Intent test_gyroscope_Intent = new Intent();
//			PackageManager gyroscope_manager = getPackageManager();
//			test_gyroscope_Intent = gyroscope_manager
//					.getLaunchIntentForPackage("com.innoventions.sensorkinetics");
//			test_gyroscope_Intent
//					.addCategory(test_gyroscope_Intent.CATEGORY_LAUNCHER);
//			startActivity(test_gyroscope_Intent);			
//			break;
		case R.id.button_test_ecompass:
			Intent test_ecompass_Intent = new Intent();
			PackageManager ecompass_manager = getPackageManager();
			test_ecompass_Intent = ecompass_manager
					.getLaunchIntentForPackage("com.apksoftware.compass");
			test_ecompass_Intent
					.addCategory(test_ecompass_Intent.CATEGORY_LAUNCHER);
			startActivity(test_ecompass_Intent);
			break;
		
		case R.id.button_test_gps:
			Intent test_gps_Intent = new Intent();
			PackageManager gps_manager = getPackageManager();
			test_gps_Intent = gps_manager
					.getLaunchIntentForPackage("com.eclipsim.gpsstatus2");
			test_gps_Intent.addCategory(test_gps_Intent.CATEGORY_LAUNCHER);
			startActivity(test_gps_Intent);
			break;

		case R.id.bBack:
			finish();
			break;

		case R.id.bVerifyTestResult:
			createTestResultFile();
			Intent intent_verify_test = new Intent(getBaseContext(),
					VerifyTestResult.class);
			startActivity(intent_verify_test);
			break;
			
		case R.id.bBackPanelTest:
			Intent backpanel_Intent = new Intent();
			PackageManager backpanel_manager = getPackageManager();
			backpanel_Intent = backpanel_manager
					.getLaunchIntentForPackage("com.yotadevices.api.sample");
			backpanel_Intent
					.addCategory(backpanel_Intent.CATEGORY_LAUNCHER);
			startActivity(backpanel_Intent);
			break;	
		}

	}

	private void createTestResultFile() {
		ContextWrapper contextWrapper = new ContextWrapper(
				getApplicationContext());
		File directory = contextWrapper.getDir(filepath, Context.MODE_PRIVATE);

		/*
		 * Create the full path to file in
		 * /data/data/com.hip.cqa/app_log/testLog
		 */
		testResultFile = new File(directory, filename); // This command does not
														// create "testLog"

		if (testResultFile.exists()) {
			/* Delete existing testLog */
			testResultFile.delete();
		}

	}
}
