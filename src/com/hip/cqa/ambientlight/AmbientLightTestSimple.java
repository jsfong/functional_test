package com.hip.cqa.ambientlight;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.BufferType;

import com.hip.cqa.R;
import com.hip.cqa.Test_All_Mode;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.Test_Main;
import com.hip.cqa.gyro.GyroscopeTest;
import com.hip.cqa.gyro.GyroscopeTestSimple;

public class AmbientLightTestSimple extends Test_Main {

	/* Define Local Variables */
	static double fLux = 0.0D;
	static long lastUiUpdateTime;
	private boolean sensorListenerRegistered = false;
	private SensorManager mSensorManager;
	private Sensor mAmbientLightSensor;
	private static String testCaseStr = "Ambient Light Test - ";
	private static String testCaseResult = "Not Tested";
	final Context context = this;
	private int testMode;
	static double BrightfLux = 0.0;
	static double DarkfLux = 0.0;
	static double buffer = 50;// TODO: read from xml
	static double BrightfluxCompare = 550;// TODO: read from xml
	static double DarkfluxCompare = 50; // TODO: read from xml

	// UI
	TextView ambientlightSensorTypeTextView, ambientlightStateTextView;
	TextView tvLuvValue, tvInstruction;
	private Button bLogLuv;
	boolean isInBrightRoom = false;
	boolean isInDarkRoom = false;
	RelativeLayout rlAmbLightBg;

	// File saving
	private static String filename = "AmbLightLog.csv";
	public static File sensorDataFile;
	private String filepath = "log";

	/* Time */
	@SuppressLint("SimpleDateFormat")
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

	/* Assign class name of Next test case to variable */
	private Class tcNext = GyroscopeTestSimple.class;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		this.TAG = "Hi-P_Sensor_AmbientLight";
		setContentView(R.layout.ambient_light2);

		/* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");

		this.mSensorManager = ((SensorManager) getSystemService(Context.SENSOR_SERVICE));
		this.mAmbientLightSensor = this.mSensorManager
				.getDefaultSensor(Sensor.TYPE_LIGHT);
		sensorListenerRegistered = true;

		this.ambientlightStateTextView = ((TextView) findViewById(R.id.textView_ambientlightState));
		this.ambientlightSensorTypeTextView = ((TextView) findViewById(R.id.textView_ambientlightSensorType));
		this.tvLuvValue = ((TextView) findViewById(R.id.tvLuv));
		this.tvInstruction = ((TextView) findViewById(R.id.tvInstruction));

		this.rlAmbLightBg = (RelativeLayout) findViewById(R.id.rlAmbientLight);

		this.lastUiUpdateTime = System.currentTimeMillis();

		if (mAmbientLightSensor == null) {
			ambientlightStateTextView.setTextColor(Color.RED);
			ambientlightStateTextView
			.setText("No Ambient Light Sensor Available!");
		} else {
			ambientlightStateTextView.setText(mAmbientLightSensor.getName()); /*
			 * Display
			 * Proximity
			 * Sensor
			 * Type
			 */
			ambientlightStateTextView.setText("Ambient Light State: "); /*
			 * Display
			 * Proximity
			 * State
			 * Level
			 */

			/* Configure for Sensor Listener */
			mSensorManager.registerListener(ambientlightSensorEventListener,
					mAmbientLightSensor, mSensorManager.SENSOR_DELAY_NORMAL);

		}

		// Init button
		bLogLuv = (Button) findViewById(R.id.bLogLuv);
		bLogLuv.setVisibility(View.GONE);

		// Start Instruction
		tvInstruction.setVisibility(View.GONE);

	}

	SensorEventListener ambientlightSensorEventListener = new SensorEventListener() {
		public void onAccuracyChanged(Sensor paramSensor, int paramAccuracy) {
			/* Auto-generated method stub) */
		}

		public void onSensorChanged(SensorEvent paramSensorEvent) {
			String ambientlightLabel = "Ambient Light State: ";
			String ambientlightState = " ";
			int startStrLen, endStrLen;

			if (paramSensorEvent.sensor.getType() == Sensor.TYPE_LIGHT) {
				AmbientLightTest.fLux = paramSensorEvent.values[0];

				if (System.currentTimeMillis()
						- AmbientLightTest.lastUiUpdateTime > 50L) {
					AmbientLightTest.lastUiUpdateTime = System
							.currentTimeMillis();

					tvLuvValue.setText("Lux: " + AmbientLightTest.fLux);

					/*
					 * if (AmbientLightTest.fLux > 240.0D) { ambientlightState =
					 * "BRIGHTEST"; } else if ( (AmbientLightTest.fLux > 180.0D)
					 * && (AmbientLightTest.fLux <= 240.0D) ) {
					 * ambientlightState = "BRIGHTER"; } else if (
					 * (AmbientLightTest.fLux > 120.0D) &&
					 * (AmbientLightTest.fLux <= 180.0D) ) { ambientlightState =
					 * "BRIGHT"; } else if ( (AmbientLightTest.fLux > 60.0D) &&
					 * (AmbientLightTest.fLux <= 120.0D) ) { ambientlightState =
					 * "NORMAL"; } else // AmbientLightTest.fLux <= 60.0D {
					 * ambientlightState = "DARK"; }
					 */

					if (AmbientLightTest.fLux > 120.0D) {
						ambientlightState = "BRIGHT";
					} else if ((AmbientLightTest.fLux > 15.0D)
							&& (AmbientLightTest.fLux <= 120.0D)) {
						ambientlightState = "NORMAL";
					} else // AmbientLightTest.fLux <= 60.0D
					{
						ambientlightState = "DARK";
					}

					/*
					 * Set the respective color for ambientlightState & print to
					 * screen
					 */
					ambientlightStateTextView.setText(ambientlightLabel
							+ ambientlightState, BufferType.SPANNABLE);
					Spannable wordHighlight = (Spannable) ambientlightStateTextView
							.getText();
					startStrLen = ambientlightLabel.length();
					endStrLen = startStrLen + ambientlightState.length();
					wordHighlight.setSpan(new ForegroundColorSpan(Color.GREEN),
							startStrLen, endStrLen,
							Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					ambientlightStateTextView.setText(wordHighlight);
				}
			}

			return;
		}
	};

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			finish();

			/* Unregister Ambient Sensor */
			if (this.sensorListenerRegistered == true) {
				this.mSensorManager
				.unregisterListener(ambientlightSensorEventListener);
				this.sensorListenerRegistered = false;
			}

			if (this.testMode == 1) {
				/* Set test result status */
				 testCaseResult = "PASS";				

				Intent nIntent = new Intent(this, tcNext);
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			} else /* Individual test case */
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}

		} else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
				&& (this.testMode == 1)) {
			 testCaseResult = "FAIL"; 
			showQuitTestAlert();

		} else {
			/* Reject other keyevent */
			Toast.makeText(this, getString(R.string.mode_continue_next), 0)
			.show();
		}

		if (!(testCaseResult.equals("Not Tested"))) {
			if (testCaseResult.equals("PASS"))
				db.setTestCasePassFail(Test_Main.tcAmbLight, true);
			if (testCaseResult.equals("FAIL"))
				db.setTestCasePassFail(Test_Main.tcAmbLight, false);
			/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr
					+ testCaseResult);

			try {
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter(
						Test_All_Mode.testResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return true;
	}

	protected void onDestroy() {
		super.onDestroy();
		if (this.sensorListenerRegistered == true) {
			Log.d(this.TAG, "In on Destroy");
			this.mSensorManager
			.unregisterListener(ambientlightSensorEventListener);
			this.sensorListenerRegistered = false;
		}
	}

	private void showQuitTestAlert() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");

		/* Set dialog message */
		alertDialogBuilder.setMessage("Click YES to Quit, NO to Proceed Next")
		// .setMessage("Click YES to Quit")
		.setCancelable(false)
		
		.setNegativeButton("No",new DialogInterface.OnClickListener() 
		{
		    public void onClick(DialogInterface dialog,int id) 
			{
				/* Failed test but proceed to next test case */
    	   	    Intent nIntent = new Intent(AmbientLightTestSimple.this, tcNext);
    			nIntent.putExtra("testModeType", 1);
    			nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    			startActivity(nIntent);
	    		
				/* End current activity */
				finish();
			}
		})
			
		.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				/*
				 * Intent homescreenIntent = new Intent(
				 * Intent.ACTION_MAIN); homescreenIntent
				 * .addCategory(Intent.CATEGORY_HOME);
				 * homescreenIntent
				 * .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				 */
				Intent nIntent = new Intent(
						AmbientLightTestSimple.this, Test_Main.class);
				startActivity(nIntent);

				/* End current activity */
				finish();
			}
		});

		/* Create alert dialog */
		AlertDialog alertDialog = alertDialogBuilder.create();

		/* Show it */
		alertDialog.show();
	}

	protected void onStop() {
		super.onStop();
		finish();
	}

}
