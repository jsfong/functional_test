/**
 * Revision History
 * ----------------
 * 10-JUN-2013 - Initial Draft
 * 30-OCT-2013 - Added 3 new test pattern (RED, GREEN, BLUE) to capture display line issue
 */

package com.hip.cqa.display.testpattern;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import android.os.Bundle;
import android.os.Handler;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.Toast;
import android.util.Log;

import com.hip.cqa.R;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.Test_Main; /* must import the master class that the current class inherit from */
import com.hip.cqa.database.TestCase;
import com.hip.cqa.vibrator.VibratorTest;
import com.hip.cqa.Test_All_Mode;


public class TestPattern extends Test_Main 
{
	/* Define local variables */

	public ImageView image;
	private int testMode;
	private int testPatternNum = 0;
	
	private static String testCaseStr = "Main Display Test Pattern - ";
	private String testCaseResult = "Not Tested";
	final Context context = this;
	
	/* Assign class name of Next test case to variable */
	private Class tcNext = VibratorTest.class;
	
    /* Called when the activity is first created. */
    public void onCreate(Bundle paramBundle) 
    {
    	this.TAG = "Hi-P_Main Display Test Pattern";
        super.onCreate(paramBundle);
        setContentView(R.layout.display_testpattern);
              
        /* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");
        
        image = (ImageView) findViewById(R.id.imageView_display_testpattern);
        image.setImageResource(R.drawable.black_white_border_main);
        testPatternNum = 1;
    }
    
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
    	if(keyCode == KeyEvent.KEYCODE_VOLUME_UP)
    	{   		
    		if (testPatternNum < 8)
    		{
    			switch(testPatternNum)
    			{
    				case 0:
    					image.setImageResource(R.drawable.black_white_border_main);
    					break;

    				case 1:
    					image.setImageResource(R.drawable.bright_color_bar_pattern_main);
    					break;

    				case 2:
    					image.setImageResource(R.drawable.grayscale_pattern_main);
    					break;

    				case 3:
    					image.setImageResource(R.drawable.white_black_border_main);
    					break;

    				case 4:
    					image.setImageResource(R.drawable.xtalk_pattern_main);
    					break;
    					
    				case 5:
    					image.setImageResource(R.drawable.red_color); /* Added test pattern to capture display line issue */
    					break;
    					
    				case 6:
    					image.setImageResource(R.drawable.green_color); /* Added test pattern to capture display line issue */
    					break;
    					
    				case 7:
    					image.setImageResource(R.drawable.blue_color); /* Added test pattern to capture display line issue */
    					break;
    					    					
    			}
    			
    			testPatternNum++;
    		}
    		else /* Complete x8 test pattern */
    		{
        		finish();
        		
    			if (this.testMode == 1)
    			{
    				/* Set test result status */
    				testCaseResult = "PASS"; 			
    				
    				Intent nIntent = new Intent(this, tcNext);
    				nIntent.putExtra("testModeType", 1);
    				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				startActivity(nIntent);
    			}
    			else if(this.testMode == 2) /* Individual test case */
    			{
    				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
    				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				startActivity(nIntent);
    			}
    		}
    	}
    	else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) && (this.testMode == 1))
    	{
    		testCaseResult = "FAIL";
    		showQuitTestAlert();

    	}
    	else
    	{
    		/* Reject other keyevent */
    		Toast.makeText(this, getString(R.string.mode_continue_next), 0).show();
    	}
   
    	if(!(testCaseResult.equals("Not Tested")))
    	{
    		if(testCaseResult.equals("PASS"))
    			db.setTestCasePassFail(Test_Main.tcDisplay,true);
    		if(testCaseResult.equals("FAIL"))
    			db.setTestCasePassFail(Test_Main.tcDisplay,false);
    		
    		/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr + testCaseResult);

			try 
			{
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter
						(Test_All_Mode.testResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}   
    	}
    	
    	return true;
    }
    
    private void showQuitTestAlert()
    {    	
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
 
		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");
		//alertDialogBuilder.setTitle(TestPattern.this.messageTitle);
		
		/* Set dialog message */
		alertDialogBuilder
			.setMessage("Click YES to Quit, NO to Proceed Next")
			.setCancelable(false)
			
			.setNegativeButton("No",new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog,int id) 
				{
					/* Failed test but proceed to next test case */
    				Intent nIntent = new Intent(TestPattern.this, tcNext);
    				nIntent.putExtra("testModeType", 1);
    				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				startActivity(nIntent);
	    		
					/* End current activity */
					finish();
				}
			})
			
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog,int id) 
				{
					/*
					Intent homescreenIntent = new Intent(
							Intent.ACTION_MAIN);
					homescreenIntent
							.addCategory(Intent.CATEGORY_HOME);
					homescreenIntent
							.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							*/
					Intent nIntent = new Intent(TestPattern.this, Test_Main.class);
					startActivity(nIntent);
		    		
		    		/* End current activity */
		    		finish();
				}
			 });
		
			/* Create alert dialog */
			AlertDialog alertDialog = alertDialogBuilder.create();
		 
			/* Show it */
			alertDialog.show();
    }
    
	protected void onStop()
	{
		super.onStop();
		finish();
	}  
	
} //End of public class BlackPattern




