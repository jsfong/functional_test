/**
 * Revision History
 * ----------------
 * 10-JUN-2013 - Initial Draft
 * 08-AUG-2013 - ProximityTest.java: Change background colour to GREEN for PASS
 * 06-SEP-2013 - Fix bug for unregister sensor before exiting Proximity and Ambient Light test
 */

package com.hip.cqa.proximity;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.BufferType;
import android.util.Log;

import com.hip.cqa.R;
import com.hip.cqa.Test_Main;
import com.hip.cqa.acc.AccelerometerTest;
import com.hip.cqa.ambientlight.AmbientLightTest;
import com.hip.cqa.ambientlight.AmbientLightTestSimple;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.Test_All_Mode;

public class ProximityTest extends Test_Main {
	/* Define Local Variables */
	static double fDistance = 0.0D;
	static long lastUiUpdateTime;
	private boolean sensorListenerRegistered = false;
	private int testMode;

	private SensorManager mSensorManager;
	private Sensor mProximitySensor;

	private static String testCaseStr = "Proximity Test - ";
	private static String testCaseResult = "Not Tested";
	final Context context = this;

	/* Assign class name of Next test case to variable */
	private Class tcNext = AmbientLightTestSimple.class;
	
	TextView proximitySensorTypeTextView, proximityStateTextView;// ,
																	// proximityMax;

	public void onCreate(Bundle paramBundle) {
		this.TAG = "Hi-P_Sensor_Proximity";

		super.onCreate(paramBundle);
		setContentView(R.layout.proximity);

		/* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");

		this.mSensorManager = ((SensorManager) getSystemService(Context.SENSOR_SERVICE));
		this.mProximitySensor = this.mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
		sensorListenerRegistered = true;

		this.proximityStateTextView = ((TextView) findViewById(R.id.textView_proximityState));
		this.proximitySensorTypeTextView = ((TextView) findViewById(R.id.textView_proximitySensorType));

		this.lastUiUpdateTime = System.currentTimeMillis();

		if (mProximitySensor == null) {
			proximitySensorTypeTextView.setTextColor(Color.RED);
			proximitySensorTypeTextView
					.setText("No Proximity Sensor Available!");
		} else {
			proximitySensorTypeTextView.setText(mProximitySensor.getName()); /*
																			 * Display
																			 * Proximity
																			 * Sensor
																			 * Type
																			 */
			proximityStateTextView.setText("Proximity State: "); /*
																 * Display
																 * Proximity
																 * State Lavel
																 */

			/* Configure for Sensor Listener */
			mSensorManager.registerListener(proximitySensorEventListener,
					mProximitySensor, mSensorManager.SENSOR_DELAY_NORMAL);

		}
	}

	SensorEventListener proximitySensorEventListener = new SensorEventListener() {
		public void onAccuracyChanged(Sensor paramSensor, int paramAccuracy) {
			/* Auto-generated method stub) */
		}

		public void onSensorChanged(SensorEvent paramSensorEvent) {
			String proximityLabel = "Proximity State: ";
			String proximityState = " ";
			int startStrLen, endStrLen;
			int colorType = Color.BLACK;

			Log.d(ProximityTest.this.TAG, "In onSensorChanged");

			if (paramSensorEvent.sensor.getType() == Sensor.TYPE_PROXIMITY) {
				ProximityTest.fDistance = paramSensorEvent.values[0];
				
				if (System.currentTimeMillis() - ProximityTest.lastUiUpdateTime > 200L) {
					ProximityTest.lastUiUpdateTime = System.currentTimeMillis();

					// if (ProximityTest.fDistance != 100.0D) //JS
					if (ProximityTest.fDistance == 0.0D) // Yota
					{
						colorType = Color.BLACK;
						setBgClr();
						proximityState = "Enable";
					} else {
						colorType = Color.RED;
						proximityState = "Disable";
					}

					/*
					 * Set the respective color for proximityState & print to
					 * screen
					 */
					proximityStateTextView.setText(proximityLabel
							+ proximityState, BufferType.SPANNABLE);
					Spannable wordHighlight = (Spannable) proximityStateTextView
							.getText();
					startStrLen = proximityLabel.length();
					endStrLen = startStrLen + proximityState.length();
					wordHighlight.setSpan(new ForegroundColorSpan(colorType),
							startStrLen, endStrLen,
							Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					proximityStateTextView.setText(wordHighlight);
				}
			}

			return;
		}
	};

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) 
		{
			finish();

			/* Must unregister sensor first before calling for AmbientLightTest */
			if (this.sensorListenerRegistered == true) 
			{
				this.mSensorManager.unregisterListener(proximitySensorEventListener);
				this.sensorListenerRegistered = false;
			}
			
			if (this.testMode == 1) {
				/* Set test result status */
				testCaseResult = "PASS";

				Intent nIntent = new Intent(this, tcNext);
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			} else /* Individual test case */
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}
		} else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
				&& (this.testMode == 1)) {
			testCaseResult = "FAIL";
			showQuitTestAlert();

		} else {
			/* Reject other keyevent */
			Toast.makeText(this, getString(R.string.mode_continue_next), 0)
					.show();
		}

		if (!(testCaseResult.equals("Not Tested"))) {
			
			if(testCaseResult.equals("PASS"))
    			db.setTestCasePassFail(Test_Main.tcProx,true);
    		if(testCaseResult.equals("FAIL"))
    			db.setTestCasePassFail(Test_Main.tcProx,false);
    		
			/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr
					+ testCaseResult);

			try {
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter(
						Test_All_Mode.testResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return true;
	}

	protected void onDestroy() {
		super.onDestroy();
		if (this.sensorListenerRegistered == true) {
			Log.d(this.TAG, "In on Destroy");
			this.mSensorManager.unregisterListener(proximitySensorEventListener);
			this.sensorListenerRegistered = false;
		}
	}

	private void showQuitTestAlert() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");

		/* Set dialog message */
		alertDialogBuilder
				.setMessage("Click YES to Quit, NO to Proceed Next")
				.setCancelable(false)
				
				.setNegativeButton("No",new DialogInterface.OnClickListener() 
			    {
				    public void onClick(DialogInterface dialog,int id) 
				    {
				    	/* Failed test but proceed to next test case */
    				    Intent nIntent = new Intent(ProximityTest.this, tcNext);
    				    nIntent.putExtra("testModeType", 1);
    				    nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				    startActivity(nIntent);
	    		
					    /* End current activity */
					    finish();
				    }
			    })
			
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								/*
								Intent homescreenIntent = new Intent(
										Intent.ACTION_MAIN);
								homescreenIntent
										.addCategory(Intent.CATEGORY_HOME);
								homescreenIntent
										.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
										*/
								Intent nIntent = new Intent(ProximityTest.this, Test_Main.class);
								startActivity(nIntent);
								
					    		/* End current activity */							
					    		finish();
							}
						});

		/* Create alert dialog */
		AlertDialog alertDialog = alertDialogBuilder.create();

		/* Show it */
		alertDialog.show();
	}

	private void setBgClr() {
		// Set bg color to green
		View view = this.getWindow().getDecorView();
		view.setBackgroundColor(Color.GREEN);
	}

	protected void onStop()
	{
		super.onStop();
		finish();
	}
	
} // End of public class ProximityTest extends Test_Main

