/**
 * Revision History
 * ----------------
 * 08-AUG-2013 - Initial Draft. Added for version 11
 * 04-SEP-2013 - Remove Gyroscope test result recording
 * 06-JUN-2014 - Add Back Panel Test result recording (YotaDemo API APK)
 */

package com.hip.cqa;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class VerifyTestResult extends Activity implements
		OnItemSelectedListener {

	// Variable
	private Spinner sGPS, sEcompass, sBackPanel; // sGyroscope, sAccelerometer, sBackpanel
	private Button back, save;
	String gpsResult, ecompassResult, backpanelResult; // gyroscopeResult, accelerometerResult,
	TextView saveStatus;
	// boolean logOneTimeOnly = true; // if TRUE user unable to press save when
	// // there is data in the logfile
	boolean logFileExist = false;

	public static File testResultFile;
	private String filename = "resultLog2.xml";
	private String filepath = "log";
	/* Time */
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.set_test_result);

		saveStatus = (TextView) findViewById(R.id.tvSaveStatus);
		back = (Button) findViewById(R.id.bBackSetTestResult);
		save = (Button) findViewById(R.id.bSave);
		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Quit the page
				finish();
			}
		});

		save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// save the test result

				// Get the spinner value
				// accelerometerResult = sAccelerometer.getSelectedItem()
				// .toString();
				// gyroscopeResult = sGyroscope.getSelectedItem().toString();
				gpsResult = sGPS.getSelectedItem().toString();
				//ecompassResult = sEcompass.getSelectedItem().toString();
				backpanelResult = sBackPanel.getSelectedItem().toString();
				

				// Write to log file
				createTestResultFile();
				// writetoLog(accelerometerResult, gyroscopeResult, gpsResult,
				// ecompassResult);

				//writetoLog(gpsResult, ecompassResult);
				writetoLog(gpsResult, backpanelResult); 
				
				// Disable SAVE button when enable LogOneTimeOnly
				// if (logOneTimeOnly) {
				// save.setEnabled(false);
				// saveStatus.setText("Test result saved");
				// }

			}

		});

		addListenerOnButton();
		createTestResultFile();
		// Disable SAVE button when enable LogOneTimeOnly
		// if (logOneTimeOnly && logFileExist) {
		// save.setEnabled(false);
		// saveStatus.setText("Test result saved");
		// }

	}

	private void addListenerOnButton() {
		// TODO Auto-generated method stub
		// sAccelerometer = (Spinner) findViewById(R.id.sAccelerometer);
		// sGyroscope = (Spinner) findViewById(R.id.sGyroscope);
		sGPS = (Spinner) findViewById(R.id.sGPS);
		sEcompass = (Spinner) findViewById(R.id.sEcompass);
		sBackPanel = (Spinner) findViewById(R.id.sBackPanel);

		// sAccelerometer.setOnItemSelectedListener(this);
		// sGyroscope.setOnItemSelectedListener(this);
		sGPS.setOnItemSelectedListener(this);
		sEcompass.setOnItemSelectedListener(this);
		sBackPanel.setOnItemSelectedListener(this);
		
		
	}

	@Override
	public void onItemSelected(AdapterView<?> main, View v, int position,
			long Id) {
		switch (main.getId()) {
		// case R.id.sAccelerometer:
		// if (position != 0) {
		// String status = null;
		// if (position == 1)
		// status = "FAIL";
		// if (position == 2)
		// status = "PASS";
		// Toast.makeText(this, "Accelerometer: " + status,
		// Toast.LENGTH_SHORT).show();
		// }
		//
		// break;

		// case R.id.sGyroscope:
		// if (position != 0) {
		// String status = null;
		// if (position == 1)
		// status = "FAIL";
		// if (position == 2)
		// status = "PASS";
		// Toast.makeText(this, "Gyroscope: " + status, Toast.LENGTH_SHORT)
		// .show();
		// }
		// break;

		case R.id.sEcompass:
			if (position != 0) {
				String status = null;
				if (position == 1)
					status = "FAIL";
				if (position == 2)
					status = "PASS";
				Toast.makeText(this, "Ecompass: " + status, Toast.LENGTH_SHORT)
						.show();
			}
			break;

		case R.id.sGPS:
			if (position != 0) {
				String status = null;
				if (position == 1)
					status = "FAIL";
				if (position == 2)
					status = "PASS";
				Toast.makeText(this, "GPS: " + status, Toast.LENGTH_SHORT)
						.show();
			}
			break;
		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	private void createTestResultFile() {
		ContextWrapper contextWrapper = new ContextWrapper(
				getApplicationContext());
		File directory = contextWrapper.getDir(filepath, Context.MODE_PRIVATE);

		/*
		 * Create the full path to file in
		 * /data/data/com.hip.cqa/app_log/testLog
		 */
		testResultFile = new File(directory, filename); // This command does not
														// create "testLog"

		if (testResultFile.exists()) {
			/* Delete existing testLog */
			logFileExist = true;
			testResultFile.delete();
		}

	}

	private void writetoLog(String gpsResult, String backpanelResult) {

		/* Save start test time to resultLog */
		StringBuilder builder = new StringBuilder("");

		/* Set current data and time */
		String TestTime = date.format(new Date());
		builder.append(TestTime)
				.append("\n")
				// .append("Accelerometer - ")
				// .append(accelerometerResult).append("\n")
				// .append("Gyroscope - ").append(gyroscopeResult).append("\n")
				.append("GPS - ").append(gpsResult).append("\n")
				//.append("Ecompass - ").append(ecompassResult);
				.append("EPD BackPanel - ").append(backpanelResult).append("\n");

		try {
			/* /data/data/com.hip.cqa/app_log/resultLog */
			BufferedWriter buf = new BufferedWriter(new FileWriter(
					testResultFile, true));
			buf.append(builder.toString());
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		saveStatus.setText("Result SAVED");

	}

}
