/**
 * Revision History
 * ----------------
 * 10-JUN-2013 - Initial Draft
 */

package com.hip.cqa.key;

import android.os.Build;
import android.os.Build.VERSION;
import android.view.KeyEvent;
import com.hip.cqa.Test_Main;
import android.util.Log;

public class KeyCode /* This API will convert the keycode (int) to string, return string to caller and print to screen */
{
	/* Convert keycode to string */
	public static String keyCodeToStr(int paramInt)
	{
		Log.d("KeyCode", "In Class KeyCode");
	    if (Build.VERSION.SDK_INT >= 12)
	    {
	    	Log.d("KeyCode", "Keycode - checkpoint 1");
	        return KeyEvent.keyCodeToString(paramInt); /* Higher SDK version supports direct return of keycodetoString api */
	    }
	    else /* Platinum only supports VOL-DOWN, VOL-UP, POWER KEY, HEADSET Button. Does Platinum use key_code_map.txt in baseline to define the supported keys? */
	    {
	    	Log.d("KeyCode", "Keycode - checkpoint 2" + paramInt);
	    	switch (paramInt)
	        {
		        case 3:
		        	Log.d("KeyCode", "Keycode - checkpoint 2.1");
		        	return "HOME Soft Key";
		        case 4:
		        	Log.d("KeyCode", "Keycode - checkpoint 2.2");
		        	return "BACK Soft key";
		        case 19:
		        	Log.d("KeyCode", "Keycode - checkpoint 2.3");
		        	return "DPad Up Key";
		        case 20:
		        	Log.d("KeyCode", "Keycode - checkpoint 2.4");
		        	return "DPad Down Key";
		        case 21:
		        	Log.d("KeyCode", "Keycode - checkpoint 2.5");
		        	return "DPad Left Key";
		        case 22:
		        	Log.d("KeyCode", "Keycode - checkpoint 2.6");
		        	return "DPad Right Key";
		        case 24:
		        	Log.d("KeyCode", "Keycode - checkpoint 2.7");
		        	return "VOLUME UP Key";
		        case 25:
		        	Log.d("KeyCode", "Keycode - checkpoint 2.8");
		        	return "VOLUME DOWN Key";
		        case 26:
		        	Log.d("KeyCode", "Keycode - checkpoint 2.9");
		        	return "POWER Key";
		        case 79:
		        	Log.d("KeyCode", "Keycode - checkpoint 2.10");
		        	return "HEADSET Key";
		        case 82:
		        	Log.d("KeyCode", "Keycode - checkpoint 2.11");
		        	return "MENU Soft Key";
		        case 84:
		        	Log.d("KeyCode", "Keycode - checkpoint 2.12");
		        	return "SEARCH Key";
		        default:
		        	Log.d("KeyCode", "Keycode - checkpoint 2.13");
		        	return "UNKNOWN Key: " + paramInt;
	        }
   
	    	//return "Record Key";	    	
	    }
	}
}





