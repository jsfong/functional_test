/**
 * Revision History
 * ----------------
 * 10-JUN-2013 - Initial Draft
 */

package com.hip.cqa.vibrator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.util.Log;

import com.hip.cqa.R;
import com.hip.cqa.Test_Main;
import com.hip.cqa.acc.AccelerometerTest;
import com.hip.cqa.display.testpattern.TestPattern;
import com.hip.cqa.simcard.SimCardTest;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.Test_All_Mode;

public class VibratorTest extends Test_Main
{
	/* Define Local Variables */
	private int mDelayBeforeVibratorStart = 0;
	private int mVibratorOffTime = 100;
	private int mVibratorOnTime = 1000;
	private Vibrator vibratorService;
	private ToggleButton localToggleButton;
	private int testMode;
	
	private static String testCaseStr = "Vibrator Test - ";
	private static String testCaseResult = "Not Tested";
	final Context context = this;
	
	/* Assign class name Next test case to variable */
	private Class tcNext = SimCardTest.class;
	
	public void onCreate(Bundle paramBundle)
	{
		this.TAG = "Hi-P_Vibrator";
	  
		super.onCreate(paramBundle);
		setContentView(R.layout.vibrator);
	  
        /* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");
		
		this.vibratorService = ((Vibrator)getSystemService("vibrator"));
		
		/* Monitor ToggleButton status */
		localToggleButton = (ToggleButton)findViewById(R.id.toggleButton_vibrator_on_off);
		localToggleButton.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v)
			{
				if (localToggleButton.isChecked())
				{
					VibratorTest.this.vibratorService.vibrate(new long[] { 10L, 10L, 100L, 1000L }, 0);
					//Toast.makeText(VibratorTest.this, VibratorTest.this.getString(R.id.toggleButton_vibrator_on_off), 0).show();
					//return;
				}
				else
				{
					VibratorTest.this.vibratorService.cancel();
				}
				//Toast.makeText(VibratorTest.this, VibratorTest.this.getString(R.id.toggleButton_vibrator_on_off), 0).show();
	    }
	  });
	}

    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
    	if(keyCode == KeyEvent.KEYCODE_VOLUME_UP)
    	{
    		finish();
    		
			if (this.testMode == 1)
			{
				/* Set test result status */
				testCaseResult = "PASS"; 
				
				Intent nIntent = new Intent(this, tcNext);
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}
			else /* Individual test case */
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}
    	}
    	else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) && (this.testMode == 1))
    	{
    		testCaseResult = "FAIL";
    		showQuitTestAlert();

    	}
    	else
    	{
    		/* Reject other keyevent */
    		Toast.makeText(this, getString(R.string.mode_continue_next), 0).show();
    	}
    	
    	if(!(testCaseResult.equals("Not Tested")))
    	{
    		if(testCaseResult.equals("PASS"))
    			db.setTestCasePassFail(Test_Main.tcVibrator,true);
    		if(testCaseResult.equals("FAIL"))
    			db.setTestCasePassFail(Test_Main.tcVibrator,false);
    		
    		/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr + testCaseResult);

			try 
			{
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter
						(Test_All_Mode.testResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}   
    	}
    	
    	return true;
    }
    
	protected void onDestroy()
	{
	  super.onDestroy();
	  this.vibratorService.cancel();
	  finish();
	}

    private void showQuitTestAlert()
    {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
 
		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");
		
		/* Set dialog message */
		alertDialogBuilder
			.setMessage("Click YES to Quit, NO to Proceed Next")
			.setCancelable(false)
			
			.setNegativeButton("No",new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog,int id) 
				{
					/* Failed test but proceed to next test case */
    				Intent nIntent = new Intent(VibratorTest.this, tcNext);
    				nIntent.putExtra("testModeType", 1);
    				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				startActivity(nIntent);
	    		
					/* End current activity */
					finish();
				}
			})
			
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog,int id) 
				{
					/*
					Intent homescreenIntent = new Intent(
							Intent.ACTION_MAIN);
					homescreenIntent
							.addCategory(Intent.CATEGORY_HOME);
					homescreenIntent
							.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							*/
					Intent nIntent = new Intent(VibratorTest.this, Test_Main.class);
					startActivity(nIntent);
		    		
		    		/* End current activity */
		    		finish();
				}
			 });

			/* Create alert dialog */
			AlertDialog alertDialog = alertDialogBuilder.create();
		 
			/* Show it */
			alertDialog.show();
    }	
    
	protected void onStop()
	{
		super.onStop();
		finish();
	}
}

