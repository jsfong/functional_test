/**
 * Revision History
 * ----------------
 * 10-JUN-2013 - Initial Draft
 */

package com.hip.cqa.led;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.hip.cqa.R;
import com.hip.cqa.Test_Main;
import com.hip.cqa.Test_Individual_Mode;
//import com.hip.cqa.eink.EinkWaveTestActivity;
import com.hip.cqa.Test_All_Mode;
import com.hip.cqa.acc.AccelerometerTest;
import com.hip.cqa.gyro.GyroscopeTest;
import com.hip.cqa.touch.MainActivity;

public class LedTest extends Test_Main
{
	/* Define Local Variables */
	private ToggleButton localToggleButton;
	private Camera camera;
	private Parameters camParam;
	private boolean isFlashSupported = false;
	private boolean isFlashOn = false;
	private int testMode;
	
	private static String testCaseStr = "LED Test - ";
	private static String testCaseResult = "Not Tested";
	final Context context = this;

	/* Assign class name of Next test case to variable */
	private Class tcNext = MainActivity.class; /* Touch main activity */
	
	public void onCreate(Bundle paramBundle)
	{
		this.TAG = "Hi-P_LED";
	  
		super.onCreate(paramBundle);
		setContentView(R.layout.led);
	  
        /* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");
		
		/* Initialize toggle ON/OFF button */
		localToggleButton = (ToggleButton)findViewById(R.id.toggleButton_flashled_on_off);
		
		/* Check for Flash support */
		isFlashSupported = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
		Log.d(LedTest.this.TAG, "Flash LED supported = " + isFlashSupported);
		
		if(isFlashSupported ==  true)
		{
			/* Initialize Camera Parameters */
			camera = Camera.open();
			camParam = camera.getParameters();
		
			/* Monitor ToggleButton status */
			localToggleButton.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (isFlashOn ==  false)
					{
						Log.d(LedTest.this.TAG, "Flash LED turn ON");
							
						camParam.setFlashMode(camParam.FLASH_MODE_TORCH);
						camera.setParameters(camParam);
						camera.startPreview();
						isFlashOn = true;

						Toast.makeText(LedTest.this, "FLASH LED ON", 0).show();
					}
					else
					{
						/* Turn OFF Camera Flash Light */
						Log.d(LedTest.this.TAG, "Flash LED turn OFF");
							
						camParam.setFlashMode(camParam.FLASH_MODE_OFF);
						camera.setParameters(camParam);
						camera.stopPreview();
						isFlashOn = false;
							
						Toast.makeText(LedTest.this, "FLASH LED OFF", 0).show();						
					}
				}
		  });
		}
		else
		{
			Log.e(LedTest.this.TAG, "Flash LED NOT SUPPORTED");
			return;
		}
	}
	
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
    	Log.d(this.TAG, "keyCode = " + keyCode);
    	if(keyCode == KeyEvent.KEYCODE_VOLUME_UP)
    	{
    		finish();
    		
			if (this.testMode == 1)
			{
				/* Set test result status */
				testCaseResult = "PASS"; 
				
				//Intent nIntent = new Intent(this, EinkWaveTestActivity.class); 
				Intent nIntent = new Intent(this, tcNext); /* Touch main activity */
				nIntent.putExtra("testModeType", 1);
	        	nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        	startActivity(nIntent);
			}
			else /* Individual test case */
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}
			
			/* Release camera resource */
			camera.release();
    	}
    	else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) && (this.testMode == 1))
    	{
    		testCaseResult = "FAIL";
    		showQuitTestAlert();

    	}
    	else
    	{
    		/* Reject other keyevent */
    		Toast.makeText(this, getString(R.string.mode_continue_next), 0).show();
    	}
    	
    	if(!(testCaseResult.equals("Not Tested")))
    	{
    		/* Save result to database */
			if(testCaseResult.equals("PASS"))
    			db.setTestCasePassFail(Test_Main.tcFlashLED,true);
    		if(testCaseResult.equals("FAIL"))
    			db.setTestCasePassFail(Test_Main.tcFlashLED,false);
    		
			/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr + testCaseResult);

			try 
			{
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter
						(Test_All_Mode.testResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}   
    	}
    	
    	return true;
    }
    
	protected void onDestroy()
	{
		super.onDestroy();
	}
	
    private void showQuitTestAlert()
    {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
 
		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");
		
		/* Set dialog message */
		alertDialogBuilder
			.setMessage("Click YES to Quit, NO to Proceed Next")
			.setCancelable(false)
			
			.setNegativeButton("No",new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog,int id) 
				{
					/* Failed test but proceed to next test case */
    				Intent nIntent = new Intent(LedTest.this, tcNext);
    				nIntent.putExtra("testModeType", 1);
    				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				startActivity(nIntent);
	    		
					/* End current activity */
					finish();
				}
			})
			
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog,int id) 
				{
					/*
					Intent homescreenIntent = new Intent(
							Intent.ACTION_MAIN);
					homescreenIntent
							.addCategory(Intent.CATEGORY_HOME);
					homescreenIntent
							.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							*/
					Intent nIntent = new Intent(LedTest.this, Test_Main.class);
					startActivity(nIntent);
		    		
		    		/* End current activity */
		    		finish();
				}
			 });

			/* Create alert dialog */
			AlertDialog alertDialog = alertDialogBuilder.create();
		 
			/* Show it */
			alertDialog.show();
    }	
 
	protected void onStop()
	{
		super.onStop();
		finish();
	}    
}
