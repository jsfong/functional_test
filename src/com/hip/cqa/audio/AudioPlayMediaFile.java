/**
 * Revision History
 * ----------------
 * 10-JUN-2013 - Initial Draft. AudioMicLoopBack test was skipped for P1-2
 * 30-MAR-2014 - Titnaium: Skip Flash LED test for P1A
 */

package com.hip.cqa.audio;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.hip.cqa.Test_Main;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.R;
import com.hip.cqa.Test_All_Mode;
import com.hip.cqa.led.LedTest;

public class AudioPlayMediaFile extends Test_Main
{
	private AudioManager audioManager;
	private int initMediaVolume;
	private int maxMediaVolume;
	private MediaPlayer mediaPlayer;
	private Button playExtSpeakerButton;
	private Button playEarpieceButton;
	private Boolean isPlayActive = false;
	private int testMode;
	
	private static String testCaseStr = "Audio Play MediaFile - ";
	private static String testCaseResult = "Not Tested";
	final Context context = this;
	
	/* Assign class name of Next test case to variable */
	private Class tcNext = LedTest.class;
	
	protected void onCreate(Bundle paramBundle)
	{
		this.TAG = "Hi-P_Audio_PlayMediaFile";
		super.onCreate(paramBundle);	
		setContentView(R.layout.audio_mediaplay);
		 
        /* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");
					
		this.audioManager = (AudioManager)getSystemService("audio");
				
		/* Set volume for playback */
		this.initMediaVolume = this.audioManager.getStreamVolume(3);
		this.maxMediaVolume = this.audioManager.getStreamMaxVolume(3);
		
		Log.d(this.TAG, "initMediaVolume = " + this.initMediaVolume);
		Log.d(this.TAG, "maxMediaVolume = " + this.maxMediaVolume);
				 
		 /* Set Playback volume */
		 AudioPlayMediaFile.this.audioManager.setStreamVolume(3, 
				 ((AudioPlayMediaFile.this.maxMediaVolume)/2), 0);
		 
		this.playExtSpeakerButton = (Button) findViewById(R.id.button_extloudspeaker_playmediafile);
		this.playExtSpeakerButton.setOnClickListener(playMediaFileListener);	
		
		this.playEarpieceButton = (Button) findViewById(R.id.button_earpiece_playmediafile);
		this.playEarpieceButton.setOnClickListener(playMediaFileListener);
			 
	 }
	
	 private OnClickListener playMediaFileListener = new View.OnClickListener()
	 {
		 public void onClick(View v)
		 {
			 if(isPlayActive == false) /* User to select "START PLAY" */
			 {
				 if(v.getId() == R.id.button_extloudspeaker_playmediafile)
				 {					 
					 /* User selected External Loudspeaker playback */
					 AudioPlayMediaFile.this.playExtSpeakerButton.setText("External Loudspeaker Stop Play");
					 AudioPlayMediaFile.this.playExtSpeakerButton.setEnabled(true);
					 
					 AudioPlayMediaFile.this.playEarpieceButton.setText("Earpiece Start Play");
					 AudioPlayMediaFile.this.playEarpieceButton.setEnabled(false);
					 
					 AudioPlayMediaFile.this.audioManager.setSpeakerphoneOn(true);
					 AudioPlayMediaFile.this.audioManager.setMode(AudioManager.MODE_NORMAL);
					 
				 }
				 else
				 {					 
					 /* User selected Earpiece playback */
					 AudioPlayMediaFile.this.playEarpieceButton.setText("Earpiece Stop Play");
					 AudioPlayMediaFile.this.playEarpieceButton.setEnabled(true);
					 
					 AudioPlayMediaFile.this.playExtSpeakerButton.setText("External Loudspeaker Start Play");
					 AudioPlayMediaFile.this.playExtSpeakerButton.setEnabled(false);
					 
					 AudioPlayMediaFile.this.audioManager.setSpeakerphoneOn(false);
					 AudioPlayMediaFile.this.audioManager.setMode(AudioManager.MODE_IN_CALL);
				 }

				 /* Start play Audio */
				 AudioPlayMediaFile.this.playAudio();
				 isPlayActive = true;					 
			 }	 
			 else /* User to select "STOP PLAY" */
			 {
				 if(v.getId() == R.id.button_extloudspeaker_playmediafile)
				 {
					 AudioPlayMediaFile.this.playExtSpeakerButton.setText("External Loudspeaker Start Play");
					 AudioPlayMediaFile.this.playEarpieceButton.setEnabled(true);
				 }
				 else
				 {
					 AudioPlayMediaFile.this.playEarpieceButton.setText("Earpiece Start Play");
					 AudioPlayMediaFile.this.playExtSpeakerButton.setEnabled(true);
				 }
				 
				 /* Stop play Audio */
				 AudioPlayMediaFile.this.mediaPlayer.release();
				 isPlayActive = false;
			 }
		 }
	 };

	 private void playAudio()
	 {
		 this.mediaPlayer = new MediaPlayer();

		 if (this.mediaPlayer != null)
		 {     
			 this.mediaPlayer.release();
		 }
	        
		 this.mediaPlayer = MediaPlayer.create(this, R.raw.audiofile1); /* Play selected audio file */
		 MediaPlayer localMediaPlayer = this.mediaPlayer;
	     
		 if (localMediaPlayer == null)
	     {
	    	 return;
	     }
	     else
	     {
	    	 this.mediaPlayer.setLooping(true); /* Enable playback looping */
	         this.mediaPlayer.start();    /* Start media file playback */
	     }
	}

	private void release()
	{
		if (this.mediaPlayer != null)
		{  
			this.audioManager.setStreamVolume(3, this.initMediaVolume, 0);
		}

	    this.mediaPlayer.release();
	    this.mediaPlayer = null;
	    return;
	}

    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
    	if(keyCode == KeyEvent.KEYCODE_VOLUME_UP)
    	{
    		if(AudioPlayMediaFile.this.mediaPlayer != null)
    		{
    			AudioPlayMediaFile.this.mediaPlayer.release();
    		}
    		
    		/* Re-enable all selections */
    		AudioPlayMediaFile.this.playEarpieceButton.setText("Earpiece Start Play");
    		AudioPlayMediaFile.this.playExtSpeakerButton.setText("External Loudspeaker Start Play");
    		AudioPlayMediaFile.this.playEarpieceButton.setEnabled(true);
    		AudioPlayMediaFile.this.playExtSpeakerButton.setEnabled(true);
    		isPlayActive = false;

    		finish();
    		
			if (this.testMode == 1)
			{
				/* Set test result status */
				testCaseResult = "PASS"; 
				
				//Intent nIntent = new Intent(this, LedTest.class); /* Titanium: Skip for P1A */
				Intent nIntent = new Intent(this, tcNext); /* FlashLED test */
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}
			else /* Individual test case */
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}
    	}
    	else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) && (this.testMode == 1))
    	{
    		testCaseResult = "FAIL";
    		showQuitTestAlert();

    	}
    	else
    	{
    		/* Reject other keyevent */    	
    		Toast.makeText(this, getString(R.string.mode_continue_next), 0).show();
    	}
  
    	if(!(testCaseResult.equals("Not Tested")))
    	{
    		if(testCaseResult.equals("PASS"))
    			db.setTestCasePassFail(Test_Main.tcAudioMedia,true);
    		if(testCaseResult.equals("FAIL"))
    			db.setTestCasePassFail(Test_Main.tcAudioMedia,false);
			/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr + testCaseResult);

			try 
			{
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter
						(Test_All_Mode.testResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}   
    	}
    	
    	return true;
    }
    
	public void onPause()
	{
		super.onPause();
	}

	public void onResume()
	{
		super.onResume();
	}

    private void showQuitTestAlert()
    {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
 
		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");
		
		/* Set dialog message */
		alertDialogBuilder
			.setMessage("Click YES to Quit, NO to Proceed Next")
			.setCancelable(false)
			
			.setNegativeButton("No",new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog,int id) 
				{
					/* Failed test but proceed to next test case */
    				Intent nIntent = new Intent(AudioPlayMediaFile.this, tcNext);
    				nIntent.putExtra("testModeType", 1);
    				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				startActivity(nIntent);
	    		
					/* End current activity */
					finish();
				}
			})
			
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog,int id) 
				{
					/*
					Intent homescreenIntent = new Intent(
							Intent.ACTION_MAIN);
					homescreenIntent
							.addCategory(Intent.CATEGORY_HOME);
					homescreenIntent
							.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							*/
					Intent nIntent = new Intent(AudioPlayMediaFile.this, Test_Main.class);
					startActivity(nIntent);
		    		
		    		/* End current activity */
		    		finish();
				}
			 });

			/* Create alert dialog */
			AlertDialog alertDialog = alertDialogBuilder.create();
		 
			/* Show it */
			alertDialog.show();
    }	
    
	protected void onStop()
	{
		super.onStop();
		finish();
	}
	
} //End of public class AudioPlayMediaFile
