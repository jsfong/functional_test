/**
 * Revision History
 * ----------------
 * 10-JUN-2013 - Initial Draft. AudioMicLoopBack test was skipped for P1-2
 * 03-SEP-2013 - Fix wrong audio routing. Change setMode(audioManager.MODE_IN_CALL) to setMode(audioManager.MODE_NORMAL) 
 */

package com.hip.cqa.audio;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder.AudioSource;
import android.os.Bundle;
import android.os.Process;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.util.Log;

import com.hip.cqa.Test_Main;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.R;
import com.hip.cqa.headset.HeadsetInfoTest;
import com.hip.cqa.Test_All_Mode;

public class AudioMicLoopBack extends Test_Main
{
	  private final int AUDIO_SAMPLE_RATE = 8000;
	  private final int AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
	  private final int AUDIORECORD_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
	  private final int AUDIOTRACK_CHANNELS = AudioFormat.CHANNEL_OUT_MONO;	  
	  
	  private AudioManager audioManager;
	  private AudioRecord localAudioRecord = null;
	  private AudioTrack localAudioTrack = null;
	  
	  private int initVolume;
	  private boolean isLoopback = false;
	  private boolean isRecording = false;
	  private boolean isTestActive = false;

	  private Thread recordingThread = null; 
	  
	  private int testMode;

	  private Button button_mic_loopback;
	  
	  private RadioGroup radioMicGroup;
	  private RadioButton radioMicTestButton;
	  private RadioButton radioPrimaryMicButton;
	  private RadioButton radioSecondaryMicButton;
	  private RadioButton radioHeadsetMicButton;
	  
	  private static String testCaseStr = "Audio MIC Loopback Test - ";
	  private static String testCaseResult = "Not Tested";
	  final Context context = this;
		
	  /* Assign class name of Next test case to variable */
	  private Class tcNext = HeadsetInfoTest.class;
		
	  public void onCreate(Bundle paramBundle)
	  {
		  this.TAG = "Hi-P_Mic_Loopback";
		  super.onCreate(paramBundle);
		  setContentView(R.layout.audio_loopback);
		  
		  /* Read Test Mode Type selected by user */
		  Bundle testModeSelected = getIntent().getExtras();
		  this.testMode = testModeSelected.getInt("testModeType");
		  
		  /* MIC Type Radiobutton */
		  radioMicGroup = (RadioGroup) findViewById(R.id.radioButton_mic_loopback);		
		  radioPrimaryMicButton = (RadioButton) findViewById(R.id.radioButton_mic_primary_loopback);
		  radioSecondaryMicButton = (RadioButton) findViewById(R.id.radioButton_mic_secondary_loopback);
		  radioHeadsetMicButton = (RadioButton) findViewById(R.id.radioButton_mic_headset_loopback);
		  
		  /* Start/Stop Loopback button */
		  button_mic_loopback = (Button) findViewById(R.id.button_mic_start_stop_audioloopback);
		  
		  /* Disable button click sound */
		  radioPrimaryMicButton.setSoundEffectsEnabled(false);
		  radioSecondaryMicButton.setSoundEffectsEnabled(false); 
		  radioHeadsetMicButton.setSoundEffectsEnabled(false);
		  button_mic_loopback.setSoundEffectsEnabled(false);
		  	  
		  AudioMicLoopBack.this.loopBackInit();
		  Log.d(this.TAG,"Calling for loopbackInit");
		  
		  Log.d(this.TAG, "Start listening for user input");
		  addStartTestListenerOnButton(); /* Handle MIC Loopback Selection */
		  
	  }
	  
		
	  private void addStartTestListenerOnButton()
	  {		    
		  button_mic_loopback.setOnClickListener (onStartTestClickListener);  
	  }
	  
	  /* Define class for onClickListener */
	  private OnClickListener onStartTestClickListener = new OnClickListener()
	  {
		  public void onClick(View v)
		  {
			  /* Get selected Radiobutton ID from radioGroup */
			  int selectedMICTest = radioMicGroup.getCheckedRadioButtonId();
			  
			  /* Get radiobutton by Radiobutton ID */
			  radioMicTestButton = (RadioButton) findViewById(selectedMICTest);
			  		  
			  if(isTestActive == false) /* USER to select "START LOOPBACK" */
			  {
				  AudioMicLoopBack.this.button_mic_loopback.setText("STOP Loopback");
				  
				  /* Disable ALL Radiobutton selection during loopback testing */
				  radioPrimaryMicButton.setEnabled(false);
				  radioSecondaryMicButton.setEnabled(false);
				  radioHeadsetMicButton.setEnabled(false);
				  
				  if(radioMicTestButton.getId() == R.id.radioButton_mic_primary_loopback)
				  {
					  AudioMicLoopBack.this.audioManager.setParameters("hip_test=primary");
					  AudioMicLoopBack.this.audioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, 
							  (AudioMicLoopBack.this.audioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL)), 0);
				  }
				  else if(radioMicTestButton.getId() == R.id.radioButton_mic_secondary_loopback)
				  {
					  AudioMicLoopBack.this.audioManager.setParameters("hip_test=secondary");
					  AudioMicLoopBack.this.audioManager.setSpeakerphoneOn(true);
					  AudioMicLoopBack.this.audioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, 
							  (AudioMicLoopBack.this.audioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL)), 0);
				  }
				  else if(radioMicTestButton.getId() == R.id.radioButton_mic_headset_loopback)
				  {
					  AudioMicLoopBack.this.audioManager.setParameters("hip_test=none");
				  }
				  
				  startLoopback();
				  isTestActive = true;
			  }
			  else /* USER to select "STOP LOOPBACK" */
			  {
				  stopLoopback();
				  isTestActive = false;
				  
				  AudioMicLoopBack.this.button_mic_loopback.setText("START Loopback");
				  
				  /* Disable ALL Radiobutton selection during loopback testing */
				  radioPrimaryMicButton.setEnabled(true);
				  radioSecondaryMicButton.setEnabled(true);
				  radioHeadsetMicButton.setEnabled(true);	

				  AudioMicLoopBack.this.audioManager.setSpeakerphoneOn(false);
			  }
		  }
	  };
	  	  
	  private void loopBackInit()
	  {	    
		  this.audioManager = ((AudioManager) getSystemService("audio"));
		  this.audioManager.setMode(audioManager.MODE_NORMAL);
	  }

	  private void startLoopback()
	  {
		  this.initVolume = this.audioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL);
		  		  
		  AudioMicLoopBack.this.recordingThread = new Thread(new Runnable()
		  {
			  public void run()
			  {
				  Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
	        	  
	        	  int bufferSize_i = AudioRecord.getMinBufferSize(AUDIO_SAMPLE_RATE, AUDIORECORD_CHANNELS, AUDIO_ENCODING);
	        	  int bufferSize_j = AudioTrack.getMinBufferSize(AUDIO_SAMPLE_RATE, AUDIOTRACK_CHANNELS, AUDIO_ENCODING);
	        	  byte[] arrayOfByte = new byte[bufferSize_i];

	        	  localAudioRecord = new AudioRecord(AudioSource.MIC, AUDIO_SAMPLE_RATE, AUDIORECORD_CHANNELS, AUDIO_ENCODING, bufferSize_i);
	        	  //localAudioTrack = new AudioTrack(AudioManager.STREAM_VOICE_CALL, AUDIO_SAMPLE_RATE, AUDIOTRACK_CHANNELS, AUDIO_ENCODING, bufferSize_j, AudioTrack.MODE_STREAM);
	        	  localAudioTrack = new AudioTrack(AudioManager.STREAM_DTMF, AUDIO_SAMPLE_RATE, AUDIOTRACK_CHANNELS, AUDIO_ENCODING, bufferSize_j, AudioTrack.MODE_STREAM);
	        	  
	        	  localAudioTrack.setPlaybackRate(AUDIO_SAMPLE_RATE);
	        	  localAudioRecord.startRecording();
	        	  
	        	  AudioMicLoopBack.this.isRecording = true;
	        	  AudioMicLoopBack.this.isLoopback = true;
        	    
	           	  try
	           	  {
	           		  localAudioTrack.play();
	           		  
	           		  /* Start Record and Playback simultaneously */
	           		  while (AudioMicLoopBack.this.isRecording)
	           		  {
	           			  localAudioRecord.read(arrayOfByte, 0, bufferSize_i);
	           			  localAudioTrack.write(arrayOfByte, 0, bufferSize_i);
	           		  }
	           	  }
	                  	
	           	  catch (Throwable localThrowable)
	           	  {     			  
	           		  localAudioRecord.stop();
	           		  localAudioRecord.release();
	           		  Log.d(AudioMicLoopBack.this.TAG, "Error in loopback. To stop AudioRecord");
	            
	           		  localAudioTrack.stop();
	           		  localAudioTrack.release();
	           		Log.d(AudioMicLoopBack.this.TAG, "Error in loopback. To stop AudioTrack");
	           	  }
			  }
		  }, "AudioRecorder Thread"); //end of new Thread(new Runnable()
		  
		  recordingThread.start(); 
	  } //end of private void startLoopback
 
	  @Override
	  public boolean onKeyDown(int keyCode, KeyEvent event) 
	  {
	      if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) 
	      {
	    	  /* Only run this if user does not stop loopback before exiting test case */
	    	  if (isTestActive == true)
	    	  {
	    		  stopLoopback();
	    	  }
	    	  
	          finish();
				
	          if (this.testMode == 1)
	          {
	        	  /* Set test result status */
	        	  testCaseResult = "PASS"; 

	        	  Intent nIntent = new Intent(this, tcNext); /* Headset test */
	        	  nIntent.putExtra("testModeType", 1);
	        	  nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        	  startActivity(nIntent);
				}
				else /* Individual test case */
				{
					Intent nIntent = new Intent(this, Test_Individual_Mode.class);
					nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(nIntent);
				}
	      }
	      else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) && (this.testMode == 1))
	      {
	    	  testCaseResult = "FAIL";
	    	  showQuitTestAlert();

	      }
	      else
	      {
	    	  /* Reject other keyevent */
	    	  Toast.makeText(this, getString(R.string.mode_continue_next), 0).show();
	      }
	      

	      if(!(testCaseResult.equals("Not Tested")))
	      {
	    		if(testCaseResult.equals("PASS"))
	    			db.setTestCasePassFail(Test_Main.tcAudioMic,true);
	    		if(testCaseResult.equals("FAIL"))
	    			db.setTestCasePassFail(Test_Main.tcAudioMic,false);
	    	  /* Save start test time to resultLog */
	    	  StringBuilder builder = new StringBuilder("");
	    	  builder.append(testCaseStr + testCaseResult);
	    	  Log.d("In Test_All_Mode", "Test result = " + testCaseStr + testCaseResult);

	    	  try 
	    	  {
	    		  /* /data/data/com.hip.cqa/app_log/resultLog */
	    		  BufferedWriter buf = new BufferedWriter(new FileWriter
	    				  (Test_All_Mode.testResultFile, true));
	    		  buf.append(builder.toString());
	    		  buf.newLine();
	    		  buf.close();
	    	  } 
	    	  catch (IOException e) 
	    	  {
	    		  // TODO Auto-generated catch block
	    		  e.printStackTrace();
	    	  }   
	      }
	    	
	      return true;
	  }
	  
	  private void stopLoopback()
	  {		  
	    Log.d(this.TAG, "Calling for stopLoopback()");
		if (this.audioManager == null)
	    {
	    	return;
	    }
	    
	    this.audioManager.setStreamVolume(0, this.initVolume, 0);
	    this.isRecording = false;
	    this.isLoopback = false;
	    
		/* Reset Audio Parameters in HAL */
	    AudioMicLoopBack.this.audioManager.setParameters("hip_test=none");
	    
	    /* Turn off Phone Speaker */
		this.audioManager.setSpeakerphoneOn(false);

		
	    /* Release all AudioRecord and AudioTrack objects */
   		if (this.localAudioRecord != null)
   		{
   			Log.d(AudioMicLoopBack.this.TAG, "In stopLoopback. To stop AudioRecord");
   			this.localAudioRecord.stop();
   			this.localAudioRecord.release();
   		}
  
   		if(this.localAudioTrack != null)
   		{
   			Log.d(AudioMicLoopBack.this.TAG, "In stopLoopback. To stop AudioTrack");
   			this.localAudioTrack.stop();
   			this.localAudioTrack.release();
   		}
	      
	    try
	    {
	    	Thread.sleep(100L);
	        return;
	      }
	      
	      catch (Exception localException)
	      {
	    	  Log.d(this.TAG, "sleep exceptions...");
	      }
	  } //end of stopLoopback()
	  
	  public void onDestroy()
	  {
		  Log.d(this.TAG, "onDestroy");
		  super.onDestroy();
		  if (this.isLoopback)
		  {
			  stopLoopback();
		  }
	  }

	  protected void onResume()
	  {
	    super.onResume();
	  }
	  
	    private void showQuitTestAlert()
	    {
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
	 
			/* Set title */
			alertDialogBuilder.setTitle("Test Failed");
			
			/* Set dialog message */
			alertDialogBuilder
				.setMessage("Click YES to Quit, NO to Proceed Next")
				.setCancelable(false)
				
				.setNegativeButton("No",new DialogInterface.OnClickListener() 
			    {
				    public void onClick(DialogInterface dialog,int id) 
				    {
					    /* Failed test but proceed to next test case */
    				    Intent nIntent = new Intent(AudioMicLoopBack.this, tcNext);
    				    nIntent.putExtra("testModeType", 1);
    				    nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				    startActivity(nIntent);
	    		
					    /* End current activity */
					    finish();
				    }
			    })
			
				.setPositiveButton("Yes",new DialogInterface.OnClickListener() 
				{
					public void onClick(DialogInterface dialog,int id) 
					{
						/*
						Intent homescreenIntent = new Intent(
								Intent.ACTION_MAIN);
						homescreenIntent
								.addCategory(Intent.CATEGORY_HOME);
						homescreenIntent
								.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								*/
						Intent nIntent = new Intent(AudioMicLoopBack.this, Test_Main.class);
						startActivity(nIntent);
			    		
			    		/* End current activity */
			    		finish();
					}
				 });

				/* Create alert dialog */
				AlertDialog alertDialog = alertDialogBuilder.create();
			 
				/* Show it */
				alertDialog.show();
	    }	
	    
		protected void onStop()
		{
			super.onStop();
			finish();
		}
		
} //public class AudioMicLoopBack
