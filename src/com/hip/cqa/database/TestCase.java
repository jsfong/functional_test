package com.hip.cqa.database;

public class TestCase {
	
	//private variable
	int _id;
	String _test_case_name;
	String _test_time;
	int _test_case_status;
	
	//Empty constructor
	public TestCase(){
		
	}
	
	//Constructor
	public TestCase(int _id, String _test_case_name, String _test_time,
			 int _test_case_status) {
		super();
		this._id = _id;
		this._test_case_name = _test_case_name;
		this._test_time = _test_time;
		this._test_case_status = _test_case_status;
	}	
	
	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String get_test_case_name() {
		return _test_case_name;
	}

	public void set_test_case_name(String _test_case_name) {
		this._test_case_name = _test_case_name;
	}

	public String get_test_time() {
		return _test_time;
	}

	public void set_test_time(String _test_time) {
		this._test_time = _test_time;
	}

	public int get_test_case_status() {
		return _test_case_status;
	}

	public void set_test_case_status(int _test_case_status) {
		this._test_case_status = _test_case_status;
	}

	

}
