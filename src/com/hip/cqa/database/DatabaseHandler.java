package com.hip.cqa.database;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper{
	
	//All Stativ Variabes
	//Database Version
	private static final int DATABASE_VERSION = 1;
	
	//Database Name
	private static final String DATABASE_NAME = "testCaseManager";
	
	//Test Case table name
	private static final String TABLE_TESTCASES = "testcase";
	
	//Test case Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_NAME = "name";
	private static final String KEY_TIME = "time";
	private static final String KEY_STATUS = "status";

	private static final String TAG = "Test Case Database";

	public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
	
	public DatabaseHandler(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);		
	}

	//Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_TESTCASE_TABLE = "CREATE TABLE " + TABLE_TESTCASES + "("
				+ KEY_ID + " INTEGER PRIMARY KEY,"
				+ KEY_NAME + " TEXT,"
				+ KEY_TIME + " TEXT,"
				+ KEY_STATUS + " INTEGER " + ")";
		
		db.execSQL(CREATE_TESTCASE_TABLE);
		
	}

	//Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS "+ TABLE_TESTCASES);
		
		// Create tables again
		onCreate(db);		
	}
	
	/**
	 * All CRUD Operation
	 * 
	 */
	//Adding new testcase
	public void addTestCase(TestCase testcase){
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(KEY_ID, testcase.get_id());
		values.put(KEY_NAME, testcase.get_test_case_name());
		values.put(KEY_TIME, testcase.get_test_time());
		values.put(KEY_STATUS, testcase.get_test_case_status());
		
		// Inserting Row
		db.insert(TABLE_TESTCASES, null, values);
		db.close(); //closing datebase connection
	}
	
	// Getting single testcase
	public  TestCase getTestCase(int id){
		SQLiteDatabase db = this.getReadableDatabase();
		
		Cursor cursor = db.query(TABLE_TESTCASES, new String[]{ KEY_ID,
				KEY_NAME, KEY_TIME, KEY_STATUS }, KEY_ID + "=?",
				new String[]{String.valueOf(id)},null,null,null,null);
		
		if(cursor !=null)
			cursor.moveToFirst();
		
		TestCase testcase = new TestCase(
				Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), 
				cursor.getString(2),
				Integer.parseInt(cursor.getString(3))				
				);
		//return testcase
		return testcase;		
	}
	
	// Getting all testcase
	public List<TestCase>getAllTestCase(int status){
		List<TestCase> testCaseList = new ArrayList<TestCase>();
		
		String selectQuery = new String();
		//Select All Query
		if(status == 1)
			selectQuery ="SELECT * FROM " + TABLE_TESTCASES + " WHERE "+KEY_STATUS+" = 1";
		else if (status == 2)
			selectQuery ="SELECT * FROM " + TABLE_TESTCASES + " WHERE "+KEY_STATUS+" = 2";
		else if (status == 0)
			selectQuery ="SELECT * FROM " + TABLE_TESTCASES + " WHERE "+KEY_STATUS+" = 0";
		else
			selectQuery ="SELECT * FROM " + TABLE_TESTCASES ;
		
		SQLiteDatabase db = this.getWritableDatabase();
		
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		//looping throught all rows and adding to list
		if(cursor.moveToFirst()){
			do{
				TestCase testcase = new TestCase();
				testcase.set_id(Integer.parseInt(cursor.getString(0)));
				testcase.set_test_case_name(cursor.getString(1));
				testcase.set_test_time(cursor.getString(2));
				testcase.set_test_case_status(Integer.parseInt(cursor.getString(3)));
				
				//Adding testcase to list
				testCaseList.add(testcase);
			}while(cursor.moveToNext());
		}
		return testCaseList;		
	}
	
	// Getting all testcase Count
	public int getTestCaseCount(){
		String countQuery = "SELECT  * FROM " + TABLE_TESTCASES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
       // cursor.close();
 
        // return count
        return cursor.getCount();		
	}
	
	// Update single testcase
	public int updateTestCase(TestCase testcase){
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, testcase.get_test_case_name());
		values.put(KEY_TIME, testcase.get_test_time());
		values.put(KEY_STATUS, testcase.get_test_case_status());
		
		// updatign row
		return db.update(TABLE_TESTCASES, values, KEY_ID + " =?", 
				new String[]{String.valueOf(testcase.get_id())});
			
	}
	
	// Deleting single testcase
	public void deleteTestCase(TestCase testcase){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_TESTCASES, KEY_ID + " =? ", new String[]{String.valueOf(testcase.get_id())});
		db.close();		
		
	}
	
	// Deleting all testcase
	public void deleteAllTestCase(){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_TESTCASES, null , null);
	}
	/**
	 * CRUD END
	 */
	
	// Set testcase pass
	public void setTestCasePassFail(TestCase testcase, boolean isPass){
		
		/* Time */
		SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		String TestTime = date.format(new Date());
		
		testcase.set_test_time(TestTime);
		
		if(isPass)
			testcase.set_test_case_status(1);//Pass
		else if(!isPass)
			testcase.set_test_case_status(2);//Fail
		else
			testcase.set_test_case_status(0);//No record
		
		//update testcase in database
		updateTestCase(testcase);
		
		Log.d(TAG, "TestCase: "+testcase.get_test_case_name()+ " updated");
	}
	
	//Get pass test cases
	public List<TestCase> getPassCases(){
		List<TestCase> testCaseList = new ArrayList<TestCase>();
		
		String selectQuery ="SELECT * FROM " + TABLE_TESTCASES + " WHERE "+KEY_STATUS+" = 1";
		SQLiteDatabase db = this.getWritableDatabase();		
		Cursor cursor = db.rawQuery(selectQuery, null);
		if(cursor.moveToFirst()){
			do{
				TestCase testcase = new TestCase();
				testcase.set_id(Integer.parseInt(cursor.getString(0)));
				testcase.set_test_case_name(cursor.getString(1));
				testcase.set_test_time(cursor.getString(2));
				testcase.set_test_case_status(Integer.parseInt(cursor.getString(3)));
				
				//Adding testcase to list
				testCaseList.add(testcase);
			}while(cursor.moveToNext());
		}
		return testCaseList;		
	}

}
