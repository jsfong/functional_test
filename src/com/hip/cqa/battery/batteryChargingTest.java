/**
 * Revision History
 * ----------------
 * 06-JUN-2014 - Initial Draft
 * 07-JUN-2014 - Update message for FULL CHARGE Battery condition
 */

package com.hip.cqa.battery;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.BatteryManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hip.cqa.R;
import com.hip.cqa.Test_All_Mode;
import com.hip.cqa.Test_Individual_Mode;
import com.hip.cqa.Test_Main;
import com.hip.cqa.Test_Result;

public class batteryChargingTest extends Test_Main{

	//UI
	private TextView batteryLevel, batteryStatus, batteryVoltage, batteryTempature, batteryHealth;
	private RelativeLayout layout;
	
	private int testMode;
	private String TAG = "Hi-P_BatteryCharging";
	private static String testCaseStr = "Charging Test - ";
	private static String testCaseResult = "Not Tested";
	final Context context = this;

	private BroadcastReceiver batteryStatusReceiver = new BroadcastReceiver(){

		@Override
		public void onReceive(Context context, Intent intent) {

			if(intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)){
				batteryLevel.setText("Level: " + String.valueOf(intent.getIntExtra("level", 0)) + "%");
				batteryVoltage.setText("Voltage: " + String.valueOf((float)intent.getIntExtra("voltage", 0)/1000) + "V");
				batteryTempature.setText("Tempature: " + String.valueOf((float)intent.getIntExtra("temperature", 0)/10) + "c");

				int status = intent.getIntExtra("status", BatteryManager.BATTERY_STATUS_UNKNOWN);
				int level = intent.getIntExtra("level", BatteryManager.BATTERY_STATUS_UNKNOWN);
				
				String strStatus;
				if ((status == BatteryManager.BATTERY_STATUS_CHARGING) && (level != 100)){
					strStatus = "Charging";
					layout.setBackgroundColor(Color.GREEN);
				} else if (status == BatteryManager.BATTERY_STATUS_DISCHARGING){
					//strStatus = "Dis-charging";
					strStatus = "Not Charging";
					layout.setBackgroundColor(Color.BLACK);
				} else if (status == BatteryManager.BATTERY_STATUS_NOT_CHARGING){
					strStatus = "Not charging";
					layout.setBackgroundColor(Color.BLACK);
				} else if ((status == BatteryManager.BATTERY_STATUS_FULL) || (level == 100)){
					//strStatus = "Full";
					strStatus = "Phone detected charger but unable to proceed with charging. Reason: Battery full charged";
					layout.setBackgroundColor(Color.GREEN);
				} else {
					strStatus = "Unknown";
					layout.setBackgroundColor(Color.BLACK);
				}

				batteryStatus.setText("Status: " + strStatus);

				int health = intent.getIntExtra("health", BatteryManager.BATTERY_HEALTH_UNKNOWN);
				String strHealth;
				if (health == BatteryManager.BATTERY_HEALTH_GOOD){
					strHealth = "Good";
				} else if (health == BatteryManager.BATTERY_HEALTH_OVERHEAT){
					strHealth = "Over Heat";
				} else if (health == BatteryManager.BATTERY_HEALTH_DEAD){
					strHealth = "Dead";
				} else if (health == BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE){
					strHealth = "Over Voltage";
				} else if (health == BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE){
					strHealth = "Unspecified Failure";
				} else{
					strHealth = "Unknown";
				}
				batteryHealth.setText("Health: " + strHealth);

			}


		}

	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.battery_charging);

		batteryLevel = (TextView)findViewById(R.id.tvBatteryLevel);
		batteryStatus = (TextView)findViewById(R.id.tvBatteryStatus);
		batteryVoltage = (TextView)findViewById(R.id.tvBatteryVolt);
		batteryTempature = (TextView)findViewById(R.id.tvBatteryTemperature);
		batteryHealth = (TextView)findViewById(R.id.tvBatteryHealth);
		
		layout = (RelativeLayout)findViewById(R.id.rlBattery);

		IntentFilter batteryStatusFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);		
		registerReceiver(batteryStatusReceiver , batteryStatusFilter);

		/* Read Test Mode Type selected by user */
		Bundle testModeSelected = getIntent().getExtras();
		this.testMode = testModeSelected.getInt("testModeType");
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			finish();

			if (this.testMode == 1) {
				/* Set test result status */
				testCaseResult = "PASS";

				Intent nIntent = new Intent(this, Test_Result.class);
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			} else /* Individual test case */
			{
				Intent nIntent = new Intent(this, Test_Individual_Mode.class);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);
			}
		} else if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
				&& (this.testMode == 1)) {
			testCaseResult = "FAIL";
			showQuitTestAlert();

		}  else {
			/* Reject other keyevent */
			Toast.makeText(this, getString(R.string.mode_continue_next), 0)
			.show();
		}

		if (!(testCaseResult.equals("Not Tested"))) {

			if(testCaseResult.equals("PASS"))
				db.setTestCasePassFail(Test_Main.tcBattery,true);
			if(testCaseResult.equals("FAIL"))
				db.setTestCasePassFail(Test_Main.tcBattery,false);
			/* Save start test time to resultLog */
			StringBuilder builder = new StringBuilder("");
			builder.append(testCaseStr + testCaseResult);
			Log.d("In Test_All_Mode", "Test result = " + testCaseStr
					+ testCaseResult);

			try {
				/* /data/data/com.hip.cqa/app_log/resultLog */
				BufferedWriter buf = new BufferedWriter(new FileWriter(
						Test_All_Mode.testResultFile, true));
				buf.append(builder.toString());
				buf.newLine();
				buf.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return true;
	}

	private void showQuitTestAlert() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		/* Set title */
		alertDialogBuilder.setTitle("Test Failed");

		/* Set dialog message */
		alertDialogBuilder
		.setMessage("Click YES to Quit, NO to Proceed Next")
		.setCancelable(false)

		.setNegativeButton("No",new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog,int id) 
			{
				/* Failed test but proceed to next test case */
				Intent nIntent = new Intent(batteryChargingTest.this, Test_Result.class);
				nIntent.putExtra("testModeType", 1);
				nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(nIntent);

				/* End current activity */
				finish();
			}
		})

		.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				/*
								Intent homescreenIntent = new Intent(
										Intent.ACTION_MAIN);
								homescreenIntent
										.addCategory(Intent.CATEGORY_HOME);
								homescreenIntent
										.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				 */
				Intent nIntent = new Intent(batteryChargingTest.this, Test_Main.class);
				startActivity(nIntent);

				/* End current activity */
				finish();
			}
		});

		/* Create alert dialog */
		AlertDialog alertDialog = alertDialogBuilder.create();

		/* Show it */
		alertDialog.show();
	}

}
