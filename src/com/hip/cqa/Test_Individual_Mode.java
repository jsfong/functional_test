/**
 * Revision History
 * ----------------
 * 10-JUN-2013 - Initial Draft
 * 04-SEP-2013 - Add Gyroscope test
 */

package com.hip.cqa;

import android.os.Bundle;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
//import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.hip.cqa.Test_Main;
import com.hip.cqa.display.testpattern.TestPattern;
import com.hip.cqa.ecompass.EcompassTest;
import com.hip.cqa.ecompass.EcompassTestSimple;
import com.hip.cqa.vibrator.VibratorTest;
import com.hip.cqa.simcard.SimCardTest;
import com.hip.cqa.proximity.ProximityTest;
import com.hip.cqa.acc.AccelerometerTest;
import com.hip.cqa.acc.AccelerometerTestSimple;
import com.hip.cqa.ambientlight.AmbientLightTest;
import com.hip.cqa.ambientlight.AmbientLightTestSimple;
import com.hip.cqa.headset.HeadsetInfoTest;
import com.hip.cqa.audio.AudioPlayMediaFile;
import com.hip.cqa.audio.AudioMicLoopBack;
import com.hip.cqa.battery.batteryChargingTest;
import com.hip.cqa.led.LedTest;
//import com.hip.cqa.eink.EinkWaveTestActivity;
import com.hip.cqa.touch.MainActivity;
import com.hip.cqa.camera.CameraTestMain;
import com.hip.cqa.gyro.GyroscopeTest;
import com.hip.cqa.gyro.GyroscopeTestSimple;
import com.hip.cqa.nfc.NfcTest;

public class Test_Individual_Mode extends Test_Main
{
	/* Define local variables */

	protected String TAG = "Hi-P_Test_Main";
	private Button button_start_display_testpattern;
	private Button button_start_vibrator;
	private Button button_start_simcard;
	private Button button_start_proximity;
	private Button button_start_ambientlight;
	private Button button_start_headsetinfo;
	private Button button_start_audioplaymediafile;
	private Button button_start_audiomicloopback;
	private Button button_start_led;
	private Button button_start_eink;
	private Button button_start_touch;
	private Button button_start_camera;
	private Button button_start_gyroscope;
	private Button button_start_nfc;
	private Button button_start_acc;
	private Button button_start_ecompass;
	private Button button_start_battery;
	
	private int testMode;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test_selection_menu);
		
		//getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		addListenerOnButton(); /* Handle Main Menu Selection */
	
	} //End of protected void onCreate(Bundle savedInstanceState)
	
	public void addListenerOnButton()
	{
		button_start_display_testpattern = (Button) findViewById(R.id.button_start_display_testpattern);
		button_start_display_testpattern.setOnClickListener (onClickListener);	
		
		button_start_vibrator = (Button) findViewById(R.id.button_start_vibrator);
		button_start_vibrator.setOnClickListener (onClickListener);	
		
		button_start_simcard = (Button) findViewById(R.id.button_start_simcard);
		button_start_simcard.setOnClickListener (onClickListener);
		
		button_start_proximity = (Button) findViewById(R.id.button_start_proximity);
		button_start_proximity.setOnClickListener (onClickListener);
		
		button_start_ambientlight = (Button) findViewById(R.id.button_start_ambientlight);
		button_start_ambientlight.setOnClickListener (onClickListener);
		
		button_start_headsetinfo = (Button) findViewById(R.id.button_start_headsetinfo);
		button_start_headsetinfo.setOnClickListener (onClickListener);
		
		button_start_audioplaymediafile = (Button) findViewById(R.id.button_start_audioplaymediafile);
		button_start_audioplaymediafile.setOnClickListener (onClickListener);
		
		button_start_audiomicloopback = (Button) findViewById(R.id.button_start_audiomicloopback);
		button_start_audiomicloopback.setOnClickListener (onClickListener);
		
		button_start_led = (Button) findViewById(R.id.button_start_led);
		button_start_led.setOnClickListener (onClickListener);
		
		button_start_eink = (Button) findViewById(R.id.button_start_eink);
		button_start_eink.setOnClickListener (onClickListener);
		
		button_start_touch = (Button) findViewById(R.id.button_start_touch);
		button_start_touch.setOnClickListener (onClickListener);
		
		button_start_camera = (Button) findViewById(R.id.button_start_camera);
		button_start_camera.setOnClickListener (onClickListener); 
		
		button_start_gyroscope = (Button) findViewById(R.id.button_start_gyroscope);
		button_start_gyroscope.setOnClickListener (onClickListener);
		
		button_start_nfc = (Button) findViewById(R.id.button_start_nfc);
		button_start_nfc.setOnClickListener (onClickListener);
		
		button_start_acc = (Button) findViewById(R.id.button_start_acc);
		button_start_acc.setOnClickListener (onClickListener);
		
		button_start_ecompass = (Button) findViewById(R.id.button_start_ecompass);
		button_start_ecompass.setOnClickListener (onClickListener);
		
		button_start_battery = (Button) findViewById(R.id.button_start_battery);
		button_start_battery.setOnClickListener (onClickListener);
	}
	
	/* Define class for onClickListener */
	private OnClickListener onClickListener = new OnClickListener()
	{
		public void onClick(View v)
		{
			switch(v.getId())
			{
				case R.id.button_start_display_testpattern:
					Intent displayIntent = new Intent(getBaseContext(), TestPattern.class);
					displayIntent.putExtra("testModeType", 2); /* Test_Individual_Mode = 2 */
					startActivity(displayIntent);
					finish();
					break;

				case R.id.button_start_vibrator:
					Intent vibratorIntent = new Intent(getBaseContext(), VibratorTest.class);
					vibratorIntent.putExtra("testModeType", 2);
					startActivity(vibratorIntent);
					finish();
					break;

				case R.id.button_start_simcard:
					Intent simcardIntent = new Intent(getBaseContext(), SimCardTest.class);
					simcardIntent.putExtra("testModeType", 2);
					startActivity(simcardIntent);
					finish();
					break;					
					
				case R.id.button_start_proximity:
					Intent proximityIntent = new Intent(getBaseContext(), ProximityTest.class);
					proximityIntent.putExtra("testModeType", 2);
					startActivity(proximityIntent);
					finish();
					break;					

				case R.id.button_start_ambientlight:
					Intent ambientlightIntent = new Intent(getBaseContext(), AmbientLightTest.class);
					ambientlightIntent.putExtra("testModeType", 2);
					startActivity(ambientlightIntent);
					finish();
					break;	
					
				case R.id.button_start_headsetinfo:
					Intent headsetinfoIntent = new Intent(getBaseContext(), HeadsetInfoTest.class);
					headsetinfoIntent.putExtra("testModeType", 2);
					startActivity(headsetinfoIntent);
					finish();
					break;	
					
				case R.id.button_start_audioplaymediafile:
					Intent audioplaymediafileIntent = new Intent(getBaseContext(), AudioPlayMediaFile.class);
					audioplaymediafileIntent.putExtra("testModeType", 2);
					startActivity(audioplaymediafileIntent);
					finish();
					break;	

				case R.id.button_start_audiomicloopback:
					Intent audiomicloopbackIntent = new Intent(getBaseContext(), AudioMicLoopBack.class);
					audiomicloopbackIntent.putExtra("testModeType", 2);
					startActivity(audiomicloopbackIntent);
					finish();
					break;	
			
				case R.id.button_start_led:
					Intent ledIntent = new Intent(getBaseContext(), LedTest.class);
					ledIntent.putExtra("testModeType", 2);
					startActivity(ledIntent);
					finish();
					break;	
					
				case R.id.button_start_eink:
					/*Intent einkIntent = new Intent(getBaseContext(), EinkWaveTestActivity.class);
					einkIntent.putExtra("testModeType", 2);
					startActivity(einkIntent);
					finish();*/
					break;	

				case R.id.button_start_touch:	
					Intent touchIntent = new Intent(getBaseContext(), MainActivity.class);
					touchIntent.putExtra("testModeType", 2);
					startActivity(touchIntent);
					finish();
					break;	
					
				case R.id.button_start_camera:	
					Intent cameraIntent = new Intent(getBaseContext(), CameraTestMain.class);
					cameraIntent.putExtra("testModeType", 2);
					startActivity(cameraIntent);
					finish();
					break;		
					
				case R.id.button_start_gyroscope:	
					Intent gyroscopeIntent = new Intent(getBaseContext(), GyroscopeTest.class);
					gyroscopeIntent.putExtra("testModeType", 2);
					startActivity(gyroscopeIntent);
					break;	
					
				case R.id.button_start_nfc:	
					Intent nfcIntent = new Intent(getBaseContext(), NfcTest.class);
					nfcIntent.putExtra("testModeType", 2);
					startActivity(nfcIntent);
					break;	
					
				case R.id.button_start_acc:
					Intent accIntent = new Intent(getBaseContext(), AccelerometerTest.class);
					accIntent.putExtra("testModeType", 2);
					startActivity(accIntent);
					break;
					
				case R.id.button_start_ecompass:
					Intent ecompassIntent = new Intent(getBaseContext(), EcompassTest.class);
					ecompassIntent.putExtra("testModeType", 2);
					startActivity(ecompassIntent);
					break;
					
				case R.id.button_start_battery:
					Intent batteryIntent = new Intent(getBaseContext(), batteryChargingTest.class);
					batteryIntent.putExtra("testModeType", 2);
					startActivity(batteryIntent);
					break;
				
			}
					
		}
	};
	
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
    	if(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
    	{
    		finish();
			Intent nIntent = new Intent(this, Test_Main.class);
			nIntent.putExtra("testCompleted", false);
			nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(nIntent);
    	}
    	else if((keyCode == KeyEvent.KEYCODE_BACK) || (keyCode == KeyEvent.KEYCODE_VOLUME_UP))
    	{
    		Toast.makeText(this, getString(R.string.mode_exit), 0).show();
    	}
    	return true;
    }
    
	protected void onDestroy()
	{
	  super.onDestroy();
	  //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}
} //end of public class Test_Individual_Mode
